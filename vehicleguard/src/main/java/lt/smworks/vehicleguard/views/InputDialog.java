package lt.smworks.vehicleguard.views;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.EditText;

import java.io.File;
import java.lang.ref.WeakReference;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import lt.smworks.tools.utils.IOUtils;
import lt.smworks.tools.utils.IntentUtils;
import lt.smworks.tools.utils.L;
import lt.smworks.vehicleguard.R;

public class InputDialog extends DialogFragment {

    private static final String TAG = "InputDialog";
    private static final String EMAIL_ADDRESS = "vehicleguard@smworks.lt";
    public static final String LOG_FILE = "log.txt";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        EditText editText = new EditText(getContext());
        return new AlertDialog.Builder(getActivity())
                .setView(editText)
                .setTitle(R.string.enter_message)
                .setPositiveButton(R.string.send, (dialogInterface, i)
                        -> sendEmailWithLog(editText.getText().toString()))
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dismiss())
                .create();
    }

    public void show(@NonNull FragmentManager fragmentManager) {
        show(fragmentManager, TAG);
    }

    private void sendEmailWithLog(String text) {
        new LogTask(getContext(), text).execute();
    }

    private static class LogTask extends AsyncTask<Void, Void, File> {

        private final String text;
        private WeakReference<Context> context;

        LogTask(@NonNull Context context, @NonNull String text) {
            this.context = new WeakReference<>(context);
            this.text = text;
        }

        @Override
        protected File doInBackground(Void... voids) {
            File file = IOUtils.createFileOnExternalStorage(context.get(), LOG_FILE);
            if (!L.getInstance().copyLogToFile(file)) {
                return null;
            }
            return file;
        }

        @Override
        protected void onPostExecute(File result) {
            if (result == null) {
                return;
            }
            IntentUtils.EmailIntentBuilder builder = new IntentUtils.EmailIntentBuilder()
                    .setMessage(text)
                    .setSubject(context.get().getString(R.string.error_report))
                    .setRecipent(EMAIL_ADDRESS)
                    .setAttachment(result);
            IntentUtils.sendEmail(context.get(), builder.build(), R.string.choose_client);
        }
    }
}
