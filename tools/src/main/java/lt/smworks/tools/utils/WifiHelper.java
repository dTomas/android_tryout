/**
 * @author Martynas Šustavičius
 * @since 2014 08 13
 */
package lt.smworks.tools.utils;

import java.lang.reflect.Method;
import java.util.List;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

/**
 * Contains methods for managing WIFI.
 */
public class WifiHelper {

	public static final String TAG = "WifiHelper";

	public enum WIFI_TYPE {
		UNKNOWN, WPA, WEP, OPEN
	}
	
	public static boolean isApEnabled(Context context) {
		WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		try {
			Method method = wifi.getClass().getMethod("isWifiApEnabled");
			return (Boolean) method.invoke(wifi);
		} catch (Exception e) {
			L.w(TAG,"Unable to set wifi AP state. Error: %s.", e);
			return false;
		}
	}
	
	public static boolean setApEnabled(Context context, boolean enabled) {
		WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	    Class<?>[] types = new Class[] {WifiConfiguration.class, boolean.class};
		try {
			Method method = wifi.getClass().getMethod("setWifiApEnabled", types);
			method.invoke(wifi, null, enabled);
			return true;
		} catch (Exception e) {
			L.w(TAG,"Unable to set wifi AP state. Error: %s.", e);
			return false;
		}
	}
	
	public static boolean disableAP(Context context) {
		return setApEnabled(context, false);
	}
	
	public static boolean enableAPWithCredentials(Context context, String username, String password) {
		WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		WifiConfiguration wc = new WifiConfiguration();
	    wc.SSID = username;
	    wc.preSharedKey  = password;
	    wc.hiddenSSID = true;
	    wc.status = WifiConfiguration.Status.CURRENT; //ENABLED;        
	    wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
	    wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
	    wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
	    wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
	    wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
	    wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
	    wc.hiddenSSID = false;
	    Class<?>[] types = new Class[] {WifiConfiguration.class, boolean.class};
		try {
			Method method = wifi.getClass().getMethod("setWifiApEnabled", types);
			method.invoke(wifi, wc, true);
			return true;
		} catch (Exception e) {
			L.w(TAG,"Unable to set wifi AP state. Error: %s.", e);
			return false;
		}
	}
	
	public static boolean setApCredentials(Context context, String username, String password) {
	    WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	    Class<?>[] types = new Class[] {WifiConfiguration.class};
	    WifiConfiguration wc = new WifiConfiguration();
	    wc.SSID = username;
	    wc.preSharedKey  = password;
	    wc.hiddenSSID = true;
	    wc.status = WifiConfiguration.Status.CURRENT; //ENABLED;        
	    wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
	    wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
	    wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
	    wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
	    wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
	    wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
	    wc.hiddenSSID = false;
		try {
			Method method = wifi.getClass().getMethod("setWifiApConfiguration", types);
			method.invoke(wifi, wc);
			L.d(TAG,"Wifi credentials set to: name=%s, password=%s", username, password);
			return true;
		} catch (Exception e) {
			L.w(TAG,"Unable to set wifi AP credentials. Error: %s.", e);
			return false;
		}
	}
	
	/**
	 * Get the network info
	 * @param context
	 * @return
	 */
	public static NetworkInfo getNetworkInfo(Context context){
	    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    return cm.getActiveNetworkInfo();
	}
	
	/**
	 * @param context - android context.
	 * @return True if WiFi is enabled.
	 */
	public static boolean isWifiEnabled(Context context) {
		return ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).isWifiEnabled();
	}
	
	/**
	 * Checks if device is connected to WiFi.
	 * @param context - android context.
	 * @return True if device is connected to WiFi.
	 */
	public static boolean isConnectedToWifi(Context context){
	    NetworkInfo info = getNetworkInfo(context);
	    return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
	}
	
	/**
	 * Enables WiFi. Must not be executed on main thread.
	 * @param context - handle to context.
	 * @param waitTime - time (in milliseconds) to wait until WiFi is enabled.
	 * @return True on success, false otherwise.
	 */
	public static boolean enableWifi(Context context, long waitTime) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		if (wifiManager.isWifiEnabled()) {
			return true;
		}
		wifiManager.setWifiEnabled(true);
		for (long i = 0; i < waitTime; i++) {
			switch (wifiManager.getWifiState()) {
			case WifiManager.WIFI_STATE_DISABLED:
			case WifiManager.WIFI_STATE_DISABLING:
			case WifiManager.WIFI_STATE_UNKNOWN:
			case WifiManager.WIFI_STATE_ENABLING:
				continue;
			case WifiManager.WIFI_STATE_ENABLED:
				return true;
			}
			try {
				Thread.sleep(waitTime);
			} catch (InterruptedException e) {
				L.e(e);
			}
		}
		return false;
	}
	
	/**
	 * Disables WiFi. Must not be executed on main thread.
	 * @param context - handle to context.
	 * @param waitTime - time (in milliseconds) to wait until WiFi is disabled.
	 * @return True on success, false otherwise.
	 */
	public static boolean disableWifi(Context context, long waitTime) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		if (!wifiManager.isWifiEnabled()) {
			return true;
		}
		wifiManager.setWifiEnabled(false);
		for (long i = 0; i < waitTime; i++) {
			switch (wifiManager.getWifiState()) {
			case WifiManager.WIFI_STATE_DISABLING:
			case WifiManager.WIFI_STATE_UNKNOWN:
			case WifiManager.WIFI_STATE_ENABLING:
			case WifiManager.WIFI_STATE_ENABLED:
				continue;
			case WifiManager.WIFI_STATE_DISABLED:
				return true;
			}
			try {
				Thread.sleep(waitTime);
			} catch (InterruptedException e) {
				L.e(e);
			}
		}
		return false;
	}
	
	/**
	 * Connects to specified network. Must not be executed on main thread.
	 * @NOTE: WiFi must be enabled.
	 * @param context - handle to context.
	 * @param ssid - network name.
	 * @param password - network password.
	 * @param waitTime - time (in milliseconds) that describes how long to wait before checking
	 * if device is connected to specified network.
	 * @return True if operation was successful.
	 */
	public static boolean connectToWifi(Context context, String ssid, String password, long waitTime) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		if (!wifiManager.isWifiEnabled()) {
			return false;
		}
//		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
//		L.d("Current wifi ssid: %s. Info: %s.", wifiInfo.getSSID(), wifiInfo.toString());
//		if (wifiInfo.getSSID().equals("\"".concat(ssid).concat("\""))) {
//			L.d("Already connected to network with ssd: %s.", ssid);
//			return true;
//		}
		WifiConfiguration conf = new WifiConfiguration();
		conf.SSID = "\"" + ssid + "\"";
		List<ScanResult> networkList = wifiManager.getScanResults();
		if (networkList == null || networkList.size() == 0) {
			wifiManager.startScan();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				L.e(e1);
			}
			networkList = wifiManager.getScanResults();
		}
		WIFI_TYPE wifiType = WIFI_TYPE.UNKNOWN;
		if (networkList != null) {
			for (ScanResult network : networkList) {
				if (network.SSID.equals(ssid)) {
					L.d(TAG,"Network with ssid %s found. Its capabilities: %s.", ssid, network.capabilities);
					if (network.capabilities.contains("WPA")) {
						wifiType = WIFI_TYPE.WPA;
					} else if (network.capabilities.contains("WEP")) {
						wifiType = WIFI_TYPE.WEP;
					} else {
						wifiType = WIFI_TYPE.UNKNOWN;
					}
					break;
				}
			}
		} else {
			L.d(TAG,"Unable to connect to wifi. No wifi access points found.");
			return false;
		}
		if (wifiType == WIFI_TYPE.UNKNOWN) {
			L.d(TAG,"Unknown wifi type.");
			return false;
		} else if (wifiType == WIFI_TYPE.WEP) { // WEP
			conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
			conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
			conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
			conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
			conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
	        conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
	        conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
	        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
	        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
	        conf.wepTxKeyIndex = 0;
	        if (getHexKey(password)) {
	        	conf.wepKeys[0] = password;
	        } else {
	        	conf.wepKeys[0] = "\"".concat(password).concat("\"");
	        }
		} else if (wifiType == WIFI_TYPE.WPA) { // WPA
			conf.preSharedKey = "\""+ password +"\"";
			conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
			conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
			conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
			conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
			conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
			conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
			conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
			conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
			conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
		}
//		// OPEN
//        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
//        conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
//        conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
//        conf.allowedAuthAlgorithms.clear();
//        conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
//        conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
//        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
//        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
//        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
//        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
		// Finally we add the new configuration to the managed list of networks
	    int nid = wifiManager.addNetwork(conf);
	    L.d(TAG,"Added wifi network ssid: %s, id: %d.", ssid, nid);
	    if (nid == -1) {
	    	L.w(TAG,"Unable to add network.");
	    	return false;
	    }
		List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
		if (list == null) {
			L.d(TAG,"No available configured networks.");
			return false;
		}
		for (WifiConfiguration wc : list) {
			L.d("SSID: %s", wc.SSID);
			if (wc.SSID != null && wc.SSID.equals("\"" + ssid + "\"")) {
				wifiManager.disconnect();
				wifiManager.enableNetwork(wc.networkId, true);
				wifiManager.reconnect();
			}
		}
		try {
			Thread.sleep(waitTime);
		} catch (InterruptedException e) {
			L.e(e);
		}
		return isConnectedToWifi(context);
	}
	
	/**
	 * WEP has two kinds of password, a hex value that specifies the key or
	 * a character string used to generate the real hex. This checks what kind of
	 * password has been supplied. The checks correspond to WEP40, WEP104 & WEP232
	 * @param s
	 * @return
	 */
	private static boolean getHexKey(String s) {
	    if (s == null) {
	        return false;
	    }
	    int len = s.length();
	    if (len != 10 && len != 26 && len != 58) {
	        return false;
	    }
	    for (int i = 0; i < len; ++i) {
	        char c = s.charAt(i);
	        if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')) {
	            continue;
	        }
	        return false;
	    }
	    return true;
	}
}
