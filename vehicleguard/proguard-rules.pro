-optimizationpasses 5
-repackageclasses
-allowaccessmodification
-dontpreverify
-dontskipnonpubliclibraryclasses
-repackageclasses ''

-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**
-keepattributes SourceFile, LineNumberTable, *Annotation*
-keep public class * extends java.lang.Exception

-keepclassmembers class * extends java.lang.Enum {
    <fields>;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# Disable for crashlytics
#-printmapping mapping.txt