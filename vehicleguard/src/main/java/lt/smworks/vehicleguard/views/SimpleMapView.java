//package lt.smworks.vehicleguard.views;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.util.AttributeSet;
//
//import com.google.android.gms.maps.CameraUpdate;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.GoogleMapOptions;
//import com.google.android.gms.maps.MapView;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//
//public class SimpleMapView extends MapView {
//
//    public static final int ZOOM_LEVEL = 15;
//    private GoogleMap map;
//    private LatLng location;
//    private Marker marker;
//
//    public SimpleMapView(Context context) {
//        super(context);
//    }
//
//    public SimpleMapView(Context context, AttributeSet attributeSet) {
//        super(context, attributeSet);
//    }
//
//    public SimpleMapView(Context context, AttributeSet attributeSet, int i) {
//        super(context, attributeSet, i);
//    }
//
//    public SimpleMapView(Context context, GoogleMapOptions googleMapOptions) {
//        super(context, googleMapOptions);
//    }
//
//    public void setLastLocation(@NonNull LatLng location) {
//        this.location = location;
//    }
//
//    public void start(@Nullable Bundle savedInstanceState) {
//        onCreateView(savedInstanceState);
//        getMapAsync(googleMap -> {
//            map = googleMap;
//            if (location != null) {
//                updateLocation(location);
//            }
//            onResume();
//        });
//    }
//
//    public void stop() {
//        marker = null;
//        location = null;
//        onPause();
//        onStop();
//        onDestroy();
//    }
//
//    public void updateLocation(@NonNull LatLng latLng) {
//        if (map == null) {
//            return;
//        }
//
//        CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
//        map.moveCamera(center);
//        CameraUpdate zoom = CameraUpdateFactory.zoomTo(ZOOM_LEVEL);
//        map.animateCamera(zoom);
//        if (marker == null) {
//            MarkerOptions markerOptions = new MarkerOptions().position(latLng);
//            marker = map.addMarker(markerOptions);
//        } else {
//            marker.setPosition(latLng);
//        }
//
//    }
//}
