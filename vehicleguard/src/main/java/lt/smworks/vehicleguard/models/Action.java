package lt.smworks.vehicleguard.models;

import androidx.annotation.NonNull;

public enum Action {
    START("action_start"),
    STOP("action_stop"),
    SMS("action_sms"),
    ACTIVITY_RECOGNITION("activity_recognition"),
    LOCATION_UPDATE("location_update"),
    GEOFENCE("geofence"),
    UNKNOWN("unknown");

    private final String type;

    Action(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }

    public static Action fromString(@NonNull String message) {
        for (Action action : values()) {
            if (message.equals(action.toString())) {
                return action;
            }
        }
        return UNKNOWN;
    }
}
