//package lt.smworks.gasup.utils;
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.graphics.ImageFormat;
//import android.hardware.Camera;
//import android.os.SystemClock;
//import android.util.AttributeSet;
//import android.util.Log;
//import android.util.SparseArray;
//
//import com.google.android.gms.vision.Detector;
//import com.google.android.gms.vision.Frame;
//import com.google.android.gms.vision.text.TextBlock;
//import com.google.android.gms.vision.text.TextRecognizer;
//
//import java.nio.ByteBuffer;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import androidx.annotation.NonNull;
//import lt.smworks.tools.models.Size;
//import lt.smworks.tools.utils.L;
//
//public class OcrCameraPreview extends CameraPreview implements Detector.Processor<TextBlock> {
//
//    public static final String TAG = "OcrCameraPreview";
//
//    private Thread processingThread;
//    private FrameProcessingRunnable frameProcessor;
//    private Map<byte[], ByteBuffer> bytesToByteBuffer = new HashMap<>();
//    private final Object cameraLock = new Object();
//    private OnTextExtractedListener listener;
//
//    public OcrCameraPreview(Context context) {
//        super(context);
//    }
//
//    public OcrCameraPreview(Context context, AttributeSet attrs) {
//        super(context, attrs);
//    }
//
//    public OcrCameraPreview(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//    }
//
//    @Override
//    public void start() {
//       super.start();
//       setOnPreviewReadyListener(this::onPreviewReady);
//    }
//
//    private void onPreviewReady(Camera camera) {
//        synchronized (cameraLock) {
//            TextRecognizer textRecognizer = new TextRecognizer.Builder(getContext()).build();
//            textRecognizer.setProcessor(this);
//            frameProcessor = new FrameProcessingRunnable(textRecognizer);
//            processingThread = new Thread(frameProcessor);
//            frameProcessor.setActive(true);
//            camera.setPreviewCallbackWithBuffer(new CameraPreviewCallback());
//            camera.addCallbackBuffer(createPreviewBuffer(previewSize));
//            camera.addCallbackBuffer(createPreviewBuffer(previewSize));
//            camera.addCallbackBuffer(createPreviewBuffer(previewSize));
//            camera.addCallbackBuffer(createPreviewBuffer(previewSize));
//            processingThread.start();
//        }
//    }
//
//    @Override
//    public void stop() {
//        synchronized (cameraLock) {
//            camera.setPreviewCallbackWithBuffer(null);
//            frameProcessor.setActive(false);
//            if (processingThread != null) {
//                try {
//                    // Wait for the thread to complete to ensure that we can't have multiple threads
//                    // executing at the same time (i.ex., which would happen if we called start too
//                    // quickly after stop).
//                    processingThread.join();
//                } catch (InterruptedException ex) {
//                    Log.d(TAG, "Frame processing thread interrupted on release.");
//                }
//                processingThread = null;
//            }
//
//            // clear the buffer to prevent oom exceptions
//            bytesToByteBuffer.clear();
//            frameProcessor.release();
//            super.stop();
//        }
//    }
//
//    @Override
//    public void release() {
//
//    }
//
//    @Override
//    public void receiveDetections(Detector.Detections<TextBlock> detections) {
//        SparseArray<TextBlock> items = detections.getDetectedItems();
//        ArrayList<String> list = new ArrayList<>();
//        for (int i = 0; i < items.size(); i++) {
//            if (items.get(i) == null) {
//                continue;
//            }
//            L.d(TAG, "Text: %s", items.get(i).getValue());
//            list.add(items.get(i).getValue());
//        }
//        if (listener != null) {
//            listener.onTextExtracted(list);
//        }
//    }
//
//    public void setOnTextListener(OnTextExtractedListener listener) {
//        this.listener = listener;
//    }
//
//    /**
//     * Called when the camera has a new preview frame.
//     */
//    private class CameraPreviewCallback implements Camera.PreviewCallback {
//        @Override
//        public void onPreviewFrame(byte[] data, Camera camera) {
//            frameProcessor.setNextFrame(data, camera);
//        }
//    }
//
//    /**
//     * This runnable controls access to the underlying receiver, calling it to process frames when
//     * available from the camera.  This is designed to run detection on frames as fast as possible
//     * (i.ex., without unnecessary context switching or waiting on the next frame).
//     * <p/>
//     * While detection is running on a frame, new frames may be received from the camera.  As these
//     * frames come in, the most recent frame is held onto as pending.  As soon as detection and its
//     * associated processing are done for the previous frame, detection on the mostly recently
//     * received frame will immediately start on the same thread.
//     */
//    private class FrameProcessingRunnable implements Runnable {
//        private Detector<?> mDetector;
//        private long mStartTimeMillis = SystemClock.elapsedRealtime();
//
//        // This lock guards all of the member variables below.
//        private final Object mLock = new Object();
//        private boolean mActive = true;
//
//        // These pending variables hold the state associated with the new frame awaiting processing.
//        private long mPendingTimeMillis;
//        private int mPendingFrameId = 0;
//        private ByteBuffer mPendingFrameData;
//
//        FrameProcessingRunnable(Detector<?> detector) {
//            mDetector = detector;
//        }
//
//        /**
//         * Releases the underlying receiver.  This is only safe to do after the associated thread
//         * has completed, which is managed in camera source's release method above.
//         */
//        @SuppressLint("Assert")
//        void release() {
//            assert (processingThread.getState() == Thread.State.TERMINATED);
//            mDetector.release();
//            mDetector = null;
//        }
//
//        /**
//         * Marks the runnable as active/not active.  Signals any blocked threads to continue.
//         */
//        void setActive(boolean active) {
//            synchronized (mLock) {
//                mActive = active;
//                mLock.notifyAll();
//            }
//        }
//
//        /**
//         * Sets the frame data received from the camera.  This adds the previous unused frame buffer
//         * (if present) back to the camera, and keeps a pending reference to the frame data for
//         * future use.
//         */
//        void setNextFrame(byte[] data, Camera camera) {
//            synchronized (mLock) {
//                if (mPendingFrameData != null) {
//                    camera.addCallbackBuffer(mPendingFrameData.array());
//                    mPendingFrameData = null;
//                }
//
//                if (!bytesToByteBuffer.containsKey(data)) {
//                    Log.d(TAG,
//                            "Skipping frame.  Could not find ByteBuffer associated with the image " +
//                                    "data from the camera.");
//                    return;
//                }
//
//                // Timestamp and frame ID are maintained here, which will give downstream code some
//                // idea of the timing of frames received and when frames were dropped along the way.
//                mPendingTimeMillis = SystemClock.elapsedRealtime() - mStartTimeMillis;
//                mPendingFrameId++;
//                mPendingFrameData = bytesToByteBuffer.get(data);
//
//                // Notify the processor thread if it is waiting on the next frame (see below).
//                mLock.notifyAll();
//            }
//        }
//
//        /**
//         * As long as the processing thread is active, this executes detection on frames
//         * continuously.  The next pending frame is either immediately available or hasn't been
//         * received yet.  Once it is available, we transfer the frame info to local variables and
//         * run detection on that frame.  It immediately loops back for the next frame without
//         * pausing.
//         * <p/>
//         * If detection takes longer than the time in between new frames from the camera, this will
//         * mean that this loop will run without ever waiting on a frame, avoiding any context
//         * switching or frame acquisition time latency.
//         * <p/>
//         * If you find that this is using more CPU than you'd like, you should probably decrease the
//         * FPS setting above to allow for some idle time in between frames.
//         */
//        @Override
//        public void run() {
//            Frame outputFrame;
//            ByteBuffer data;
//
//            while (true) {
//                synchronized (mLock) {
//                    while (mActive && (mPendingFrameData == null)) {
//                        try {
//                            // Wait for the next frame to be received from the camera, since we
//                            // don't have it yet.
//                            mLock.wait();
//                        } catch (InterruptedException ex) {
//                            Log.d(TAG, "Frame processing loop terminated.", ex);
//                            return;
//                        }
//                    }
//
//                    if (!mActive) {
//                        // Exit the loop once this camera source is stopped or released.  We check
//                        // this here, immediately after the wait() above, to handle the case where
//                        // setActive(false) had been called, triggering the termination of this
//                        // loop.
//                        return;
//                    }
//
//                    outputFrame = new Frame.Builder()
//                            .setImageData(mPendingFrameData, previewSize.getWidth(),
//                                    previewSize.getHeight(), ImageFormat.NV21)
//                            .setId(mPendingFrameId)
//                            .setTimestampMillis(mPendingTimeMillis)
//                            .setRotation(rotation)
//                            .build();
//
//                    // Hold onto the frame data locally, so that we can use this for detection
//                    // below.  We need to clear mPendingFrameData to ensure that this buffer isn't
//                    // recycled back to the camera before we are done using that data.
//                    data = mPendingFrameData;
//                    mPendingFrameData = null;
//                }
//
//                // The code below needs to run outside of synchronization, because this will allow
//                // the camera to add pending frame(s) while we are running detection on the current
//                // frame.
//
//                try {
//                    mDetector.receiveFrame(outputFrame);
//                } catch (Throwable t) {
//                    Log.ex(TAG, "Exception thrown from receiver.", t);
//                } finally {
//                    camera.addCallbackBuffer(data.array());
//                }
//            }
//        }
//    }
//
//    /**
//     * Creates one buffer for the camera preview callback.  The size of the buffer is based off of
//     * the camera preview size and the format of the camera image.
//     *
//     * @return a new preview buffer of the appropriate size for the current camera settings
//     */
//    private byte[] createPreviewBuffer(Size previewSize) {
//        int bitsPerPixel = ImageFormat.getBitsPerPixel(ImageFormat.NV21);
//        long sizeInBits = previewSize.getHeight() * previewSize.getWidth() * bitsPerPixel;
//        int bufferSize = (int) Math.ceil(sizeInBits / 8.0d) + 1;
//
//        //
//        // NOTICE: This code only works when using play services v. 8.1 or higher.
//        //
//
//        // Creating the byte array this way and wrapping it, as opposed to using .allocate(),
//        // should guarantee that there will be an array to work with.
//        byte[] byteArray = new byte[bufferSize];
//        ByteBuffer buffer = ByteBuffer.wrap(byteArray);
//        if (!buffer.hasArray() || (buffer.array() != byteArray)) {
//            // I don't think that this will ever happen.  But if it does, then we wouldn't be
//            // passing the preview content to the underlying detector later.
//            throw new IllegalStateException("Failed to create valid buffer for camera source.");
//        }
//
//        bytesToByteBuffer.put(byteArray, buffer);
//        return byteArray;
//    }
//
//    public interface OnTextExtractedListener {
//        void onTextExtracted(@NonNull List<String> text);
//    }
//}
