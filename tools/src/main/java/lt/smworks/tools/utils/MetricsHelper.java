/**
 * @author martynas.su@gmail.com
 * @since 2014 08 30
 */
package lt.smworks.tools.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

public class MetricsHelper {

	/**
	 * This method converts device independent pixels to actual pixels.
	 * @param context - android context.
	 * @param dp - device independent pixel value which we need to convert into pixels.
	 * @return A float value to represent px equivalent to dp depending on device density.
	 */
	public static float convertDpToPixel(Context context, float dp){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float px = dp * (metrics.densityDpi / 160f);
	    return px;
	}

	/**
	 * This method converts device specific pixels to density independent pixels.
	 * @param context - android context.
	 * @param px - Pixel value which we need to convert into dp.
	 * @return A float value to represent dp equivalent to px value.
	 */
	public static float convertPixelsToDp(Context context, float px){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float dp = px / (metrics.densityDpi / 160f);
	    return dp;
	}
	
	/**
	 * @note Depends on screen orientation.
	 * @param context - android context.
	 * @return Width of the screen in pixels.
	 */
	@SuppressWarnings("deprecation")
	public int getScreenWidth(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		return display.getWidth();
	}
	
	/**
	 * @note Depends on screen orientation.
	 * @param context - android context.
	 * @return Height of the screen in pixels.
	 */
	@SuppressWarnings("deprecation")
	public int getScreenHeight(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		return display.getHeight();
	}
	
	/**
	 * @param context - android context.
	 * @return Height of the status bar in pixels.
	 */
	public int getStatusBarHeight(Activity activity) {
		Rect rectangle = new Rect();
		Window window = activity.getWindow();
		window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
		int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
		return contentViewTop - rectangle.top;
	}
}
