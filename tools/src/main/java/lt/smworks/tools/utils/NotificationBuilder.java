package lt.smworks.tools.utils;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.Notification.BigPictureStyle;
import android.app.Notification.BigTextStyle;
import android.app.Notification.Builder;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;

public class NotificationBuilder {

    private NotificationManager notificationManager;
    private Context context;
    private Class<?> mainActivityClass;
    private String intentAction;
    private int icon;
    private int title;
    private int smallIcon;
    private int ticker;
    private int contentText;
    private String notificationChannelId;
    @StringRes
    private int notificationChannelNameRes;

    public NotificationBuilder(@NonNull Context context) {
        this.context = context;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public NotificationBuilder setMainActivityClass(@NonNull Class<?> mainActivityClass) {
        this.mainActivityClass = mainActivityClass;
        return this;
    }

    public NotificationBuilder setIntentAction(@NonNull String intentAction) {
        this.intentAction = intentAction;
        return this;
    }

    public NotificationBuilder setIcon(@IdRes int icon) {
        this.icon = icon;
        return this;
    }

    public NotificationBuilder setSmallIcon(@IdRes int smallIcon) {
        this.smallIcon = smallIcon;
        return this;
    }

    public NotificationBuilder setTitle(@StringRes int title) {
        this.title = title;
        return this;
    }

    public NotificationBuilder setTicker(@StringRes int ticker) {
        this.ticker = ticker;
        return this;
    }

    public NotificationBuilder setContentText(@StringRes int contentText) {
        this.contentText = contentText;
        return this;
    }

    public NotificationBuilder setNotificationChannelId(@NonNull String id) {
        this.notificationChannelId = id;
        return this;
    }

    public NotificationBuilder setNotificationChannelName(@StringRes int nameRes) {
        this.notificationChannelNameRes = nameRes;
        return this;
    }

    public Notification build() {
        if (notificationManager == null) {
            Toast.makeText(context,
                    "Unable to show notification. Notification manager is empty",
                    Toast.LENGTH_LONG).show();
            return null;
        }

        Intent notificationIntent = mainActivityClass == null
                ? new Intent() : new Intent(context, mainActivityClass);
        if (intentAction != null) {
            notificationIntent.setAction(intentAction);
        }
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), this.icon);
        Resources resources = context.getResources();
        int width = (int) resources.getDimension(android.R.dimen.notification_large_icon_width);
        int height = (int) resources.getDimension(android.R.dimen.notification_large_icon_height);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(notificationChannelId,
                    context.getString(notificationChannelNameRes), NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        return new NotificationCompat.Builder(context, notificationChannelId)
                .setContentTitle(context.getString(title))
                .setTicker(context.getString(ticker))
                .setContentText(context.getString(contentText))
                .setSmallIcon(smallIcon)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, width, height, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setVibrate(new long[]{0L})
                .build();
    }

    /**
     * Creates notification intent.
     *
     * @param context - application context.
     * @param cls     - class that will be launched with intent.
     * @param action  - intent action.
     * @return Intent object.
     */
    public static Intent createIntent(Context context, Class<?> cls, String action) {
        return createIntent(context, cls, action, null, null);
    }

    /**
     * Creates notification intent with string parameter.
     *
     * @param context    - application context.
     * @param cls        - class that will be launched with intent.
     * @param action     - intent action.
     * @param param      - parameter name.
     * @param paramValue - parameter value.
     * @return Intent object.
     */
    public static Intent createIntent(@NonNull Context context,
                                      @NonNull Class<?> cls,
                                      @NonNull String action,
                                      @Nullable String param,
                                      @Nullable String paramValue) {
        Intent intent = new Intent(context, cls);
        intent.setAction(action);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        if (param != null) {
            intent.putExtra(param, paramValue);
        }
        return intent;
    }


    /**
     * Creates notification with big picture, small icon, title and summary text.
     *
     * @param context        - application context.
     * @param intent         - intent that will be executed after notification is pressed.
     * @param notificationId - id of the notification.
     * @param title          - notification title.
     * @param message        - notification message.
     * @param picture        - notification picture.
     * @param smallIcon      - notification icon.
     */
    @SuppressLint("NewApi")
    public static void create(Context context, Intent intent, int notificationId, String title, String message, Bitmap picture, int smallIcon) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            return;
        }

        PendingIntent pendingIntent = PendingIntent.getActivities(context, 0, new Intent[]{intent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Builder builder = new Builder(context);
        builder.setAutoCancel(true);
        builder.setContentTitle(title);
        builder.setContentText(message);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(smallIcon);
        if (picture != null) {
            builder.setLargeIcon(picture);
        }
        if (DeviceInfo.getAndroidVersion() >= Build.VERSION_CODES.JELLY_BEAN) {
            BigPictureStyle style = new BigPictureStyle();
            style.setBigContentTitle(title);
            style.setSummaryText(message);
            if (picture != null) {
                style.bigPicture(picture);
            }
            builder.setStyle(style);
        }
        Notification notification = builder.build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.tickerText = message;
        notificationManager.notify(notificationId, notification);
    }

    /**
     * Creates notification with small icon, title and summary text.
     *
     * @param context        - application context.
     * @param intent         - intent that will be executed after notification is pressed.
     * @param notificationId - id of the notification.
     * @param title          - notification title.
     * @param message        - notification message.
     * @param smallIcon      - notification icon.
     */
    @SuppressLint("NewApi")
    public static void create(Context context, Intent intent, int notificationId, String title, String message, int smallIcon) {
        PendingIntent pendingIntent = PendingIntent.getActivities(context, 0, new Intent[]{intent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Builder builder = new Builder(context);
        builder.setAutoCancel(true);
        builder.setContentTitle(title);
        builder.setContentText(message);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(smallIcon);
        if (DeviceInfo.getAndroidVersion() >= Build.VERSION_CODES.JELLY_BEAN) {
            BigTextStyle style = new BigTextStyle();
            style.setBigContentTitle(title);
            style.bigText(message);
            builder.setStyle(style);
        }
        Notification notification = builder.build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.tickerText = message;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, notification);
    }
}
