package lt.smworks.gasup.viewmodels;

import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public abstract class GasupViewModel extends AndroidViewModel {

    public GasupViewModel(@NonNull Application application, @NonNull Bundle bundle) {
        super(application);
    }
}
