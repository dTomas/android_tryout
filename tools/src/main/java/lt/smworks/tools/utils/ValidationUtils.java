package lt.smworks.tools.utils;

import android.content.res.Resources;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import androidx.annotation.NonNull;
import lt.smworks.tools.R;
import lt.smworks.tools.interfaces.SimpleTextWatcher;

public class ValidationUtils {

    public static final int MAX_USERNAME_LETTER_COUNT = 24;

    /**
     * @param value - email address.
     * @return True if email address is valid.
     */
    public static boolean isValidEmail(String value) {
        return value != null && android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches();
    }

    /**
     * @param value - phone number.
     * @return True if phone number is valid.
     */
    public static boolean isValidPhoneNumber(String value) {
        return value != null && android.util.Patterns.PHONE.matcher(value).matches();
    }

    /**
     * @param value - URL address.
     * @return True if URL is valid.
     */
    public static boolean isValidUrl(String value) {
        return value != null && android.util.Patterns.WEB_URL.matcher(value).matches();
    }

    public static class FormValidator {

        private boolean useUsername;
        private boolean useEmail;
        private boolean usePassword;
        private boolean usernameValid;
        private boolean emailValid;
        private boolean passwordValid;
        private Runnable onSubmitListener;

        public FormValidator() {
        }

        public FormValidator setUsernameInput(TextInputEditText usernameInput) {
            useUsername = true;
            Resources resources = usernameInput.getResources();
            usernameInput.addTextChangedListener((SimpleTextWatcher) text -> {
                String username = Objects.requireNonNull(usernameInput.getText()).toString();
                if (TextUtils.isEmpty(username)) {
                    usernameInput.setError(resources.getString(R.string.username_is_empty));
                } else if (TextUtils.isEmpty(username) || username.length() > MAX_USERNAME_LETTER_COUNT) {
                    usernameInput.setError(resources.getString(R.string.username_too_long));
                } else {
                    usernameValid = true;
                }
            });
            return this;
        }

        public FormValidator setEmailInput(@NonNull EditText emailInput) {
            useEmail = true;
            Resources resources = emailInput.getResources();
            emailInput.addTextChangedListener((SimpleTextWatcher) text -> {
                if (!isValidEmail(emailInput.getText().toString())) {
                    emailInput.setError(resources.getString(R.string.email_is_not_valid));
                    emailValid = false;
                } else {
                    emailValid = true;
                }
            });
            return this;
        }

        public FormValidator setPasswordInput(@NonNull EditText passwordInput) {
            usePassword = true;
            Resources resources = passwordInput.getResources();
            passwordInput.addTextChangedListener((SimpleTextWatcher) text -> {
                if (TextUtils.isEmpty(passwordInput.getText().toString())) {
                    passwordInput.setError(resources.getString(R.string.password_is_empty));
                    passwordValid = false;
                } else if (passwordInput.getText().length() < 6) {
                    passwordInput.setError(resources.getString(R.string.password_is_too_short));
                    passwordValid = false;
                } else {
                    passwordValid = true;
                }
            });
            return this;
        }

        public FormValidator setOnSubmitListener(@NonNull Runnable onSubmitListener) {
            this.onSubmitListener = onSubmitListener;
            return this;
        }

        public FormValidator setSubmitButton(@NonNull Button submitButton) {
            submitButton.setOnClickListener(v -> {
                if (onSubmitListener != null) {

                    if (useUsername && !usernameValid) {
                        Snackbar.make(submitButton,
                                R.string.username_is_not_valid, Snackbar.LENGTH_LONG).show();
                    }

                    if (useEmail && !emailValid) {
                        Snackbar.make(submitButton,
                                R.string.email_is_not_valid, Snackbar.LENGTH_LONG).show();
                        return;
                    }

                    if (usePassword && !passwordValid) {
                        Snackbar.make(submitButton,
                                R.string.password_is_invalid, Snackbar.LENGTH_LONG).show();
                        return;
                    }

                    onSubmitListener.run();
                }
            });
            return this;
        }
    }
}
