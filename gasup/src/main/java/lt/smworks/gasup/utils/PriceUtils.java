package lt.smworks.gasup.utils;

import java.util.Locale;

import androidx.databinding.InverseMethod;

public final class PriceUtils {

    private PriceUtils() {
    }

    @InverseMethod("inverseFormatPrice")
    public static String formatPrice(double price) {
        return String.format(Locale.getDefault(), "%.3f", price);
    }

    public static double inverseFormatPrice(String price) {
        try {
            return Double.parseDouble(price);
        } catch (Exception e) {
            return 0;
        }
    }
}
