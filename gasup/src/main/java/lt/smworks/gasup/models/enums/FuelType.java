package lt.smworks.gasup.models.enums;

import android.content.res.Resources;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import lt.smworks.gasup.R;

public enum FuelType {
    PETROL_95(0, R.string.petrol_95, "fuelType95"),
    PETROL_98(1, R.string.petrol_98, "fuelType98"),
    DIESEL(2, R.string.diesel, "fuelTypeDiesel"),
    LPG(3, R.string.lpg, "fuelTypeLPG"),
    ALL(4, R.string.all, "all"),
    UNKNOWN(0, R.string.unknown, "unknown");

    private final int id;
    @StringRes
    private final int stringResId;
    private final String dbFieldName;

    FuelType(int id, @StringRes int resId, String dbFieldName) {
        this.id = id;
        this.stringResId = resId;
        this.dbFieldName = dbFieldName;
    }

    public int getStringResId() {
        return stringResId;
    }

    @NonNull
    public static ArrayList<String> getAllValues(@NonNull Resources resources) {
        ArrayList<String> strings = new ArrayList<>();
        for (FuelType ft : values()) {
            if (ft == UNKNOWN) {
                continue;
            }
            strings.add(resources.getString(ft.getStringResId()));
        }
        return strings;
    }

    @NonNull
    public static ArrayList<String> getSelectableValues(@NonNull Resources resources) {
        ArrayList<String> strings = new ArrayList<>();
        for (FuelType ft : values()) {
            if (ft == UNKNOWN || ft == ALL) {
                continue;
            }
            strings.add(resources.getString(ft.getStringResId()));
        }
        return strings;
    }

    @NonNull
    public static FuelType fromId(int id) {
        for (FuelType fuelType : values()) {
            if (fuelType.id == id && fuelType != UNKNOWN) {
                return fuelType;
            }
        }
        return UNKNOWN;
    }

    public String getDbFieldName() {
        return dbFieldName;
    }

    public int getId() {
        return id;
    }
}
