package lt.smworks.vehicleguard.fragments;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import lt.smworks.vehicleguard.R;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.about_fragment)
public class AboutFragment extends VehicleGuardFragment {

    @Override
    protected void onCreateView(@NonNull Context context) {

    }
}
