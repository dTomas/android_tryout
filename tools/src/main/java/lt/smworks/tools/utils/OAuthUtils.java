/**
 * @author Martynas Šustavičius
 * @since 2013 08 19
 */
package lt.smworks.tools.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.annotation.SuppressLint;
import android.util.Base64;

public class OAuthUtils {

	public static final String TAG = "OAuthUtils";
	private static final String EMPTY_STRING = "";
	private static final String CARRIAGE_RETURN = "\r\n";
	private static final String UTF8 = "UTF-8";
	private static final String HMAC_SHA256 = "HmacSHA256";
	
	/**
	 * Creates random string used as nonce authorization parameter.
	 * @return Random string.
	 */
	public static String generateNonce() {
		byte[] nonce = new byte[16];
		Random rand = null;
		try {
			rand = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			L.e(TAG,"SHA1PRNG algorithm not supported. Error: %s.", e.toString());
		}
		rand.nextBytes(nonce);
		return new String(nonce);
	}

	/**
	 * @param message - message that needs to be encoded.
	 * @param key - encoding key.
	 * @return Generated MAC encoded with base64.
	 */
	@SuppressLint("InlinedApi")
	public static String sign(String message, String key) {
		try {
			SecretKeySpec keySpec = new SecretKeySpec((key).getBytes(UTF8), HMAC_SHA256);
			Mac mac = Mac.getInstance(HMAC_SHA256);
			mac.init(keySpec);
			byte[] bytes = mac.doFinal(message.getBytes(UTF8));
			return (new String(Base64.encode(bytes, Base64.DEFAULT), "UTF-8")).replace(CARRIAGE_RETURN, EMPTY_STRING);
		} catch (UnsupportedEncodingException e) {
			L.e(TAG,"Unsuported encoding exception. Error: %s.", e.toString());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
}
