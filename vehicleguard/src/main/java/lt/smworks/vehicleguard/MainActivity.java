package lt.smworks.vehicleguard;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import io.fabric.sdk.android.Fabric;
import lt.smworks.tools.BaseActivity;
import lt.smworks.tools.utils.DialogUtils;
import lt.smworks.tools.utils.L;
import lt.smworks.tools.utils.PermissionUtils;
import lt.smworks.utils.LocationHelper;
import lt.smworks.vehicleguard.fragments.AboutFragment;
import lt.smworks.vehicleguard.fragments.VehicleGuardFragment;
import lt.smworks.vehicleguard.fragments.MainFragment;
import lt.smworks.vehicleguard.fragments.SettingsFragment;
import lt.smworks.vehicleguard.views.InputDialog;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = "MainActivity";
    private DrawerLayout drawerLayout;
    private Dialog dialog;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableStrictMode();
        L.getInstance().setup(getFilesDir())
                .setOnLogExceptionListener(Crashlytics::logException);

        Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);
        setContentView(R.layout.main_activity);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }

        drawerLayout = findViewById(R.id.mainActivity_drawerLayout);
        navigationView = findViewById(R.id.mainActivity_navigationView);
        navigationView.setCheckedItem(R.id.mainActivityDrawer_home);
        navigationView.setNavigationItemSelectedListener(this);

        replaceFragment(new MainFragment());
    }

    private void enableStrictMode() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
    }

    public void setDrawerMenuEnabled(boolean state) {
        drawerLayout.setDrawerLockMode(
                state ? DrawerLayout.LOCK_MODE_UNLOCKED : DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(state);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        askForDoNotDisturbPermission();

        if (PermissionUtils.askPermissions(this,
                SEND_SMS, RECEIVE_SMS, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)) {
            BackgroundService.startIfNeeded(getApplicationContext());
        }


        LocationHelper.checkSettings(this, null);
    }

    private void askForDoNotDisturbPermission() {
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !notificationManager.isNotificationPolicyAccessGranted()) {

            dialog = DialogUtils.yesNo(this,
                    getString(R.string.please_enable_notification_services),
                    getString(R.string.yes),
                    getString(R.string.no),
                    yes -> {
                        if (yes) {
                            Intent intent = new Intent(
                                    android.provider.Settings
                                            .ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);

                            startActivity(intent);
                            finish();
                        }
                    });
            dialog.show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void replaceFragment(VehicleGuardFragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        int fragmentCount = manager.getBackStackEntryCount();
        MainFragment mainFragment = new MainFragment();
        manager.popBackStack(mainFragment.getUID(), 0);
        if (fragmentCount > 0 && fragment.getUID().equals(mainFragment.getUID())) {
            return;
        }
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.mainActivity_content, fragment);
        transaction.addToBackStack(fragment.getUID());
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
        } else if (fragmentManager.getBackStackEntryCount() < 2) {
            doubleClickBackToExit(drawerLayout);
        } else {
            navigationView.setCheckedItem(R.id.mainActivityDrawer_home);
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actionbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermissionUtils.REQUEST_ASK_PERMISSION: {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                } else {
                    BackgroundService.startIfNeeded(getApplicationContext());
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (LocationHelper.onActivityResult(this, requestCode, resultCode, data)) {
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mainActivityDrawer_home:
                replaceFragment(new MainFragment());
                break;
            case R.id.mainActivityDrawer_settings:
                replaceFragment(new SettingsFragment());
                break;
            case R.id.mainActivityDrawer_reportIssue:
                new InputDialog().show(getSupportFragmentManager());
                break;
            case R.id.mainActivityDrawer_about:
                replaceFragment(new AboutFragment());
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
