package lt.smworks.gasup.fragments;

import android.Manifest;
import android.net.Uri;

import bolts.Task;
import lt.smworks.gasup.MainActivity;
import lt.smworks.gasup.R;
import lt.smworks.tools.fragments.BaseFragment;
import lt.smworks.tools.models.Size;
import lt.smworks.tools.utils.IOUtils;
import lt.smworks.tools.utils.PermissionUtils;

public abstract class GasupBaseFragment extends BaseFragment<MainActivity> {

    private static final int GAS_STATION_PICTURE_WIDTH = 512;
    private static final int GAS_STATION_PICTURE_HEIGHT = 512;

    @Override
    public MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    protected boolean hasPermissions(String... permissions) {
        if (getContext() == null) {
            return false;
        }
        return PermissionUtils.hasPermissions(getContext(), permissions);
    }

    protected boolean askPermissions(String... permissions) {
        return PermissionUtils.askPermissions(getMainActivity(), permissions);
    }


    protected void resizeAndSaveImage(Uri uri, OnImageSavedListener listener) {
        showProgressDialog(R.string.resizing_image);
        Task.callInBackground(() -> {
            Size size = new Size(GAS_STATION_PICTURE_WIDTH, GAS_STATION_PICTURE_HEIGHT);
            if (hasPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                return IOUtils.resizeBitmapAndGetUri(getMainActivity(), uri, size);
            }
            return null;
        }).continueWith(task -> {
            hideProgressDialog();
            if (task.getResult() != null) {
                if (listener != null) {
                    listener.onImageSaved(task.getResult().toString());
                }
            } else {
                showSnackbarFailure(getString(R.string.unable_to_resize_image));
            }
            return null;
        }, Task.UI_THREAD_EXECUTOR);

    }

    public interface OnImageSavedListener {
        void onImageSaved(String savedImagePath);
    }
}
