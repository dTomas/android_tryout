package lt.smworks.gasup.interfaces;

import android.view.View;
import android.widget.AdapterView;

public interface SpinnerItemSelectedListener extends AdapterView.OnItemSelectedListener {
    @Override
    default void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        onItemSelected(position);
    }

    @Override
    default void onNothingSelected(AdapterView<?> parent) {

    }

    void onItemSelected(int position);
}
