package lt.smworks.gasup.adapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.R;
import lt.smworks.tools.adapters.RecyclerViewAdapter;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.fual_type_adapter_item)
public class FuelTypeAdapter extends RecyclerViewAdapter<String, FuelTypeAdapter.ItemViewHolder> {

    public FuelTypeAdapter(@NonNull Context context) {
        super(context);
    }

    @Override
    protected ItemViewHolder onCreateViewHolder(@NonNull View itemView) {
        return new ItemViewHolder(itemView);
    }

    @Override
    protected void onBindViewHolder(@NonNull ItemViewHolder holder, @NonNull String item, int position) {
        holder.type.setText(item);
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView type;

        private ItemViewHolder(View itemView) {
            super(itemView);
            type = itemView.findViewById(R.id.fuelTypeAdapterItem_fuelType);
        }
    }
}
