//package lt.smworks.gasup.utils;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.hardware.Camera;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.google.android.gms.vision.text.TextRecognizer;
//
//import androidx.annotation.NonNull;
//import lt.smworks.gasup.R;
//import lt.smworks.gasup.ocr.OcrDetectorProcessor;
//import lt.smworks.gasup.ui.camera.CameraSource;
//import lt.smworks.tools.utils.L;
//
//public final class CameraUtils {
//
//    public static final String TAG = "CameraUtils";
//
//    public static CameraSource getCameraSource(@NonNull Activity activity, boolean useFlash) {
//
//        TextRecognizer textRecognizer = new TextRecognizer.Builder(activity).build();
//        textRecognizer.setProcessor(new OcrDetectorProcessor());
//
//        if (!textRecognizer.isOperational()) {
//            // Note: The first time that an app using a Vision API is installed on a
//            // device, GMS will download a native libraries to the device in order to do detection.
//            // Usually this completes before the app is run for the first time.  But if that
//            // download has not yet completed, then the above call will not detect any text,
//            // barcodes, or faces.
//            //
//            // isOperational() can be used to check if the required native libraries are currently
//            // available.  The detectors will automatically become operational once the library
//            // downloads complete on device.
//            L.w(TAG, "Detector dependencies are not yet available.");
//
//            // Check for low storage.  If there is low storage, the native library will not be
//            // downloaded, so detection will not become operational.
//            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
//            boolean hasLowStorage = activity.registerReceiver(null, lowstorageFilter) != null;
//
//            if (hasLowStorage) {
//                Toast.makeText(activity, R.string.low_storage_error, Toast.LENGTH_LONG).show();
//                Log.w(TAG, activity.getString(R.string.low_storage_error));
//            }
//        }
//
//        return new CameraSource.Builder(activity, textRecognizer)
//                .setFacing(CameraSource.CAMERA_FACING_BACK)
//                .setRequestedPreviewSize(1280, 1024)
//                .setRequestedFps(2.0f)
//                .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
//                .setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)
//                .build();
//    }
//}
