package lt.smworks.tools.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import androidx.annotation.WorkerThread;
import lt.smworks.tools.models.Size;

public class IOUtils {

    public static final String TAG = "IOUtils";
    private static final int MEGABYTE_BUFFER = 1024;

    private IOUtils() {

    }

    @WorkerThread
    public static File createFileOnExternalStorage(@NonNull Context context, String fileName) {
        File file = new File(context.getExternalFilesDir(null), fileName);
        if (file.getParentFile().mkdirs()) {
            L.d("Created directories for file: %s", file.toString());
        }
        try {
            if (!file.exists() && !file.createNewFile()) {
                L.w(TAG, "Unable to create file: %s", file.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    @WorkerThread
    @Nullable
    public static File getOrCreateFile(@NonNull File dir, @NonNull String file) {
        try {
            File logFile = new File(dir, file);
            if (!logFile.exists()) {
                if (!logFile.createNewFile()) {
                    return null;
                }
            }
            return logFile;
        } catch (IOException e) {
            return null;
        }
    }

    @WorkerThread
    public static int getLineCount(@NonNull File file) {
        try (FileInputStream fis = new FileInputStream(file);
             InputStream is = new BufferedInputStream(fis)) {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } catch (Exception e) {
            return 0;
        }
    }

    @WorkerThread
    @NonNull
    public static List<String> readLastNLines(@NonNull File file, int lineCount) {
        String[] lines = new String[lineCount];
        int count = 0;
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while ((line = reader.readLine()) != null) {
                lines[count % lines.length] = line;
                count++;
            }
        } catch (Exception e) {
            return new ArrayList<>();
        }
        List<String> list = new ArrayList<>();
        Collections.addAll(list, lines);
        return list;
    }

    @WorkerThread
    public static boolean writeToFile(@NonNull File file, @NonNull List<String> lines) {
        try (BufferedWriter buf = new BufferedWriter(new FileWriter(file, false))) {
            for (String line : lines) {
                buf.append(line);
                buf.newLine();
            }
            buf.flush();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WorkerThread
    public static boolean appendToFile(@NonNull File file, @NonNull String line) {
        try (BufferedWriter buf = new BufferedWriter(new FileWriter(file, true))) {
            buf.append(line);
            buf.newLine();
            buf.flush();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WorkerThread
    public static boolean copyFile(@NonNull File src, @NonNull File dest) {
        try {
            if (!dest.exists() && !dest.createNewFile()) {
                return false;
            }
            if (src.exists()) {
                IOUtils.copy(src, dest);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void copy(File src, File dst) throws IOException {
        try (InputStream in = new FileInputStream(src);
             OutputStream out = new FileOutputStream(dst)) {
            byte[] buf = new byte[MEGABYTE_BUFFER];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (FileNotFoundException e) {
            L.e(e);
        }
    }

    @RequiresPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public static Uri saveBitmapAndGetUri(@NonNull Context context, @NonNull Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String fileName = String.valueOf(System.currentTimeMillis());
        String path = MediaStore.Images.Media.insertImage(
                context.getContentResolver(), bitmap, fileName, null);
        return Uri.parse(path);
    }

    @Nullable
    @WorkerThread
    //@RequiresPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    @SuppressLint("MissingPermission")
    public static Uri resizeBitmapAndGetUri(@NonNull Context context,
                                            @NonNull Uri bitmapUri,
                                            @NonNull Size size) {
        try {
            Bitmap bmp = MediaStore.Images.Media.getBitmap(context.getContentResolver(), bitmapUri);
            Bitmap scaledBitmap = ThumbnailUtils.extractThumbnail(
                    bmp, size.getWidth(), size.getHeight(), ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
            Uri scaledBitmapUri = saveBitmapAndGetUri(context, scaledBitmap);
            scaledBitmap.recycle();

            return getRealPathFromUri(context, scaledBitmapUri);
        } catch (IOException e) {
            L.ex(TAG, e);
        }
        return null;
    }

    private static Uri getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int colIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return Uri.parse(cursor.getString(colIndex));
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
