/**
 * @author Martynas Šustavičius
 * @since 2013 03 26
 */
package lt.smworks.tools.utils;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Rect;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;

import static android.content.Context.AUDIO_SERVICE;

/**
 * Contains all device information.
 */
public class SystemUtils {

    public static final String TAG = "SystemUtils";
    private final Activity activity;

    public SystemUtils(Activity activity) {
        this.activity = activity;
    }


    /**
     * @return Height of the status bar in pixels.
     */
    public int getStatusBarHeight() {
        Rect rectangle = new Rect();
        Window window = this.activity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        return contentViewTop - rectangle.top;
    }

    /**
     * @return Integer containing android version id.
     * Possible values are defined in Build.VERSION_CODES.
     */
    public int getAndroidVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    /**
     * @return Operating system version.
     */
    public String getOSVersion() {
        return android.os.Build.VERSION.RELEASE;
    }

    /**
     * @return Device name.
     */
    public String getDeviceName() {
        return android.os.Build.MODEL;
    }

    /**
     * Sets application language.
     *
     * @param locale - locale of required language.
     *               "en_US" - for United States English.
     *               "lt_LT" - for Lithuanian.
     */
    public void setApplicationLanguage(String locale) {
        Locale loc = new Locale(locale);
        Locale.setDefault(loc);
        Configuration config = activity.getApplicationContext().getResources().getConfiguration();
        config.locale = loc;
        activity.getApplicationContext().getResources().updateConfiguration(config, null);
    }

    /**
     * Method used to enable or disable mobile data.
     *
     * @param state - if true, enables mobile data, otherwise disables.
     * @return True on success.
     * @require android.permission.CHANGE_NETWORK_STATE
     */
    public boolean setMobileDataState(boolean state) {
        final ConnectivityManager conman = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class<?> conmanClass = Class.forName(conman.getClass().getName());
            final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            iConnectivityManagerField.setAccessible(true);
            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
            final Class<?> iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
            final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);
            setMobileDataEnabledMethod.invoke(iConnectivityManager, state);
            return true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return false;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return false;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return false;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Method used to enable or disable WiFi.
     *
     * @param state - if true, enables WiFi, otherwise disables.
     * @require android.permission.ACCESS_WIFI_STATE
     * android.permission.CHANGE_WIFI_STATE
     */
    @RequiresPermission(Manifest.permission.CHANGE_WIFI_STATE)
    public void setWifiState(boolean state) {
        WifiManager wifiManager = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(state);
    }

    /**
     * @return Phone number or empty string.
     * @require android.permission.READ_HONE_STATE
     */
    @RequiresPermission(Manifest.permission.READ_PHONE_STATE)
    public @NonNull
    String getPhoneNumber() {
        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager == null) {
            return "";
        }

        String number = telephonyManager.getLine1Number();
        if (number == null || number.length() == 0) {
            number = telephonyManager.getSubscriberId();
        }

        return number;
    }
//	
//	/**
//	 * On tablets slides open status bar window.
//	 * @param runnable - runnable that will be called when window is opened.
//	 */
//	public void showStatusBar(final Runnable runnable) {
//		activity.getWindow().setFlags(0, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		SM.get(activity).getTM().execute(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					Thread.sleep(400);
//				} catch (InterruptedException ex) {
//					ex.printStackTrace();
//				}
//				activity.runOnUiThread(new Runnable() {
//					@Override
//					public void run() {
//						try {
//							Object service = activity.getSystemService("statusbar");
//							Class<?> statusBarManager = Class.forName("android.app.StatusBarManager");
//							Method expand = statusBarManager.getMethod("expand");
//							expand.invoke(service);
//						} catch (ClassNotFoundException ex) {
//							ex.printStackTrace();
//						} catch (NoSuchMethodException ex) {
//							ex.printStackTrace();
//						} catch (IllegalAccessException ex) {
//							ex.printStackTrace();
//						} catch (IllegalArgumentException ex) {
//							ex.printStackTrace();
//						} catch (InvocationTargetException ex) {
//							ex.printStackTrace();
//						} finally {
//							SM.get(activity).getTM().execute(new Runnable() {
//								@Override
//								public void run() {
//									try {
//										Thread.sleep(400);
//									} catch (InterruptedException ex) {
//										ex.printStackTrace();
//									}
//									activity.runOnUiThread(new Runnable() {
//										@Override
//										public void run() {
//											runnable.run();
//										}
//									});
//									
//								}
//							});
//						}
//					}
//				});
//			}
//		});
//	}

    @RequiresPermission(Manifest.permission.KILL_BACKGROUND_PROCESSES)
    public void killOtherApps() {
        PackageManager pm = activity.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(0);
        ActivityManager am = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ApplicationInfo packageInfo : packages) {
            //if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1) continue;
            if (packageInfo.packageName.equals(activity.getPackageName())) continue;
            if (packageInfo.packageName.equals("net.soti.mobicontrol.samsung")) continue;
            L.d("Killing package: %s.", packageInfo.packageName);
            am.killBackgroundProcesses(packageInfo.packageName);
        }
    }

    @RequiresPermission(Manifest.permission.KILL_BACKGROUND_PROCESSES)
    public void killApp(@NonNull Context context, @NonNull String pckg) {
        if (!PermissionUtils.hasPermissions(context, Manifest.permission.KILL_BACKGROUND_PROCESSES)) {
            return;
        }

        ActivityManager am = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        if (am == null) {
            return;
        }

        am.killBackgroundProcesses(pckg);
    }

    public void isAppActive(@NonNull String pckg) {
        ActivityManager activityManager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager == null) {
            return;
        }

        List<RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for (int i = 0; i < procInfos.size(); i++) {
            if (procInfos.get(i).processName.equals(pckg)) {
                return;
            }
        }
    }

    /**
     * Deletes all SMS.
     *
     * @return Number of deleted SMS messages.
     */
    public int deleteAllSMS() {
        Uri uri = Uri.parse("content://sms");
        ContentResolver contentResolver = activity.getContentResolver();
        Cursor cursor = contentResolver.query(uri, null, null, null, null);
        int count = 0;
        while (cursor.moveToNext()) {
            long thread_id = cursor.getLong(1);
            Uri thread = Uri.parse("content://sms/conversations/" + thread_id);
            count += activity.getContentResolver().delete(thread, null, null);
        }
        cursor.close();
        return count;
    }

    @SuppressLint("MissingPermission")
    @RequiresPermission(Manifest.permission.GET_ACCOUNTS)
    public void removeUserAccounts() {
        AccountManager am = AccountManager.get(activity);
        Account[] accounts = am.getAccounts();
        for (Account accountToRemove : accounts) {
            am.removeAccount(accountToRemove, null, null);
        }
    }

    public String getBuildVersion() {
        String versionName;
        try {
            versionName = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
        return versionName;
    }

    /**
     * Sets screen brightness.
     *
     * @param brightness - 0 - completely dark, 255 - maximum brightness.
     * @NOTE: requires WRITE_SETTINGS permission.
     */
    public void setBrightness(int brightness) {
        if (brightness < 0) brightness = 0;
        else if (brightness > 255) brightness = 255;
        ContentResolver resolver = activity.getApplicationContext().getContentResolver();
        Settings.System.putInt(resolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
        WindowManager.LayoutParams params = activity.getWindow().getAttributes();
        params.screenBrightness = brightness / (float) 255;
        activity.getWindow().setAttributes(params);
    }

    /**
     * Returns screen brightness.
     *
     * @return Screen brightness, where 0 - completely dark, 255 - maximum brightness.
     * @NOTE: requires WRITE_SETTINGS permission.
     */
    public int getBrightness() {
        return Settings.System.getInt(activity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, -1);
    }

    /**
     * Disables auto brightness.
     */
    public void disableAutoBrightness() {
        Settings.System.putInt(activity.getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

    }

    /**
     * @param lineCount - number of lines to show.
     * @param tag       - tag filter.
     * @return Logcat output.
     */
    public String getLogcat(long lineCount, String tag) {
        try {
            Process process = Runtime.getRuntime().exec(String.format("logcat -v time -t %d -s %s", lineCount, tag)); //"logcat -t " + lineCount + (tag == null ? "" : "-s " + tag));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuilder log = new StringBuilder();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line).append("\n");
            }
            return log.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * @return True if device is charging on outlet charger.
     */
    public boolean isChargingOnAC() {
        Intent intent = activity.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        return plugged == BatteryManager.BATTERY_PLUGGED_AC; // || plugged == BatteryManager.BATTERY_PLUGGED_USB;
    }

    /**
     * Checks if application has specified permission.
     *
     * @param permission - permission to be checked. Example: android.permission.WRITE_EXTERNAL_STORAGE
     * @return True if application has this permission.
     */
    public boolean hasPermission(String permission) {
        int res = activity.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * Hides soft keyboard.
     *
     * @param activity - current activity.
     */
    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null) {
            L.w(TAG, "Specified activity to hideSoftKeyboard method is null.");
            return;
        }
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager == null) {
            return;
        }

        View view = activity.getCurrentFocus();
        if (view == null) {
            return;
        }
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static boolean disableVibrationAndSound(@NonNull Context context) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            L.w(TAG, "Notification manager not available");
            return false;
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !notificationManager.isNotificationPolicyAccessGranted()) {
            L.w(TAG, "Unable to disable vibration and sound" +
                    " due to missing ACCESS_NOTIFICATION_POLICY permission");
            return false;
        }

        L.d(TAG, "Disabling vibration and sound");
        AudioManager audioManager = (AudioManager) context.getSystemService(AUDIO_SERVICE);
        if (audioManager == null) {
            L.w(TAG, "Unable to get audio manager");
            return false;
        }

        audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        return true;
    }
}
