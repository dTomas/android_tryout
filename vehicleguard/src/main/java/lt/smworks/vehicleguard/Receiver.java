package lt.smworks.vehicleguard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import com.google.i18n.phonenumbers.PhoneNumberUtil;

import lt.smworks.tools.utils.IntentUtils;
import lt.smworks.tools.utils.L;
import lt.smworks.vehicleguard.models.Action;
import lt.smworks.tools.utils.Settings;
import lt.smworks.vehicleguard.models.SettingTypes;

public class Receiver extends BroadcastReceiver {

    public static final String TAG = "Receiver";
    public static final String KEY_PDUS = "pdus";
    public static final String KEY_FROM = "from";
    public static final String KEY_MESSAGE = "message";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getAction() == null) {
            L.w(TAG, "Received intent with no action in receiver");
            return;
        }

        L.d(TAG, "Received intent:\n%s", IntentUtils.getIntentContent(intent));
        if (intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            handleSms(context, intent.getExtras());
        } else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            L.d(TAG, "Device has just finished booting up");
            BackgroundService.startIfNeeded(context);
        }
    }

    private void handleSms(Context context, Bundle bundle) {
        if (bundle == null || !bundle.containsKey(KEY_PDUS)) {
            L.d(TAG, "Intent bundle is null or doesn't contain key: %s", KEY_PDUS);
            return;
        }
        try {
            String from = "";
            Object[] pdus = (Object[]) bundle.get(KEY_PDUS);
            if (pdus == null) {
                return;
            }
            StringBuilder sb = new StringBuilder();
            for (Object pdu : pdus) {
                SmsMessage message = SmsMessage.createFromPdu((byte[]) pdu);
                from = message.getOriginatingAddress();
                sb.append(message.getMessageBody());
            }

            L.d(TAG, "Received message from: %s. Message: %s", from, sb.toString());
            Bundle extras = new Bundle();
            extras.putString(KEY_FROM, from);
            extras.putString(KEY_MESSAGE, sb.toString());

            Settings settings = new Settings(context);
            PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
            PhoneNumberUtil.MatchType numberMatch = instance.isNumberMatch(
                    from, settings.get(SettingTypes.PHONE_NUMBER));
            if (numberMatch != PhoneNumberUtil.MatchType.EXACT_MATCH
                    && numberMatch != PhoneNumberUtil.MatchType.NSN_MATCH
                    && numberMatch != PhoneNumberUtil.MatchType.SHORT_NSN_MATCH) {
                L.i(TAG, "Phone number in settings doesn't match sender's. %s vs %s",
                        from, settings.get(SettingTypes.PHONE_NUMBER));
                return;
            }

            BackgroundService.action(context, Action.SMS.toString(), extras);
            abortBroadcast();
        } catch (Exception e) {
            L.ex(TAG, e);
        }
    }
}
