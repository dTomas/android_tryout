package lt.smworks.gasup.models.json;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class AmenityJson implements Parcelable {

    private String gasStationId;

    private int amenityTypeId;

    public AmenityJson() {
    }

    protected AmenityJson(Parcel in) {
        gasStationId = in.readString();
        amenityTypeId = in.readInt();
    }

    public static final Creator<AmenityJson> CREATOR = new Creator<AmenityJson>() {
        @Override
        public AmenityJson createFromParcel(Parcel in) {
            return new AmenityJson(in);
        }

        @Override
        public AmenityJson[] newArray(int size) {
            return new AmenityJson[size];
        }
    };

    @NonNull
    public String getGasStationId() {
        return gasStationId;
    }

    public void setGasStationId(String gasStationId) {
        this.gasStationId = gasStationId;
    }

    public void setAmenityTypeId(int amenityTypeId) {
        this.amenityTypeId = amenityTypeId;
    }

    public int getAmenityTypeId() {
        return amenityTypeId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(gasStationId);
        dest.writeInt(amenityTypeId);
    }
}
