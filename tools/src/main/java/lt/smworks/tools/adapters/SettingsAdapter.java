package lt.smworks.tools.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Locale;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.tools.R;
import lt.smworks.tools.models.SettingType;
import lt.smworks.tools.utils.Settings;

public abstract class SettingsAdapter extends RecyclerViewAdapter<SettingType, SettingsAdapter.ItemViewHolder> {

    private final Settings settings;

    public SettingsAdapter(Context context) {
        super(context);
        settings = new Settings(context);
        setupSettingItems();
    }

    protected abstract void setupSettingItems();

    @Override
    protected ItemViewHolder onCreateViewHolder(@NonNull View itemView) {
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        SettingType setting = getItem(position);
        return setting.getLayout();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ItemViewHolder(inflater.inflate(viewType, parent, false), viewType);
    }

    @Override
    protected void onBindViewHolder(@NonNull ItemViewHolder holder, @NonNull SettingType item, int position) {
        int layoutId = (int) holder.itemView.getTag();
        if (layoutId == SettingType.Constants.LAYOUT_DEFAULT) {
            holder.key.setText(context.getString(item.getName()));
            holder.value.setText(settings.get(item));
            holder.valueLayout.setHint(context.getString(item.getHint()));
            if (holder.value.getTag() != null) {
                holder.value.removeTextChangedListener((TextWatcher) holder.value.getTag());
            }
            holder.value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String text = s.toString();
                    int result = item.validate(context, text);
                    if (result == R.string.ok) {
                        settings.set(item, text);
                        holder.valueLayout.setError(null);
                    } else {
                        holder.valueLayout.setError(context.getString(result));
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        } else if (layoutId == SettingType.Constants.LAYOUT_SLIDER) {
            holder.progress.setMax(0);
            holder.progress.setMax(100);
            int progress = settings.getInt(item);
            holder.key.setText(String.format(Locale.US,
                    "%s (%d)", context.getString(item.getName()), progress));
            holder.progress.setProgress(progress);
            holder.progress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    settings.set(item, progress);
                    holder.key.setText(String.format(Locale.US,
                            "%s (%d)", context.getString(item.getName()), progress));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView key;
        TextInputEditText value;
        TextInputLayout valueLayout;
        SeekBar progress;

        ItemViewHolder(View itemView, @LayoutRes int viewType) {
            super(itemView);
            itemView.setTag(viewType);
            key = itemView.findViewById(R.id.settingsItem_key);
            value = itemView.findViewById(R.id.settingsItem_value);
            valueLayout = itemView.findViewById(R.id.settingsItem_valueLayout);
            progress = itemView.findViewById(R.id.settingsItem_progress);
        }
    }
}
