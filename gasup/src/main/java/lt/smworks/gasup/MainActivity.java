package lt.smworks.gasup;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.facebook.login.LoginManager;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import io.fabric.sdk.android.Fabric;
import lt.smworks.gasup.databinding.MainActivityBinding;
import lt.smworks.gasup.dialogs.InputDialog;
import lt.smworks.gasup.fragments.AddGasStationFragment;
import lt.smworks.gasup.fragments.DiscountCardFragment;
import lt.smworks.gasup.fragments.DonateFragment;
import lt.smworks.gasup.fragments.GasStationFragment;
import lt.smworks.gasup.fragments.LoginFragment;
import lt.smworks.gasup.fragments.MainFragment;
import lt.smworks.gasup.fragments.ProfileFragment;
import lt.smworks.gasup.fragments.SettingsFragment;
import lt.smworks.gasup.interfaces.OnPermissionListener;
import lt.smworks.gasup.interfaces.RestService;
import lt.smworks.gasup.models.json.UserJson;
import lt.smworks.tools.BaseActivity;
import lt.smworks.tools.interfaces.ContentFragmentId;
import lt.smworks.tools.utils.IntentUtils;
import lt.smworks.tools.utils.L;
import lt.smworks.tools.utils.PermissionUtils;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

@ContentFragmentId(R.id.mainActivity_fragmentContainer)
public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = "MainActivity";
    //public static final String API_URL = "http://10.0.2.2";
    public static final String API_URL = "https://smworks.herokuapp.com";
    private ActionBarDrawerToggle navigationDrawerToggle;
    private OnPermissionListener permissionListener;
    private RestService restService;
    private Settings settings;
    private MainActivityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restService = initRestService();
        settings = new Settings(getApplicationContext());
        L.getInstance().setup(getFilesDir())
                .setOnLogExceptionListener(Crashlytics::logException);
        Fabric.with(this, new Crashlytics());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        L.d(TAG, "onCreateView() called");
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity);
        setSupportActionBar(binding.toolbar);

        navigationDrawerToggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout,
                binding.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.addDrawerListener(navigationDrawerToggle);
        navigationDrawerToggle.syncState();

        binding.mainActivityNavigationView.setNavigationItemSelectedListener(this);

        openFirstFragment(getIntent());
    }

    private RestService initRestService() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(API_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build().create(RestService.class);
    }

    private void openFirstFragment(Intent intent) {
        clearFragments();
        if (getUser() != null) {
            showNavigationDrawer();
            addFragment(new MainFragment());
            if (intent != null) {
                String action = intent.getAction();
                Uri url = intent.getData();
                if (url != null && IntentUtils.VIEW_INTENT.equals(action)
                        && url.getLastPathSegment() != null) {
                    L.d(TAG, "%s: %s", action, url);
                    addFragment(new GasStationFragment().withParameter(GasStationFragment.GAS_STATION_ID, url.getLastPathSegment()));
                }
            }
        } else {
            hideNavigatonDrawer();
            addFragment(new LoginFragment());
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        clearFragments();
        openFirstFragment(intent);
    }

    @Override
    public void onBackPressed() {
        L.d(TAG, "onBackPressed() called");
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else if (!doubleClickBackToExit(binding.drawerLayout)) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        L.d(TAG, "onNavigationItemSelected(%d) called", item.getItemId());
        int id = item.getItemId();

        if (id == R.id.nav_add_gas_station) {
            addFragment(new AddGasStationFragment());
        } else if (id == R.id.nav_settings) {
            addFragment(new SettingsFragment());
        } else if (id == R.id.nav_discount_cards) {
            addFragment(new DiscountCardFragment());
        } else if (id == R.id.nav_report) {
            new InputDialog().show(getSupportFragmentManager());
        } else if (id == R.id.nav_donate) {
            addFragment(new DonateFragment());
        } else if (id == R.id.nav_logout) {
            LoginManager.getInstance().logOut();
            getSettings().clearUser();
            clearFragments();
            hideNavigatonDrawer();
            addFragment(new LoginFragment());
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public UserJson getUser() {
        return getSettings().getUser();
    }


    public void hideNavigatonDrawer() {
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        navigationDrawerToggle.setDrawerIndicatorEnabled(false);
        navigationDrawerToggle.syncState();
    }

    public void showNavigationDrawer() {
        updateUserInfoInDrawer();
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        navigationDrawerToggle.setDrawerIndicatorEnabled(true);
        navigationDrawerToggle.syncState();
    }

    public void updateUserInfoInDrawer() {
        View headerView = binding.mainActivityNavigationView.getHeaderView(0);
        headerView.setOnClickListener(v -> {
            addFragment(new ProfileFragment());
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        });
        ImageView photo = headerView.findViewById(R.id.navigationDrawer_photo);
        TextView username = headerView.findViewById(R.id.navigationDrawer_username);
        TextView email = headerView.findViewById(R.id.navigationDrawer_email);

        UserJson user = getUser();
        Picasso.loadProfilePicture(photo, user.getAvatarUrl());
        username.setText(user.getUsername());
        email.setText(user.getEmail());
    }

    public void askForPermission(@NonNull String description,
                                 @Nullable OnPermissionListener permissionListener,
                                 @NonNull String... permissions) {
        if (PermissionUtils.hasPermissions(this, permissions)) {
            permissionListener.onPermission(true);
        } else {
            this.permissionListener = permissionListener;
            L.i(TAG, "Camera permission is not granted. Requesting permission");
            for (String permission : permissions) {
                if (!PermissionUtils.isDescriptionRequired(this, permission)) {
                    PermissionUtils.askPermissions(this, permissions);
                    return;
                }
            }

            View.OnClickListener listener = view -> {
                PermissionUtils.askPermissions(this, permissions);
            };

            Snackbar.make(findViewById(android.R.id.content), description,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, listener)
                    .show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermissionUtils.REQUEST_ASK_PERMISSION: {
                if (permissionListener == null) {
                    return;
                }
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    permissionListener.onPermission(false);
                } else {
                    permissionListener.onPermission(true);
                }
            }
        }
    }

    public Uri getFileUri(String filename) {
        return Uri.fromFile(new File(Environment.getExternalStorageDirectory(), filename));
    }

    public RestService getRestService() {
        return restService;
    }

    public Settings getSettings() {
        return settings;
    }
}
