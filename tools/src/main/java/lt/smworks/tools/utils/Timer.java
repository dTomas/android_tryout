package lt.smworks.tools.utils;

public class Timer {

    private final long timeout;
    private long expirationTimestamp;

    public Timer(long timeout) {
        this.timeout = timeout;
        update();
    }

    public void update() {
        expirationTimestamp = System.currentTimeMillis() + timeout;
    }

    public boolean isExpired() {
        return expirationTimestamp < System.currentTimeMillis();
    }
}
