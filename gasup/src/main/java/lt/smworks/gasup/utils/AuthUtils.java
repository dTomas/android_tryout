package lt.smworks.gasup.utils;

import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import lt.smworks.tools.utils.L;

public final class AuthUtils {

    private static final String TAG = "AuthUtils";
    private static final String PERMISSION_EMAIL = "email";
    private static final String PERMISSION_PUBLIC_PROFILE = "public_profile";

    private AuthUtils() {
    }

    /**
     * Make sure to call returned callback manager's onActivityResult method in your fragment/activity
     */
    public static CallbackManager getFacebookAccessToken(@NonNull Fragment fragment,
                                              @NonNull OnFacebookAccessTokenReceiveListener listener) {
        CallbackManager callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                listener.onFacebookAccessTokenReceived(loginResult.getAccessToken(), null);
            }

            @Override
            public void onCancel() {
                L.d(TAG, "Facebook auth cancelled");
            }

            @Override
            public void onError(FacebookException e) {
                listener.onFacebookAccessTokenReceived(null, e);
            }
        });
        LoginManager.getInstance().logInWithReadPermissions(
                fragment, Arrays.asList(PERMISSION_EMAIL, PERMISSION_PUBLIC_PROFILE));
        return callbackManager;
    }

    public interface OnFacebookAccessTokenReceiveListener {
        void onFacebookAccessTokenReceived(AccessToken token, FacebookException e);
    }
}
