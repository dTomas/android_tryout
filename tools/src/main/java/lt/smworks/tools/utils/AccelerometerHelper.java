package lt.smworks.tools.utils;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import lt.smworks.tools.utils.L;

public class AccelerometerHelper implements SensorEventListener {

    public static final String TAG = "AccelerometerHelper";
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private double lastAcceleration;
    private OnAccelerationListener onAccelerationListener;
    private float threshold;
    private boolean isStarted;

    public AccelerometerHelper(Context context, float threshold) {
        this.threshold = threshold;
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager == null) {
            return;
        }
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    public void start() {
        if (sensorManager == null) {
            L.w(TAG, "Accelerometer sensor manager is not available");
            return;
        }
        if (isStarted) {
            return;
        }

        L.d(TAG, "Starting accelerometer sensor with threshold: %f", threshold);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        isStarted = true;
    }

    public void stop() {
        if (!isStarted) {
            return;
        }

        sensorManager.unregisterListener(this);
        isStarted = false;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float[] data = event.values.clone();
            float x = data[0];
            float y = data[1];
            float z = data[2];
            double currentAcceleration = Math.sqrt((double) x * x + y * y + z * z);
            double delta = Math.abs(currentAcceleration - lastAcceleration);
            lastAcceleration = currentAcceleration;
            if (delta > getThreshold()) {
                if (onAccelerationListener != null) {
                    onAccelerationListener.onAcceleration(delta);
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        L.d(TAG, "Accuracy changed to %s", accuracyToString(accuracy));
    }

    private String accuracyToString(int accuracy) {
        switch (accuracy) {
            case 0:
                return "SENSOR_STATUS_UNRELIABLE";
            case 1:
                return "SENSOR_STATUS_ACCURACY_LOW";
            case 2:
                return "SENSOR_STATUS_ACCURACY_MEDIUM";
            case 3:
                return "SENSOR_STATUS_ACCURACY_HIGH";
            case -1:
                return "SENSOR_STATUS_NO_CONTACT";
            default:
                return "Unknown";
        }
    }

    public void setOnAccelerationListener(OnAccelerationListener onAccelerationListener) {
        this.onAccelerationListener = onAccelerationListener;
    }

    public float getThreshold() {
        return threshold;
    }

    public void setThreshold(float threshold) {
        this.threshold = threshold;
    }

    public interface OnAccelerationListener {
        void onAcceleration(double acceleration);
    }
}
