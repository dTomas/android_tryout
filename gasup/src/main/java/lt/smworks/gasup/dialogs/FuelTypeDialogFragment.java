//package lt.smworks.gasup.dialogs;
//
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.os.Bundle;
//import android.os.Handler;
//import android.view.View;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import androidx.annotation.NonNull;
//import androidx.fragment.app.Fragment;
//import androidx.recyclerview.widget.RecyclerView;
//import lt.smworks.gasup.R;
//import lt.smworks.gasup.adapters.FuelTypeAdapter;
//import lt.smworks.gasup.utils.OcrCameraPreview;
//import lt.smworks.tools.dialogs.BaseDialogFragment;
//import lt.smworks.tools.interfaces.Layout;
//import lt.smworks.tools.utils.L;
//import lt.smworks.tools.utils.ListUtils;
//
//@Layout(R.layout.fuel_type_dialog)
//public class FuelTypeDialogFragment extends BaseDialogFragment {
//
//    public static final String TAG = "FuelTypeDialogFragment";
//    public static final String KEY_FUEL_TYPES = "key_fuel_types";
//    private OcrCameraPreview cameraPreview;
//    private FuelTypeAdapter fuelTypeAdapter;
//
//    @Override
//    public Dialog onCreateDialog(View view) {
//        cameraPreview = view.findViewById(R.id.fuelTypeDialog_cameraPreview);
//        cameraPreview.setOnTextListener(this::onTextExtracted);
//        RecyclerView list = view.findViewById(R.id.fuelTypeDialog_list);
//        fuelTypeAdapter = new FuelTypeAdapter(getContext());
//        ListUtils.setupList(list, fuelTypeAdapter);
//
//        view.findViewById(R.id.fuelTypeDialog_next).setOnClickListener(this::next);
//
//
//        return new AlertDialog.Builder(getActivity())
//                .setTitle(R.string.available_fuel_types)
//                .setView(view)
//                .show();
//    }
//
//    private void onTextExtracted(List<String> items) {
//        L.d(TAG, "Extracted text:\n%s", items);
//        fuelTypeAdapter.clear();
//        fuelTypeAdapter.addItems(items);
//    }
//
//    private void next(View view) {
//        Bundle bundle = new Bundle();
//        bundle.putStringArrayList(KEY_FUEL_TYPES, new ArrayList<>(fuelTypeAdapter.getItems()));
//        sendResult(bundle);
//        dismiss();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        cameraPreview.start();
//
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        cameraPreview.stop();
//    }
//
//    public static class Builder {
//
//        private final Fragment parentFragment;
//        private Bundle bundle = new Bundle();
//
//        public Builder(@NonNull Fragment parentFragment) {
//            this.parentFragment = parentFragment;
//        }
//
//        public FuelTypeDialogFragment build() {
//            FuelTypeDialogFragment dialog = new FuelTypeDialogFragment();
//            dialog.setArguments(bundle);
//            dialog.setFragmentManager(parentFragment.getFragmentManager());
//            dialog.setTargetFragment(parentFragment, 0);
//            return dialog;
//        }
//    }
//}
