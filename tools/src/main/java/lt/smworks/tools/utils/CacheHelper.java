package lt.smworks.tools.utils;

import android.util.LruCache;

import androidx.annotation.NonNull;

public class CacheHelper {

    private static final int MAX_SIZE = 1000;
    private LruCache<String, ObjectContainer> cache = new LruCache<>(MAX_SIZE);

    public CacheHelper() {
    }
    
    public void put(@NonNull CacheEntry cacheEntry, Object object) {
        long expirationTime = System.currentTimeMillis() + cacheEntry.getTimeToLive();
        cache.put(cacheEntry.getName(), new ObjectContainer(object, expirationTime));
    }

    @SuppressWarnings("unchecked")
    public <T> T get(@NonNull CacheEntry cacheEntry) {
        ObjectContainer container = cache.get(cacheEntry.getName());
        if (container == null || container.isExpired()) {
            return null;
        }
        return (T) container.getObject();
    }

    public static class CacheEntry {

        private final String name;
        private final long timeToLive;

        public CacheEntry(@NonNull String name, long timeToLive) {
            this.name = name;
            this.timeToLive = timeToLive;
        }

        @NonNull
        public String getName() {
            return name;
        }

        public long getTimeToLive() {
            return timeToLive;
        }
    }

    private static class ObjectContainer {

        private final Object object;
        private final long expirationTime;

        private ObjectContainer(@NonNull Object object, long expirationTime) {
            this.object = object;
            this.expirationTime = expirationTime;
        }

        private boolean isExpired() {
            return expirationTime < System.currentTimeMillis();
        }

        @NonNull
        public Object getObject() {
            return object;
        }
    }
}
