package lt.smworks.vehicleguard.fragments;

import android.os.Bundle;

import lt.smworks.tools.fragments.BaseFragment;
import lt.smworks.tools.utils.Settings;
import lt.smworks.vehicleguard.MainActivity;

public abstract class VehicleGuardFragment extends BaseFragment<MainActivity> {

    private Settings settings;

    public String getUID() {
        return getClass().getName();
    }

    @Override
    public MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @Override
    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        settings = new Settings(getContext());
    }

    public Settings getSettings() {
        return settings;
    }

}
