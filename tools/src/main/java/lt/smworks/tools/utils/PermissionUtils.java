package lt.smworks.tools.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

public final class PermissionUtils {

    public static final int REQUEST_ASK_PERMISSION = 31;

    public static boolean hasPermissions(@NonNull Context context, @NonNull String... permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static boolean askPermissions(@NonNull Activity activity,
                                         @NonNull String... permissions) {
        if (hasPermissions(activity, permissions)) {
            return true;
        }

        ActivityCompat.requestPermissions(activity, permissions, REQUEST_ASK_PERMISSION);
        return false;
    }

    public static boolean isDescriptionRequired(@NonNull Activity activity,
                                                @NonNull String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, permission);
    }
}
