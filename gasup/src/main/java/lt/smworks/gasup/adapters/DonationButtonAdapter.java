package lt.smworks.gasup.adapters;

import android.content.Context;
import android.view.View;

import com.android.billingclient.api.SkuDetails;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.R;
import lt.smworks.gasup.databinding.DonationButtonAdapterItemBinding;
import lt.smworks.tools.adapters.RecyclerViewAdapter;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.donation_button_adapter_item)
public class DonationButtonAdapter extends RecyclerViewAdapter<SkuDetails, DonationButtonAdapter.ItemViewHolder> {

    public DonationButtonAdapter(@NonNull Context context) {
        super(context);
    }

    @Override
    protected ItemViewHolder onCreateViewHolder(@NonNull View itemView) {
        DonationButtonAdapterItemBinding binding = DataBindingUtil.bind(itemView);
        return new ItemViewHolder(Objects.requireNonNull(binding));
    }

    @Override
    protected void onBindViewHolder(@NonNull ItemViewHolder holder,
                                    @NonNull SkuDetails item,
                                    int position) {
        holder.bind(item);
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        private final DonationButtonAdapterItemBinding binding;

        private ItemViewHolder(DonationButtonAdapterItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(SkuDetails item) {
            binding.setModel(item);
            binding.executePendingBindings();
        }
    }
}
