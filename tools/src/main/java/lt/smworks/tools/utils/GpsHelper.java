package lt.smworks.tools.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class GpsHelper {

    private static final String TAG = "GpsHelper";
    private static final int UPDATE_FREQUENCY = 60 * 1000;
    private final Context context;
    private LocationManager locationManager;
    private OnLocationReceiveListener listener;
    private boolean inProgress;

    public GpsHelper(@NonNull Context context) {
        this.context = context;
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager == null) {
            L.e(TAG, "Unable to get location manager");
            return;
        }
        if (!locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)
                || !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            L.e(TAG, "Gps provider not enabled");
            return;
        }

        this.locationManager = locationManager;
    }

    @SuppressLint("MissingPermission")
    public void getLocation(OnLocationReceiveListener listener) {
        if (inProgress) {
            L.w(TAG, "Retrieval of location update is already in progress");
            return;
        }

        if (locationManager == null) {
            L.e(TAG, "Location manager unavailable");
            listener.onLocationReceived(null);
            return;
        }

        if (!PermissionUtils.hasPermissions(context, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)) {
            L.e(TAG, "Missing permissions for location");
            throw new RuntimeException("Missing COARSE and FINE permissions for location");
        }

        inProgress = true;
        this.listener = listener;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_FREQUENCY, 0, gpsListener);
    }

    private LocationListener gpsListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location == null) {
                L.w(TAG, "Received null location");
            } else {
                listener.onLocationReceived(location);
                removeLocationUpdates();
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            L.d(TAG, "Status changed. Provider: %s, status: %sd.", provider, status);
        }

        public void onProviderEnabled(String provider) {
            L.d(TAG, "Location provider enabled");
        }

        public void onProviderDisabled(String provider) {
            if (listener != null) {
                L.w(TAG, "Location provider disabled");
                listener.onLocationReceived(null);
                removeLocationUpdates();
            }
        }
    };

    @SuppressLint("MissingPermission")
    private void removeLocationUpdates() {
        inProgress = false;

        if (!PermissionUtils.hasPermissions(context, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)) {
            L.e(TAG, "Missing permissions for location");
            return;
        }

        locationManager.removeUpdates(gpsListener);
    }

    public interface OnLocationReceiveListener {
        void onLocationReceived(@Nullable Location location);
    }
}
