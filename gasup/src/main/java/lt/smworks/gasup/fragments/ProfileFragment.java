package lt.smworks.gasup.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import lt.smworks.gasup.R;
import lt.smworks.gasup.databinding.ProfileFragmentBinding;
import lt.smworks.gasup.dialogs.UpdateUsernameDialogFragment;
import lt.smworks.gasup.interfaces.RetrofitResult;
import lt.smworks.gasup.models.json.LocationJson;
import lt.smworks.gasup.models.json.UserJson;
import lt.smworks.gasup.utils.RetrofitUtils;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.interfaces.OnDialogResultListener;
import lt.smworks.tools.utils.IntentUtils;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

@Layout(R.layout.profile_fragment)
public class ProfileFragment extends GasupBaseFragment implements OnDialogResultListener {

    private static final String TAG = "ProfileFragment";
    private static final int IMAGE_SIZE = 1024;
    private static final int GALLERY_REQUEST_CODE = 9002;
    private ProfileFragmentBinding binding;

    @Override
    protected void onCreateView(@NonNull Context context) {
        binding = Objects.requireNonNull(DataBindingUtil.bind(getRootView()));
        binding.setModel(getMainActivity().getUser());
        binding.setView(this);
    }

    public void changeProfilePicture() {
        if (hasPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Uri tmpUri = getMainActivity().getFileUri(getMainActivity().getUser().getUserId());
            IntentUtils.GalleryIntentBuilder builder = new IntentUtils.GalleryIntentBuilder()
                    .setAspectX(1)
                    .setAspectY(1)
                    .setCrop(true)
                    .setFormat(Bitmap.CompressFormat.JPEG)
                    .setUri(tmpUri)
                    .setScale(true)
                    .setOutputWidth(IMAGE_SIZE)
                    .setOutputHeight(IMAGE_SIZE);
            IntentUtils.selectPictureFromGallery(this, builder, GALLERY_REQUEST_CODE);
        } else {
            askPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    public void changeUsername() {
        new UpdateUsernameDialogFragment().withTargetFragment(this).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK && intent.getData() != null) {
                    resizeAndSaveImage(intent.getData(), this::uploadAndReplaceImage);
                }
                break;
        }
    }

    private void uploadAndReplaceImage(String savedImagePath) {
        showProgressDialog(R.string.uploading_image);
        UserJson user = getMainActivity().getUser();
        String userId = user.getUserId();
        String token = user.getToken();
        getMainActivity().getRestService()
                .updateAvatar(userId, token, RetrofitUtils.getImageForUpload(savedImagePath))
                .enqueue((RetrofitResult<String>) (data, error) -> {
                    hideProgressDialog();
                    if (error != null) {
                        showError(error);
                        return;
                    }

                    user.setAvatarUrl(data.body());
                    getMainActivity().getSettings().setUser(user);
                    binding.setModel(user);
                    showSnackbarSuccess(getString(R.string.image_uploaded));
                    getMainActivity().updateUserInfoInDrawer();
                });
    }

    @Override
    public void onDialogResult(@NonNull Bundle bundle) {
        String username = bundle.getString(UpdateUsernameDialogFragment.KEY_USERNAME);
        if (TextUtils.isEmpty(username)) {
            return;
        }

        showProgressDialog(R.string.uploading_image);
        UserJson user = getMainActivity().getUser();
        String userId = user.getUserId();
        String token = user.getToken();
        getMainActivity().getRestService().updateUsername(userId, token, username)
                .enqueue((RetrofitResult<String>) (data, error) -> {
                    hideProgressDialog();
                    if (error != null) {
                        showError(error);
                        return;
                    }

                    user.setUsername(username);
                    getMainActivity().getSettings().setUser(user);
                    binding.setModel(user);
                    showSnackbarSuccess(getString(R.string.image_uploaded));
                    getMainActivity().updateUserInfoInDrawer();
                });
    }
}
