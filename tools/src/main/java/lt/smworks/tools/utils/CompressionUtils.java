/**
 * @author Martynas Šustavičius
 * @since 2014 02 20
 */
package lt.smworks.tools.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Class contains methods for file compression.
 */
public class CompressionUtils {

    public static final String TAG = "CompressionUtils";
    private static final int BUFFER = 2048;
    private static final int DOWNLOAD_BUFFER_SIZE = 2048;

    /**
     * Compresses specified file or folder with it's contents into
     * ZIP archive and puts into specified location.
     *
     * @param source      - absolute path to file or folder.
     * @param destination - absolute path to folder where ZIP archive will be saved.
     * @return True on success.
     */
    public static boolean zip(String source, String destination) {
        L.d(TAG, "Compressing %s into %s.", source, destination);
        File path = new File(source);
        if (!path.exists()) {
            L.w(TAG, "Unable to compress from: %s, because location or file not found.", source);
            return false;
        }
        String files[];
        if (path.isFile()) {
            files = new String[]{source};
        } else {
            File fileArr[] = path.listFiles();
            if (fileArr == null) {
                L.w(TAG, "Unable to compress. Folder content is empty.");
                return false;
            }
            files = new String[fileArr.length];
            int c = 0;
            for (File file : fileArr) {
                files[c++] = file.getAbsolutePath();
            }
        }
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(destination);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
            byte data[] = new byte[BUFFER];
            for (int i = 0; i < files.length; i++) {
                L.d(TAG, "Adding: %s.", files[i]);
                FileInputStream fi = new FileInputStream(files[i]);
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(files[i].substring(files[i].lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }
            out.close();
            L.d(TAG, "Successfuly compressed %s into %s.", source, destination);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Extracts specified ZIP file content into specified folder.
     * If file already exists there, it will be overwritten.
     *
     * @param source      - absolute path to file or folder.
     * @param destination - absolute path to folder where ZIP archive will be saved.
     * @param name        - if not null, only file with this name will be extracted.
     * @return True on success.
     */
    public static boolean unzip(String source, String destination, String name) {
        L.d(TAG, "Extracting %s archive into %s folder.", source, destination);
        if (!new File(source).isFile()) {
            L.w(TAG, "File \"%s\" is not a valid file.", source);
            return false;
        }
        File dest = new File(destination);
        if (!dest.exists()) {
            dest.mkdirs();
        }
        ZipInputStream zin = null;
        try {
            zin = new ZipInputStream(new FileInputStream(source));
            ZipEntry ze = null;
            byte[] buffer = new byte[DOWNLOAD_BUFFER_SIZE];
            while ((ze = zin.getNextEntry()) != null) {
                if (name != null && !ze.getName().equals(name)) {
                    continue;
                }
                L.d(TAG, "Extracting: %s.", ze.getName());
                if (ze.isDirectory()) {
                    File file = new File(destination + "/" + ze.getName());
                    if (!file.isDirectory()) {
                        file.mkdirs();
                    }
                } else {
                    FileOutputStream fout = new FileOutputStream(destination + "/" + ze.getName());
                    for (int c = zin.read(buffer); c != -1; c = zin.read(buffer)) {
                        fout.write(buffer, 0, c);
                    }
                    zin.closeEntry();
                    fout.close();
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (zin != null) {
                try {
                    zin.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Extracts specified ZIP file content into specified folder.
     * If file already exists there, it will be overwritten.
     *
     * @param source      - absolute path to file or folder.
     * @param destination - absolute path to folder where ZIP archive will be saved.
     * @return True on success.
     */
    public static boolean unzip(String source, String destination) {
        return unzip(source, destination, null);
    }
}
