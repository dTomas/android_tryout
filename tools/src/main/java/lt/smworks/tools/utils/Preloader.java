/**
 * @author Martynas Šustavičius
 * @since 2013 03 18
 */
package lt.smworks.tools.utils;

import android.view.View;
import android.view.animation.Animation;

/**
 * This class provides simple methods
 * to implement preloader functionality
 * for all views that arent loaded instantly.
 */
public class Preloader {

	/**
	 * Shows animated preloader while original view is loaded.
	 * @param container - view containing preloader and content views.
	 * @param preloader - preloader view resource id.
	 * @param content - original view resource id, that will be loaded later.
	 * @param animation - animation for preloader.
	 */
	public static void start(View container, int preloader, int content, Animation animation)	{
		View preloaderView = container.findViewById(preloader);
		preloaderView.setVisibility(View.VISIBLE);
		preloaderView.startAnimation(animation);
		container.findViewById(content).setVisibility(View.GONE);
	}
	
	/**
	 * Stops preloader animation and shows loaded content.
	 * @param container - view containing preloader and content views.
	 * @param preloader - preloader view resource id.
	 * @param content - original view resource id, that is already loaded.
	 */
	public static void stop(View container, int preloader, int content) {
		View preloaderView = container.findViewById(preloader);
		preloaderView.setAnimation(null);
		preloaderView.setVisibility(View.GONE);
		container.findViewById(content).setVisibility(View.VISIBLE);
	}
}
