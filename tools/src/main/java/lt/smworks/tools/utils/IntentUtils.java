package lt.smworks.tools.utils;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

import java.io.File;

import static android.Manifest.permission.CALL_PHONE;

/**
 * Provides methods to launch other applications to handle specific tasks.
 */
public final class IntentUtils {

    public static final String TAG = "IntentUtils";
    public static final String OUTPUT_FORMAT = "outputFormat";
    public static final String SCALE = "scale";
    public static final String ASPECT_Y = "aspectY";
    public static final String ASPECT_X = "aspectX";
    public static final String OUTPUT_Y = "outputY";
    public static final String OUTPUT_X = "outputX";
    public static final String CROP = "crop";
    public static final String IMAGE = "image/*";
    public static final String VAL_TRUE = "true";
    public static final String VAL_FALSE = "false";
    public static final String VIEW_INTENT = "android.intent.action.VIEW";

    private IntentUtils() {

    }

    public static void openMaps(@NonNull Context context, double lat, double lon) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("https://www.google.com/maps/search/?api=1&query=" + lat + "," + lon));
        context.startActivity(intent);
    }

    /**
     * Brings up phone window and calls to specified number.
     *
     * @param context - application context.
     * @param number  - phone number.
     */
    @SuppressLint("MissingPermission")
    public static boolean call(Context context, String number) {
        if (!PermissionUtils.hasPermissions(context, CALL_PHONE)) {
            return false;
        }
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
        context.startActivity(intent);
        return true;
    }

    /**
     * Launches default SMS application with specified message content.
     *
     * @param context - application context.
     * @param number  - phone number.
     * @param message - SMS message.
     */
    public static void writeSMS(Context context, String number, String message) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + number));
        sendIntent.putExtra("sms_body", message);
        context.startActivity(sendIntent);
    }

    public static boolean sendEmail(@NonNull Context context,
                                    @NonNull Intent intent,
                                    @StringRes int chooserTitle) {
        try {
            context.startActivity(Intent.createChooser(intent, context.getString(chooserTitle)));
            return true;
        } catch (ActivityNotFoundException ex) {
            L.e(ex);
            return false;
        }
    }

    /**
     * @param context - application context.
     * @param url     - web page URL to be opened in default browser.
     */
    public static void openURL(Context context, String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }

    /**
     * Opens home screen. This intent can be used to simulate home button.
     *
     * @param context - application context.
     */
    public static void openHomeScreen(Context context) {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    /**
     * Opens device settings window.
     *
     * @param context - application context.
     */
    public static void openSettings(Context context) {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        context.startActivity(intent);
    }

    /**
     * Opens device dialer application.
     *
     * @param context - application context.
     */
    public static void openDialer(Context context) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        context.startActivity(intent);
    }

    /**
     * Opens default email application without asking user to choose from any available.
     *
     * @param context - application context.
     * @param email   - optional receiver email. Specify null if not needed.
     * @param subject - optional subject of email. Specify null if not needed.
     * @param body    - optional email text. Specify null if not needed.
     */
    public static void openDefaultEmail(Context context, String email, String subject, String body) {
        //	Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.android.email");
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        if (email != null) {
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        }
        if (subject != null) {
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        }
        if (body != null) {
            intent.putExtra(Intent.EXTRA_TEXT, body);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(Intent.createChooser(intent, "Send mail..."));
    }

    /**
     * Opens default camera application without asking user to choose from any available.
     *
     * @param context - application context.
     */
    public static void openDefaultCamera(Context context) {
        Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            PackageManager pm = context.getPackageManager();
            ResolveInfo mInfo = pm.resolveActivity(i, 0);
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(mInfo.activityInfo.packageName, mInfo.activityInfo.name));
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(intent);
        } catch (Exception e) {
            L.w(TAG, "Unable to launch camera. Error: %s.", e);
        }
    }

    public static void takePicture(@NonNull Fragment fragment,
                                   @NonNull Uri pathUri,
                                   int requestCode) {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, pathUri);
        fragment.startActivityForResult(takePicture, requestCode);
    }

    public static void selectPictureFromGallery(@NonNull Fragment fragment,
                                                @NonNull GalleryIntentBuilder builder,
                                                int requestCode) {
        fragment.startActivityForResult(builder.build(), requestCode);
    }

    public static class GalleryIntentBuilder {

        private final Intent intent;

        public GalleryIntentBuilder() {
            this.intent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
            intent.setType(IMAGE);
        }

        public GalleryIntentBuilder setUri(@NonNull Uri uri) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            return this;
        }

        public GalleryIntentBuilder setCrop(boolean state) {
            intent.putExtra(CROP, state ? VAL_TRUE : VAL_FALSE);
            return this;
        }

        public GalleryIntentBuilder setOutputWidth(int width) {
            intent.putExtra(OUTPUT_X, width);
            return this;
        }

        public GalleryIntentBuilder setOutputHeight(int height) {
            intent.putExtra(OUTPUT_Y, height);
            return this;
        }

        public GalleryIntentBuilder setAspectX(int value) {
            intent.putExtra(ASPECT_X, value);
            return this;
        }

        public GalleryIntentBuilder setAspectY(int value) {
            intent.putExtra(ASPECT_Y, value);
            return this;
        }

        public GalleryIntentBuilder setScale(boolean state) {
            intent.putExtra(SCALE, state);
            return this;
        }

        public GalleryIntentBuilder setFormat(
                @NonNull Bitmap.CompressFormat compressFormat) {
            intent.putExtra(OUTPUT_FORMAT, compressFormat.toString());
            return this;
        }

        public Intent build() {
            return intent;
        }
    }

    /**
     * Open specified application if it exists.
     *
     * @param context - application context.
     * @param name    - package name, like: "com.skype.raider".
     * @return True if application was found.
     */
    public static boolean openApp(Context context, String name) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(name);
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            return true;
        }
        return false;
    }

    /**
     * Open specified application if it exists.
     *
     * @param context - application context.
     * @param data    - data that lets you launch specific application with parameters.
     * @return True if application was found.
     */
    public static boolean openApp(Context context, Uri data) {
        try {
            Intent intent = new Intent(VIEW_INTENT);
            intent.setData(data);
            context.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }

    }

    /**
     * @param context   - activity or application context.
     * @param latitude  - horizontal position.
     * @param longitude - vertical position.
     */
    public static void openNavigation(Context context, String latitude, String longitude) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + latitude + "," + longitude));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            L.e(e);
        }
    }

    /**
     * Goes to market to search for specified package.
     *
     * @param context - application context.
     * @param name    - package name, like: "com.skype.raider".
     */
    public static void openMarket(Context context, String name) {
        Uri marketUri = Uri.parse("market://details?id=" + name);
        Intent intent = new Intent(Intent.ACTION_VIEW, marketUri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            L.e(e);
        }
    }

    /**
     * Deletes specified application.
     *
     * @param name - package name, like: "com.skype.raider".
     */
    public static void deleteApp(Context context, String name) {
        try {
            context.getPackageManager().getApplicationInfo(name, 0);
            Intent localIntent = new Intent("android.intent.action.DELETE");
            localIntent.setData(Uri.parse("package:" + name));
            context.startActivity(localIntent);
        } catch (NameNotFoundException e) {
            L.w(TAG, "Unable to delete application: %s. Error: %s.", name, e.toString());
        }
    }

    /**
     * Stores intent to be sent at specified time.
     *
     * @param context - application context.
     * @param intent  - intent with specified broadcast receiver class and action.
     * @param time    - time intent should be launched.
     */
    public static void setAlarmIntent(Context context, Intent intent, long time) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
        }
    }

    /**
     * Cancels stored alarm intent.
     *
     * @param context       - application context.
     * @param receiverClass - receiver class that should have been called by stored intent.
     */
    public static void cancelAlarmIntent(Context context, Class<?> receiverClass) {
        Intent intent = new Intent(context, receiverClass);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.cancel(sender);
        }
    }

    /**
     * Sends intent to specified receiver.
     *
     * @param context - application context.
     * @param to      - class that receives intent.
     * @param action  - intent action.
     * @param params  - bundle with additional parameters.
     */
    public static void sendIntent(Context context, Class<?> to, String action, Bundle params) {
        Intent i = new Intent(context, to);
        if (params != null) {
            i.putExtras(params);
        }
        i.setAction(action);
        context.sendBroadcast(i);
    }

    public static PendingIntent getBroadcastPendingIntent(@NonNull Context context,
                                                          @NonNull String action, Class<?> cl) {
        Intent newIntent = new Intent(context, cl);
        newIntent.setAction(action);
        return PendingIntent.getBroadcast(
                context, 0, newIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static PendingIntent getServicePendingIntent(@NonNull Context context,
                                                        @NonNull String action,
                                                        Class<?> cl) {
        Intent newIntent = new Intent(context, cl);
        newIntent.setAction(action);
        return PendingIntent.getService(
                context, 0, newIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    @NonNull
    public static String getIntentContent(@NonNull Intent intent) {
        Bundle bundle = intent.getExtras();
        StringBuilder sb = new StringBuilder();
        sb.append("Action: ").append(intent.getAction());
        if (bundle != null) {
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                String val = "";
                String cls = "";
                if (value != null) {
                    val = value.toString();
                    cls = value.getClass().getName();
                }
                sb.append(String.format("\n%s: %s (%s)", key, val, cls));

            }
        }
        return sb.toString();
    }

    @NonNull
    public static Intent getShareLinkIntent(@NonNull String url) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, url);
        return intent;
    }

    public static class EmailIntentBuilder {

        private static final String EMAIL_TYPE = "message/rfc822";
        private String recipient;
        private String message;
        private String subject;
        private File attachment;

        public EmailIntentBuilder setRecipent(@NonNull String recipient) {
            this.recipient = recipient;
            return this;
        }

        public EmailIntentBuilder setMessage(@NonNull String message) {
            this.message = message;
            return this;
        }

        public EmailIntentBuilder setSubject(@NonNull String subject) {
            this.subject = subject;
            return this;
        }

        public EmailIntentBuilder setAttachment(@NonNull File attachment) {
            this.attachment = attachment;
            return this;
        }

        public Intent build() {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType(EMAIL_TYPE);
            if (recipient != null) {
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{recipient});
            }
            if (subject != null) {
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            }
            if (message != null) {
                intent.putExtra(Intent.EXTRA_TEXT, message);
            }
            if (attachment != null) {
                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(attachment));
            }
            return intent;
        }
    }

}
