package lt.smworks.tools.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import lt.smworks.tools.models.SettingType;

public final class Settings {

    public static final String VAL_TRUE = "true";
    public static final String VAL_FALSE = "false";
    private static final String KEY = "smworks-shared-prefs";

    private final SharedPreferences sharedPreferences;

    public Settings(@NonNull Context context) {
        sharedPreferences = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
    }

    public boolean isSet(SettingType key) {
        return sharedPreferences.contains(key.getKey());
    }

    public void set(SettingType key, String value) {
        setString(key.getKey(), value);
    }

    public void set(SettingType key, int value) {
        setString(key.getKey(), String.valueOf(value));
    }

    public String get(SettingType key) {
        return getString(key.getKey(), key.getDefaultValue());
    }

    public float getFloat(SettingType key) {
        return Float.valueOf(get(key));
    }

    public int getInt(SettingType key) {
        String value = get(key);
        return Integer.valueOf(TextUtils.isEmpty(value) ? key.getDefaultValue() : value);
    }

    private void setString(String key, String value) {
        L.d("Settings", "Setting key %s to value %s", key, value);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    @NonNull
    private String getString(String key, String defValue) {
        return sharedPreferences.getString(key, defValue);
    }
}
