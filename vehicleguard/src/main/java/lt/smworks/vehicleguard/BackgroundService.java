package lt.smworks.vehicleguard;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import lt.smworks.tools.models.Pair;
import lt.smworks.tools.models.SettingType;
import lt.smworks.tools.utils.AccelerometerHelper;
import lt.smworks.tools.utils.BatteryHelper;
import lt.smworks.tools.utils.GpsHelper;
import lt.smworks.tools.utils.IntentUtils;
import lt.smworks.tools.utils.L;
import lt.smworks.tools.utils.NotificationBuilder;
import lt.smworks.tools.utils.Settings;
import lt.smworks.tools.utils.SmsHelper;
import lt.smworks.tools.utils.SystemUtils;
import lt.smworks.tools.utils.Timer;
import lt.smworks.tools.utils.WakeLockHelper;
import lt.smworks.utils.ActivityRecognitionHelper;
import lt.smworks.utils.GeofenceHelper;
import lt.smworks.utils.LocationHelper;
import lt.smworks.vehicleguard.models.Action;
import lt.smworks.vehicleguard.models.SettingTypes;
import lt.smworks.vehicleguard.models.SmsAction;

public class BackgroundService extends Service {

    public static final String TAG = "BackgroundService";
    public static final int UID = 1;
    public static final String INITIAL_LOCATION = "initial_location";
    public static final float INITIAL_LOCATION_RADIUS = 100.f;
    public static final String SERVICE_UPDATE = "service_update";
    public static final String ACCELERATION_UPDATE = "acceleration_update";
    public static final String COORDINATE_UPDATE = "coordinate_update";
    private static final Timer SERVICE_INACTIVITY_TIMER = new Timer(3 * 60 * 1000);
    private static final Timer LOCATION_INACTIVITY_TIMER = new Timer(5 * 60 * 1000);
    private static final Timer ACCELERATION_TIMER = new Timer(500);
    private static final Timer ACCELERATION_NOTIFICATION_TIMER = new Timer(2 * 60 * 1000);
    public static final String ACTIVITY_UPDATE = "activity_update";
    public static final String VEHICLE_GUARD_WAKE_LOCK = "VehicleGuard wake lock";
    public static final String NOTIFICATION_CHANNEL_ID = "VehicleGuardBackgroundServiceNotification";
    private static final float MIN_MOVEMENT_DELTA_IN_METERS = 20.0f;

    private BatteryHelper batteryHelper;
    private ActivityRecognitionHelper activityRecognitionHelper;
    private LocationHelper locationHelper;
    private GeofenceHelper geofenceHelper;
    private AccelerometerHelper accelerometerHelper;
    private SmsHelper smsHelper;
    private WakeLockHelper wakeLockHelper;
    private Location lastLocationUpdate;
    private boolean batteryBelow50 = false;
    private boolean batteryBelow25 = false;
    private boolean batteryBelow10 = false;
    private double lastAccelerationValue;
    private boolean isRunning = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        L.d(TAG, "onCreate() CALLED FOR BACKGROUND SERVICE");
        batteryHelper = new BatteryHelper(this);
        batteryHelper.setBatteryStatusChangeListener(this::onBatteryStatus);
        PendingIntent locationIntent = IntentUtils.getServicePendingIntent(this,
                Action.LOCATION_UPDATE.toString(), BackgroundService.class);
        locationHelper = new LocationHelper(this, locationIntent);
        PendingIntent activityIntent = IntentUtils.getServicePendingIntent(this,
                Action.ACTIVITY_RECOGNITION.toString(), BackgroundService.class);
        activityRecognitionHelper = new ActivityRecognitionHelper(this, activityIntent);
        PendingIntent geofenceIntent = IntentUtils.getServicePendingIntent(this,
                Action.GEOFENCE.toString(), BackgroundService.class);
        geofenceHelper = new GeofenceHelper(this, geofenceIntent);
        geofenceHelper.setOnGeofenceLocationReceivedListener(this::updateLocationOnUI);
        accelerometerHelper = new AccelerometerHelper(this,
                new Settings(this).getFloat(SettingTypes.SENSOR_SENSITIVITY) * 0.1f);
        accelerometerHelper.setOnAccelerationListener(this::onAcceleration);
        smsHelper = new SmsHelper(this);
        wakeLockHelper = new WakeLockHelper(this, VEHICLE_GUARD_WAKE_LOCK);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null || intent.getAction() == null) {
            L.w(TAG, "Starting background service with null action");
            return START_STICKY;
        }

        switch (Action.fromString(intent.getAction())) {
            case START:
                handleStart();
                break;
            case STOP:
                handleStop();
                break;
            case SMS:
                handleSms(intent.getExtras());
                break;
            case ACTIVITY_RECOGNITION:
                handleActivityRecognition(intent);
                break;
            case LOCATION_UPDATE:
                handleLocationUpdate(intent);
                break;
            case GEOFENCE:
                handleGeofence(intent);
                break;
            case UNKNOWN:
                L.w(TAG, "Received unknown action: %s", intent.getAction());
                break;
            default:
                L.w(TAG, "Received command with not recognised action");
                L.d(TAG, "%s", IntentUtils.getIntentContent(intent));
                break;
        }

        return START_STICKY;
    }

    private void handleStart() {
        L.d(TAG, "Received start action");
        SystemUtils.disableVibrationAndSound(this);
        SERVICE_INACTIVITY_TIMER.update();
        LOCATION_INACTIVITY_TIMER.update();
        ACCELERATION_NOTIFICATION_TIMER.update();
        smsHelper.start();
        batteryHelper.start();
        activityRecognitionHelper.start();
        geofenceHelper.add(INITIAL_LOCATION, INITIAL_LOCATION_RADIUS);
        accelerometerHelper.start();
        lastLocationUpdate = null;
        @SuppressLint("ResourceType") NotificationBuilder builder = new NotificationBuilder(this)
                .setMainActivityClass(MainActivity.class)
                .setIntentAction(Action.START.toString())
                .setIcon(R.drawable.ic_stat_logo)
                .setSmallIcon(R.drawable.ic_stat_logo)
                .setTitle(R.string.notification_title)
                .setTicker(R.string.notification_ticker)
                .setContentText(R.string.notification_description)
                .setNotificationChannelId(NOTIFICATION_CHANNEL_ID)
                .setNotificationChannelName(R.string.notification_channel_name);
        startForeground(UID, builder.build());
        sendSMS(R.string.this_phone_is_configured);
        isRunning = true;
    }

    private void handleStop() {
        L.d(TAG, "Received stop action");
        accelerometerHelper.stop();
        geofenceHelper.stop();
        batteryHelper.stop();
        activityRecognitionHelper.stop();
        locationHelper.stop();
        sendSMS(R.string.app_is_not_running);
        smsHelper.stop();
        isRunning = false;
        stopForeground(true);
        stopSelf();
        L.d(TAG, "Background service stopped");
    }

    private void handleSms(Bundle extras) {
        L.d(TAG, "Received SMS action");
        if (extras == null) {
            L.w(TAG, "No extras provided for SMS handler");
            return;
        }

        String from = extras.getString(Receiver.KEY_FROM);
        String message = extras.getString(Receiver.KEY_MESSAGE);

        if (message == null) {
            L.i(TAG, "Received empty message from: %s", from);
            return;
        }

        switch (SmsAction.fromMessage(message)) {
            case SEND_LOCATION:
                new GpsHelper(this).getLocation(location -> {
                    L.d(TAG, "Received last known location");
                    if (isRunning) {
                        sendLocationUpdate(location);
                    } else {
                        L.d(TAG, "Ignoring location update because process is stopped");
                    }
                });
                break;
            case START:
                if (!isRunning) {
                    L.d(TAG, "Starting background service");
                    new Settings(getBaseContext()).set(SettingTypes.SERVICE_STARTED, Settings.VAL_TRUE);
                    handleStart();
                } else {
                    L.d(TAG, "Background service is already running. Ignoring START");
                }
                break;
            case UNKNOWN:
                L.i(TAG, "Received unrelated message from: %s", from);
        }
    }

    private void handleActivityRecognition(Intent intent) {
        if (!SERVICE_INACTIVITY_TIMER.isExpired()) {
            L.i(TAG, "Ignoring activity recognition update, because service is just started");
            return;
        }
        if (ActivityRecognitionHelper.hasActivityResult(intent)) {
            Pair<Integer, Integer> pair = ActivityRecognitionHelper.getActivityResult(intent);
            if (pair != null) {
                int activityType = pair.getFirst();
                int confidence = pair.getSecond();
                String activity = ActivityRecognitionHelper.activityTypeToString(activityType);
                L.d(TAG, "Received type: %s. Confidence: %d", activity, confidence);
                updateActivityTypeOnUI(activity);
                if (ActivityRecognitionHelper.isMovementStarted(activityType)) {
                    locationHelper.start();
                } else if (ActivityRecognitionHelper.isMovementStopped(activityType)) {
                    if (LOCATION_INACTIVITY_TIMER.isExpired()) {
                        if (confidence >= 99 && locationHelper.isStarted()) {
                            L.d(TAG, "Stopping location updates because of still activity");
                            locationHelper.stop();
                            wakeLockHelper.release();
                        }
                    } else {
                        L.d(TAG, "Detected still activity, but not enough time has passed " +
                                "since last location update");
                    }
                }
            }
        }
        if (ActivityRecognitionHelper.hasActivityTransitionResult(intent)) {
            L.d(TAG, "Received activity transition event");
            List<Pair<Integer, Integer>> results = ActivityRecognitionHelper.getActivityTransitionResults(intent);
            if (results.isEmpty()) {
                L.w(TAG, "Activity transition result is null");
                return;
            }
            StringBuilder sb = new StringBuilder();
            boolean started = false;
            boolean stopped = false;
            for (Pair<Integer, Integer> result : results) {
                String transition = ActivityRecognitionHelper.activityTransitionToString(
                        result.getFirst());
                String activity = ActivityRecognitionHelper.activityTypeToString(
                        result.getSecond());
                L.d(TAG, "Transition type: %s, activity type: %s", transition, activity);
                sb.append("Activity: ")
                        .append(activity).append(" - ").append(transition).append("\n");
                if (ActivityRecognitionHelper.isMovementStarted(result.getFirst(), result.getSecond())) {
                    started = true;
                }
                if (ActivityRecognitionHelper.isMovementStopped(result.getFirst(), result.getSecond())) {
                    stopped = true;
                    wakeLockHelper.release();
                }
            }
            L.d(TAG, sb.toString());
            if (started) {
                locationHelper.start();
            } else if (stopped) {
                locationHelper.stop();
            }
        }
    }

    private void handleLocationUpdate(Intent intent) {
        L.d(TAG, "Received location update");
        Boolean locationAvailability = LocationHelper.getLocationAvailability(intent);
        if (locationAvailability != null) {
            L.d(TAG, "Location availability: %b", locationAvailability);
            return;
        }

        if (intent.getExtras() == null) {
            L.w(TAG, "Intent extras are null");
            return;
        }

        if (LocationHelper.hasResult(intent)) {
            Location location = LocationHelper.extractLocation(intent);
            if (lastLocationUpdate != null
                    && location.distanceTo(lastLocationUpdate) > MIN_MOVEMENT_DELTA_IN_METERS) {
                L.d(TAG, "Updating location inactivity timer, because received distance " +
                                "is more than %.2f meters. Distance: %.2f",
                        MIN_MOVEMENT_DELTA_IN_METERS,
                        location.distanceTo(lastLocationUpdate));
                LOCATION_INACTIVITY_TIMER.update();
                sendLocationUpdate(location);
            } else {
                L.i(TAG, "Ignoring location update because no relevant movement detected");
            }
            lastLocationUpdate = location;
        } else {
            L.w(TAG, "No location point is provided");
        }
    }

    private void handleGeofence(Intent intent) {
        L.d(TAG, "Received geofence update action\n%s", IntentUtils.getIntentContent(intent));
        int transition = GeofenceHelper.getGeofenceTransition(intent);
        L.d(TAG, "Geofence transition type: %s", GeofenceHelper.transitionToString(transition));
        locationHelper.start();
    }

    private void sendLocationUpdate(@NonNull Location location) {
        updateLocationOnUI(location);
        sendSMS(R.string.location_update_message,
                location.getLatitude(), location.getLongitude(),
                location.getProvider(), location.getAccuracy());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        L.d(TAG, "onDestroy() CALLED FOR BACKGROUND SERVICE");
        isRunning = false;
        accelerometerHelper.stop();
        geofenceHelper.stop();
        activityRecognitionHelper.stop();
        batteryHelper.stop();
        locationHelper.stop();
        wakeLockHelper.release();
    }

    public static void startIfNeeded(Context context) {
        L.d(TAG, "startIfNeeded() called for BackgroundService");
        Handler handler = new Handler();
        new Thread(() -> {
            Settings settings = new Settings(context);
            if (isNotRunning(context) && settings.get(SettingTypes.SERVICE_STARTED).equals(Settings.VAL_TRUE)) {
                handler.post(() -> {
                    Intent intent = new Intent(context, BackgroundService.class);
                    intent.setAction(Action.START.toString());
                    startForegroundService(context, intent);
                });
            }

        }).start();
    }

    private static void startForegroundService(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    public static void stop(Context context) {
        L.d(TAG, "stop() called for BackgroundService");
        Handler handler = new Handler();
        new Thread(() -> {
            if (isNotRunning(context)) {
                L.w(TAG, "Ignoring stop() because BackgroundService is not running");
            } else {
                handler.post(() -> {
                    Intent intent = new Intent(context, BackgroundService.class);
                    intent.setAction(Action.STOP.toString());
                    startForegroundService(context, intent);
                });
            }
        }).start();
    }

    public static boolean isNotRunning(@NonNull Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager == null) {
            return true;
        }
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (BackgroundService.class.getName().equals(service.service.getClassName())) {
                return false;
            }
        }
        return true;
    }

    public static void action(Context context, String action, Bundle extras) {
        L.d(TAG, "action() called for BackgroundService");
        Handler handler = new Handler();
        new Thread(() -> {
            if (isNotRunning(context)) {
                L.w(TAG, "Ignoring action() because BackgroundService is not running");
            } else {
                handler.post(() -> {
                    Intent intent = new Intent(context, BackgroundService.class);
                    intent.setAction(action);
                    intent.putExtras(extras);
                    startForegroundService(context, intent);
                });

            }
        }).start();
    }

    private void sendSMS(@StringRes int stringResource, Object... args) {
        if (getBaseContext() == null) {
            L.w(TAG, "Service is destroyed");
            return;
        }

        Locale locale;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            locale = getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = getResources().getConfiguration().locale;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("\n(dd HH:mm)", locale);

        smsHelper.sendSMS(new Settings(getBaseContext()).get(SettingTypes.PHONE_NUMBER),
                String.format(Locale.US, getString(stringResource), args)
                        + dateFormat.format(Calendar.getInstance().getTime()));
    }

    private void onAcceleration(double acceleration) {
        updateAccelerationOnUI(acceleration);
        if (!ACCELERATION_NOTIFICATION_TIMER.isExpired() || locationHelper.isStarted()) {
            return;
        }

        L.d(TAG, "Acceleration detected: %.2f", acceleration);

        if (!ACCELERATION_TIMER.isExpired()) {
            if (activityRecognitionHelper != null) {
                L.d(TAG, "Restarting activity recognition because of detected acceleration changes");
                wakeLockHelper.acquire();
                activityRecognitionHelper.stop();
                activityRecognitionHelper.start();
            }
            sendSMS(R.string.device_movement_detected, lastAccelerationValue, acceleration);
            ACCELERATION_NOTIFICATION_TIMER.update();
        } else {
            ACCELERATION_TIMER.update();
            lastAccelerationValue = acceleration;
        }
    }

    private void updateAccelerationOnUI(double acceleration) {
        Intent intent = new Intent(SERVICE_UPDATE);
        intent.putExtra(ACCELERATION_UPDATE, acceleration);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void updateLocationOnUI(@NonNull Location location) {
        Intent intent = new Intent(SERVICE_UPDATE);
        String locationLink = getString(
                R.string.location_link, location.getLatitude(), location.getLongitude());
        intent.putExtra(COORDINATE_UPDATE, locationLink);
        new Settings(this).set(SettingType.LAST_LOCATION, locationLink);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void updateActivityTypeOnUI(@NonNull String activity) {
        Intent intent = new Intent(SERVICE_UPDATE);
        intent.putExtra(ACTIVITY_UPDATE, activity);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void onBatteryStatus(int level) {
        L.d(TAG, "Battery level: %d%%", level);

        if (level <= 10 && !batteryBelow10) {
            batteryBelow10 = true;
            batteryBelow25 = true;
            batteryBelow50 = true;
            sendSMS(R.string.battery_level, level);
        } else if (level <= 25 && !batteryBelow25) {
            batteryBelow25 = true;
            batteryBelow50 = true;
            sendSMS(R.string.battery_level, level);
        } else if (level <= 50 && !batteryBelow50) {
            batteryBelow50 = true;
            sendSMS(R.string.battery_level, level);
        } else if (level > 50) {
            batteryBelow10 = false;
            batteryBelow25 = false;
            batteryBelow50 = false;
        }
    }
}