/**
 * @author Martynas Šustavičius
 * @since 2014 05 15
 */
package lt.smworks.tools.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CalendarHelper {

	/**
	 * Converts time stamp to readable date.
	 * @param format - date format. Example: yyyy/MM/dd HH:mm:ss.SSS
	 * @param time - time represented as unix time stamp.
	 * @return String containing human readable date.
	 */
	public static String getHumanReadableTime(String format, long time) {
		SimpleDateFormat formatter = new SimpleDateFormat(format, new Locale("UTF-8"));
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);
		return formatter.format(calendar.getTime());
	}
	
}
