package lt.smworks.tools.utils;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;

import androidx.annotation.NonNull;
import lt.smworks.tools.BuildConfig;

public class SmsHelper {

    public static final String TAG = "SmsHelper";
    private static String SENT = "sms_sent";
    private static String DELIVERED = "sms_delivered";

    private final Context context;
    private boolean started;
    private BroadcastReceiver sentReceiver;
    private BroadcastReceiver deliveredReceiver;

    public SmsHelper(@NonNull Context context) {
        this.context = context;
    }

    public void start() {
        if (started) {
            return;
        }

        sentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                handleSMSSentStatus(intent, getResultCode());
            }
        };
        deliveredReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                handleSMSDeliveryStatus(intent, getResultCode());
            }
        };
        context.registerReceiver(sentReceiver, new IntentFilter(SENT));
        context.registerReceiver(deliveredReceiver, new IntentFilter(DELIVERED));
        started = true;
    }

    public void stop() {
        if (!started) {
            return;
        }

        context.unregisterReceiver(deliveredReceiver);
        context.unregisterReceiver(sentReceiver);
    }

    public void sendSMS(@NonNull String phoneNo, @NonNull String msg) {
        try {
            PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
            //PendingIntent deliveredIntent = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);
            SmsManager manager = SmsManager.getDefault();
            manager.sendTextMessage(phoneNo, null, msg, sentIntent, null);
            if (BuildConfig.DEBUG) {
                L.d(TAG, "Message sent: %s", msg);
            }
        } catch (Exception ex) {
            L.e(ex);
        }
    }

    private static void handleSMSSentStatus(@NonNull Intent intent, int resultCode) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                L.d(TAG, "Message successfully sent");
                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                L.w(TAG, "Message not delivered. Generic message sent failure");
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                L.w(TAG, "Message not delivered. No service error");
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
                L.w(TAG, "Message not delivered. Null PDU");
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                L.w(TAG, "Message not delivered. Radio is off");
                break;
        }
    }

    private void handleSMSDeliveryStatus(@NonNull Intent intent, int resultCode) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                L.d(TAG, "Message successfully delivered");
                break;
            case Activity.RESULT_CANCELED:
                L.w(TAG, "Message delivery cancelled");
                break;
        }
    }
}
