package lt.smworks.gasup.models.enums;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import lt.smworks.gasup.R;

public enum Amenity {
    WC(1, R.string.wc),
    SHOP(2, R.string.shop),
    FOOD(3, R.string.food),
    CAR_WASH(4, R.string.car_wash),
    COMPRESSOR(5, R.string.compressor),
    SELF_SERVICE(6, R.string.self_service),
    CASH(7, R.string.cash),
    CREDIT_CARD(8, R.string.credit_card),
    UNKNOWN(-1, 0);

    private final int id;
    private final int stringResource;

    Amenity(int id, @StringRes int stringResource) {
        this.id = id;
        this.stringResource = stringResource;
    }

    @NonNull
    public static List<Amenity> fromIds(@NonNull List<Integer> ids) {
        List<Amenity> amenityList = new ArrayList<>();
        for (Integer id : ids) {
            Amenity amenity = fromId(id);
            if (amenity != UNKNOWN) {
                amenityList.add(amenity);
            }
        }
        return amenityList;
    }

    @NonNull
    public int getId() {
        return id;
    }

    @StringRes
    public int getStringResource() {
        return stringResource;
    }

    @NonNull
    public static List<Amenity> toList() {
        List<Amenity> amenities = new ArrayList<>();
        for (Amenity amenity : values()) {
            if (amenity != UNKNOWN) {
                amenities.add(amenity);
            }
        }
        return amenities;
    }

    @NonNull
    public static Amenity fromId(int id) {
        for (Amenity amenity : values()) {
            if (amenity.id == id) {
                return amenity;
            }
        }

        return UNKNOWN;
    }

    @NonNull
    public static List<Integer> toIdList(List<Amenity> amenities) {
        List<Integer> list = new ArrayList<>();
        for (Amenity amenity : amenities) {
            list.add(amenity.getId());
        }
        return list;
    }
}
