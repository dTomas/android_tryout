package lt.smworks.gasup.interfaces;

import java.util.List;

import lt.smworks.gasup.models.json.DiscountCardJson;
import lt.smworks.gasup.models.json.GasStationJson;
import lt.smworks.gasup.models.json.UserJson;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestService {

    @GET("rest/gas_stations")
    Call<List<GasStationJson>> getGasStations(@Query("coordinates") String coordinates);

    @GET("rest/gas_stations")
    Call<List<GasStationJson>> getSortedGasStations(
            @Query("fuelType") int fuelTypeId, @Query("coordinates") String coordinates);

    @GET("rest/gas_stations/{id}")
    Call<GasStationJson> getGasStation(@Path("id") String id);

    @PUT("rest/users")
    Call<UserJson> createUser(@Body UserJson user);

    @GET("rest/users/{email}")
    Call<UserJson> getUser(@Path("email") String email, @Query("password") String password);

    @GET("rest/users/facebook")
    Call<UserJson> getUser(@Query("token") String token);

    @Multipart
    @PUT("rest/images")
    Call<String> uploadImage(@Part MultipartBody.Part image);

    @Multipart
    @POST("rest/images/{userId}")
    Call<String> updateAvatar(@Path("userId") String userId,
                              @Query("token") String token,
                              @Part MultipartBody.Part image);

    @POST("rest/users/{userId}/username")
    Call<String> updateUsername(@Path("userId") String userId,
                                @Query("token") String token,
                                @Query("username") String username);

    @PUT("rest/gas_stations/add")
    Call<Void> addGasStation(@Body GasStationJson gasStation);

    @PUT("rest/gas_stations/{id}")
    Call<Void> updateGasStation(@Path("id") String id, @Body GasStationJson gasStation);
}
