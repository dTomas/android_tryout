package lt.smworks.utils;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.ActivityTransitionEvent;
import com.google.android.gms.location.ActivityTransitionRequest;
import com.google.android.gms.location.ActivityTransitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import lt.smworks.tools.models.Pair;
import lt.smworks.tools.utils.L;

public class ActivityRecognitionHelper {

    public static final String TAG = "ActivityRecognitionHelper";
    private static final int DETECTION_INTERVAL = 30 * 1000;
    private final ActivityRecognitionClient recognitionClient;
    private final PendingIntent pendingIntent;
    private boolean isStarted;

    public ActivityRecognitionHelper(@NonNull Context context, @NonNull PendingIntent pendingIntent) {
        this.pendingIntent = pendingIntent;
        recognitionClient = ActivityRecognition.getClient(context);
    }

    public void start() {
        if (isStarted) {
            return;
        }

        L.d(TAG, "Starting activity recognition tracking");
        Task<Void> task = recognitionClient.requestActivityUpdates(DETECTION_INTERVAL, pendingIntent);
        task.addOnSuccessListener(o -> L.d(TAG, "Activity recognition updates requested successfully"));
        task.addOnFailureListener(L::e);
        task = recognitionClient.requestActivityTransitionUpdates(getAllTransitionRequest(), pendingIntent);
        task.addOnSuccessListener(o -> L.d(TAG, "Activity transition updates requested successfully"));
        task.addOnFailureListener(L::e);
        isStarted = true;
    }

    public void stop() {
        if (!isStarted) {
            return;
        }

        L.d(TAG, "Stopping activity recognition tracking");
        recognitionClient.removeActivityUpdates(pendingIntent);
        recognitionClient.removeActivityTransitionUpdates(pendingIntent);
        isStarted = false;
    }

    public static String activityTypeToString(int type) {
        switch (type) {
            case DetectedActivity.IN_VEHICLE:
                return "in vehicle";
            case DetectedActivity.ON_BICYCLE:
                return "on bicycle";
            case DetectedActivity.ON_FOOT:
                return "on foot";
            case DetectedActivity.RUNNING:
                return "running";
            case DetectedActivity.STILL:
                return "still";
            case DetectedActivity.TILTING:
                return "tilting";
            case DetectedActivity.UNKNOWN:
                return "unknown";
            case DetectedActivity.WALKING:
                return "walking";
            default:
                return "not recognized";
        }
    }

    public static String activityTransitionToString(int type) {
        switch (type) {
            case ActivityTransition.ACTIVITY_TRANSITION_ENTER:
                return "enter";
            case ActivityTransition.ACTIVITY_TRANSITION_EXIT:
                return "exit";
            default:
                return "not recognized";
        }
    }

    public static boolean isMovementStarted(int activityType) {
        return isMovementStarted(activityType,
                ActivityTransition.ACTIVITY_TRANSITION_ENTER);
    }

    public static boolean isMovementStarted(int activityType, int transitionType) {
        return (activityType == DetectedActivity.IN_VEHICLE
                || activityType == DetectedActivity.RUNNING
                || activityType == DetectedActivity.ON_BICYCLE
                || activityType == DetectedActivity.ON_FOOT
                || activityType == DetectedActivity.WALKING)
                && transitionType == ActivityTransition.ACTIVITY_TRANSITION_ENTER;
    }

    public static boolean isMovementStopped(int activityType) {
        return isMovementStopped(activityType,
                ActivityTransition.ACTIVITY_TRANSITION_ENTER);
    }

    public static boolean isMovementStopped(int activityType, int transitionType) {
        return (activityType == DetectedActivity.TILTING
                || activityType == DetectedActivity.STILL)
                && transitionType == ActivityTransition.ACTIVITY_TRANSITION_ENTER;
    }

    private ActivityTransitionRequest getAllTransitionRequest() {
        List<ActivityTransition> list = new ArrayList<>();
        addTransition(list, DetectedActivity.IN_VEHICLE);
        addTransition(list, DetectedActivity.WALKING);
        addTransition(list, DetectedActivity.ON_BICYCLE);
        addTransition(list, DetectedActivity.RUNNING);
        addTransition(list, DetectedActivity.STILL);
        //addTransition(list, DetectedActivity.ON_FOOT);
        //addTransition(list, DetectedActivity.TILTING);
        //addTransition(list, DetectedActivity.UNKNOWN);
        return new ActivityTransitionRequest(list);
    }

    private void addTransition(List<ActivityTransition> transitions, int inVehicle) {
        transitions.add(new ActivityTransition.Builder()
                .setActivityType(inVehicle)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build());
        transitions.add(new ActivityTransition.Builder()
                .setActivityType(inVehicle)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build());
    }

    public static boolean hasActivityResult(Intent intent) {
        return ActivityRecognitionResult.hasResult(intent);
    }

    @Nullable
    public static Pair<Integer, Integer> getActivityResult(Intent intent) {
        ActivityRecognitionResult res = ActivityRecognitionResult.extractResult(intent);
        if (res != null) {
            DetectedActivity detectedActivity = res.getMostProbableActivity();
            int activityType = detectedActivity.getType();
            int confidence = detectedActivity.getConfidence();
            return new Pair<>(activityType, confidence);
        }

        return null;
    }

    public static boolean hasActivityTransitionResult(Intent intent) {
        return ActivityTransitionResult.hasResult(intent);
    }

    @NonNull
    public static List<Pair<Integer, Integer>> getActivityTransitionResults(Intent intent) {
        ActivityTransitionResult result = ActivityTransitionResult.extractResult(intent);
        List<Pair<Integer, Integer>> results = new ArrayList<>();
        if (result == null) {
            L.w(TAG, "Activity transition result is null");
            return results;
        }
        for (ActivityTransitionEvent event : result.getTransitionEvents()) {
            results.add(new Pair<>(event.getTransitionType(), event.getActivityType()));
        }
        return results;
    }
}
