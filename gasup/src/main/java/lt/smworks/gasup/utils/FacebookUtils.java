package lt.smworks.gasup.utils;

import android.net.Uri;

import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;

import lt.smworks.gasup.models.json.GasStationJson;

public final class FacebookUtils {

    private static final String HASH_TAGS = "#gasup";

    private FacebookUtils() {
    }

    public static ShareContent getGasStationShareContent(GasStationJson json) {
        return new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://smworks.herokuapp.com/web/gas_station/" + json.getId()))
                .setShareHashtag(new ShareHashtag.Builder().setHashtag(HASH_TAGS).build())
                .build();
    }
}
