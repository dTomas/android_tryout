/**
 * @author Martynas Šustavičius
 * @since 2014 03 31
 */
package lt.smworks.tools.utils;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;

import androidx.annotation.NonNull;
import lt.smworks.tools.interfaces.SimpleAnimationListener;

public class AnimationUtils {

	/**
	 * Animates horizontal translation on x axis.
	 * @param fromX - starting x position.
	 * @param toX - ending x position.
	 * @param duration - animation duration in milliseconds.
	 * @return Translate animation object.
	 */
	public static TranslateAnimation translate(int fromX, int toX, int duration) {
		return translate(fromX, toX, duration, null);
	}
	
	/**
	 * Animates horizontal translation on x axis.
	 * @param fromX - starting x position.
	 * @param toX - ending x position.
	 * @param duration - animation duration in milliseconds.
	 * @param runnable - runnable that will be executed after animation.
	 * @return Translate animation object.
	 */
	public static TranslateAnimation translate(int fromX, int toX, int duration, final Runnable runnable) {
		TranslateAnimation ta = new TranslateAnimation(fromX, toX, 0, 0);
		ta.setDuration(duration);
		ta.setFillAfter(true);
		ta.setInterpolator(new LinearInterpolator());
		if (runnable != null) {
			ta.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					runnable.run();
				}
			});
		}
		return ta;
	}

    public static void fadeViewOut(@NonNull View view, int duration) {
        view.setVisibility(View.VISIBLE);
        AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
        anim.setDuration(duration);
        anim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				view.setVisibility(View.INVISIBLE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
        view.startAnimation(anim);
    }

    public static void fadeViewIn(@NonNull View view, int duration) {
        view.setVisibility(View.VISIBLE);
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(duration);
        view.startAnimation(anim);
    }
}
