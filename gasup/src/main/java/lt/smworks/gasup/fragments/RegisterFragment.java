package lt.smworks.gasup.fragments;

import android.content.Context;
import android.widget.Button;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import lt.smworks.gasup.R;
import lt.smworks.gasup.databinding.RegisterFragmentBinding;
import lt.smworks.gasup.interfaces.RetrofitResult;
import lt.smworks.gasup.models.json.UserJson;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.utils.ValidationUtils;

@Layout(R.layout.register_fragment)
public class RegisterFragment extends GasupBaseFragment {

    private RegisterFragmentBinding binding;

    @Override
    protected void onCreateView(@NonNull Context context) {
        binding = DataBindingUtil.bind(getRootView());
        Button registerButton = findView(R.id.registerFragment_register);

        new ValidationUtils.FormValidator()
                .setUsernameInput(binding.emailAndPasswordFormUsername)
                .setEmailInput(binding.emailAndPasswordFormEmail)
                .setPasswordInput(binding.emailAndPasswordFormPassword)
                .setSubmitButton(registerButton)
                .setOnSubmitListener(this::register);
    }

    private void register() {
        getMainActivity().hideKeyboard();
        showProgressDialog(R.string.registration_in_progress);
        UserJson userJson = new UserJson();
        userJson.setUsername(Objects.requireNonNull(binding.emailAndPasswordFormUsername.getText()).toString());
        userJson.setEmail(Objects.requireNonNull(binding.emailAndPasswordFormEmail.getText()).toString());
        getMainActivity().getRestService().createUser(userJson).enqueue((RetrofitResult<UserJson>) (data, error) -> {
            hideProgressDialog();
            if (error != null) {
                showError(error);
                return;
            }

            getMainActivity().getSettings().setUser(data.body());
            getMainActivity().clearFragments();
            getMainActivity().addFragment(new MainFragment());
        });
    }
}
