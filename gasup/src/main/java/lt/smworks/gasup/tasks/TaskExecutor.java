package lt.smworks.gasup.tasks;

import java.util.concurrent.Executor;

public class TaskExecutor implements Executor {
    public void execute(Runnable r) {
        new Thread(r).start();
    }
}