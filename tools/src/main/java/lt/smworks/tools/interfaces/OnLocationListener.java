package lt.smworks.tools.interfaces;

import android.location.Location;

import androidx.annotation.NonNull;

public interface OnLocationListener {

    void onLocationResult(@NonNull Location location);
}
