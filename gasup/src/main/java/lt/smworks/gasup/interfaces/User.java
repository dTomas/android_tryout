package lt.smworks.gasup.interfaces;

public interface User {
    String getUserId();

    String getUsername();

    String getAvatarUrl();

    String getToken();
}
