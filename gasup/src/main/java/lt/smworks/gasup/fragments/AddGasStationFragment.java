package lt.smworks.gasup.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import bolts.Continuation;
import bolts.Task;
import lt.smworks.gasup.R;
import lt.smworks.gasup.adapters.AmenityAdapter;
import lt.smworks.gasup.adapters.FuelTypeEditAdapter;
import lt.smworks.gasup.databinding.AddGasStationFragmentBinding;
import lt.smworks.gasup.interfaces.RestService;
import lt.smworks.gasup.models.enums.Amenity;
import lt.smworks.gasup.models.enums.HttpResponseCode;
import lt.smworks.gasup.models.json.FuelPriceJson;
import lt.smworks.gasup.models.json.GasStationJson;
import lt.smworks.gasup.models.json.LocationJson;
import lt.smworks.gasup.utils.PlacePickerUtils;
import lt.smworks.gasup.utils.RetrofitUtils;
import lt.smworks.gasup.viewmodels.AddGasStationViewModel;
import lt.smworks.gasup.viewmodels.BundleAwareViewModelFactory;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.utils.IOUtils;
import lt.smworks.tools.utils.IntentUtils;
import lt.smworks.tools.utils.L;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

@Layout(R.layout.add_gas_station_fragment)
public class AddGasStationFragment extends GasupBaseFragment {

    private static final String TAG = "AddGasStationFragment";
    private static final int CAMERA_REQUEST_CODE = 9001;
    private static final int GALLERY_REQUEST_CODE = 9002;
    private static final int PLACE_PICKER_REQUEST_CODE = 9003;
    private static final int IMAGE_SIZE = 1024;
    public static final String HTTP = "http";
    protected AddGasStationViewModel viewModel;
    protected AddGasStationFragmentBinding binding;
    private Uri photoUri;

    @Override
    protected void onCreateView(@NonNull Context context) {
        BundleAwareViewModelFactory factory = new BundleAwareViewModelFactory(getMainActivity().getApplication(), getBundle());
        viewModel = ViewModelProviders.of(this, factory).get(AddGasStationViewModel.class);
        binding = Objects.requireNonNull(DataBindingUtil.bind(getRootView()));
        binding.setView(this);
        binding.setViewmodel(viewModel);
        binding.setLifecycleOwner(this);
        setupAdapters(context);
        binding.getAmenityAdapter().setOnCheckListener(checkedItems
                -> viewModel.getGasStation().setAmenities(Amenity.toIdList(checkedItems)));

        showProgressDialog(R.string.camera_alert);
        getMainActivity().askForPermission(getString(R.string.permission_camera_rationale), (granted) -> {
            hideProgressDialog();
            if (!granted) {
                showSnackbarFailure(getString(R.string.camera_alert));
                getMainActivity().onBackPressed();
            }
        }, Manifest.permission.CAMERA);
    }

    protected void setupAdapters(@NonNull Context context) {
        binding.setFuelTypeAdapter(new FuelTypeEditAdapter(context));
        binding.setAmenityAdapter(new AmenityAdapter(context));
    }

    public void addLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getMainActivity()), PLACE_PICKER_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            L.ex(TAG, e);
        }
    }

    public void addFuelType() {
        FuelPriceJson fuelPriceJson = new FuelPriceJson();
        binding.getFuelTypeAdapter().addItem(fuelPriceJson);
        viewModel.getGasStation().setFuelPrices(binding.getFuelTypeAdapter().getItems());
    }

    public void takePhoto() {
        if (hasPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            photoUri = getStorageUri();
            IntentUtils.takePicture(this, photoUri, CAMERA_REQUEST_CODE);
        } else {
            askPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    public void selectImageFromGallery() {
        if (hasPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            IntentUtils.GalleryIntentBuilder builder = new IntentUtils.GalleryIntentBuilder()
                    .setAspectX(1)
                    .setAspectY(1)
                    .setCrop(true)
                    .setFormat(Bitmap.CompressFormat.JPEG)
                    .setUri(getStorageUri())
                    .setScale(true)
                    .setOutputWidth(IMAGE_SIZE)
                    .setOutputHeight(IMAGE_SIZE);
            IntentUtils.selectPictureFromGallery(this, builder, GALLERY_REQUEST_CODE);
        } else {
            askPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    public void save(FuelTypeEditAdapter adapter) {
        GasStationJson station = viewModel.getGasStation();
        String name = station.getName();
        if (viewModel.getImageUrl() == null) {
            showSnackbarFailure(getString(R.string.no_image_provided));
            return;
        } else if (TextUtils.isEmpty(name)) {
            showSnackbarFailure(getString(R.string.name_cannot_be_empty));
            return;
        } else if (!viewModel.hasLocation()) {
            showSnackbarFailure(getString(R.string.no_location_provided));
            return;
        } else if (adapter.getItemCount() == 0) {
            showSnackbarFailure(getString(R.string.no_fuel_types_added));
            return;
        }

        uploadData();
    }

    protected void uploadData() {
        showProgressDialog(R.string.adding_new_gas_station);
        viewModel.getGasStation().setUser(getMainActivity().getUser());
        GasStationJson station = viewModel.getGasStation();
        RestService restService = getMainActivity().getRestService();
        Task.callInBackground(() -> {
            if (!saveImage(station, restService)) {
                return getString(R.string.unable_to_save_image);
            }
            return saveGasStation(station, restService);
        }).continueWith((Continuation<String, Void>) task -> {
            hideProgressDialog();
            String error = task.getResult();
            if (error != null) {
                L.e(TAG, error);
                showSnackbarFailure(error);
            } else {
                showSnackbarSuccess(getString(R.string.gas_station_added));
                getMainActivity().getSettings().setGasStationUpdateRequired(true);
                getMainActivity().popFragment();
            }
            return null;
        }, Task.UI_THREAD_EXECUTOR);
    }

    @WorkerThread
    protected boolean saveImage(GasStationJson station, RestService restService) {
        String imageUrl = viewModel.getImageUrl();
        if (imageUrl != null && imageUrl.startsWith(HTTP)) {
            return true;
        }
        try {
            Response<String> response = restService
                    .uploadImage(RetrofitUtils.getImageForUpload(imageUrl))
                    .execute();
            if (response.isSuccessful()) {
                station.setImageUrl(response.body());
                return true;
            }
        } catch (IOException e) {
            L.ex(e);
        }
        return false;
    }

    @WorkerThread
    private String saveGasStation(GasStationJson station, RestService restService) {
        try {
            Response<Void> response = restService.addGasStation(station).execute();
            if (!response.isSuccessful()) {
                if (response.code() == HttpResponseCode.CONFLICT.getCode()) {
                    return getString(R.string.gas_station_already_exists);
                } else {
                    return getString(R.string.unable_to_save_gas_station);
                }
            }
        } catch (Exception e) {
            L.ex(e);
            return e.getMessage();
        }

        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    resizeAndSaveImage(photoUri, savedImagePath -> viewModel.setImageUrl(savedImagePath));
                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK && intent.getData() != null) {
                    resizeAndSaveImage(intent.getData(), savedImagePath -> viewModel.setImageUrl(savedImagePath));
                }
                break;
            case PLACE_PICKER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(getMainActivity(), intent);
                    if (place.getAddress() != null) {
                        viewModel.setAddress(place.getAddress().toString());
                    }
                    LatLng latLng = place.getLatLng();
                    viewModel.setLocation(new LocationJson(latLng.latitude, latLng.longitude));
                    if (TextUtils.isEmpty(viewModel.getName())) {
                        viewModel.setName(place.getName().toString());
                    }
                    if (photoUri == null) {
                        showSelectedLocationImage(place);
                    }
                }
                break;
        }
    }

    private Uri getStorageUri() {
        return getMainActivity().getFileUri(
                getMainActivity().getUser().getUserId() + "_" + System.currentTimeMillis());
    }

    private void showSelectedLocationImage(Place place) {
        showProgressDialog(R.string.loading_image);
        PlacePickerUtils.getPlacePhoto(getMainActivity(), place, bitmap -> {
            if (!isValid()) {
                return;
            }
            hideProgressDialog();
            if (bitmap == null) {
                showSnackbarFailure(getString(R.string.unable_to_load_image));
                return;
            }
            @SuppressLint("MissingPermission") Uri imageUri = IOUtils.saveBitmapAndGetUri(getMainActivity(), bitmap);
            resizeAndSaveImage(imageUri, savedImagePath -> viewModel.setImageUrl(savedImagePath));
        });
    }
}
