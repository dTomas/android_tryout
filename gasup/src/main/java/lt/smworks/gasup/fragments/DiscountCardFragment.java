package lt.smworks.gasup.fragments;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import lt.smworks.gasup.R;
import lt.smworks.gasup.adapters.DiscountCardAdapter;
import lt.smworks.gasup.databinding.DiscountCardFragmentBinding;
import lt.smworks.gasup.dialogs.AddDiscountCardDialogFragment;
import lt.smworks.gasup.models.json.UserJson;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.discount_card_fragment)
public class DiscountCardFragment extends GasupBaseFragment {

    private static final String TAG = "DiscountCardFragment";

    @Override
    protected void onCreateView(@NonNull Context context) {
        DiscountCardFragmentBinding binding = Objects.requireNonNull(DataBindingUtil.bind(getRootView()));

        UserJson user = getMainActivity().getSettings().getUser();
        if (user == null) {
            return;
        }
        DiscountCardAdapter adapter = new DiscountCardAdapter(context);
        adapter.setItems(getMainActivity().getSettings().getUser().getDiscountCards());
        binding.setCardAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.add(R.string.add_card);
        item.setIcon(R.drawable.ic_credit_card);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        item.setOnMenuItemClickListener(this::add);
    }

    private boolean add(MenuItem menuItem) {
        new AddDiscountCardDialogFragment()
                .withTargetFragment(this)
                .show();
        return true;
    }
}
