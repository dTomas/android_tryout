package lt.smworks.gasup.utils;

public final class MathUtils {

    public static boolean isAGreaterThanBOrEqual(Double a, Double b, double delta) {
        if (a == null && b == null) {
            return true;
        }

        if (a == null) {
            return false;
        }

        if (b == null) {
            return true;
        }

        double abs = Math.abs(a - b);
        return abs < delta || (abs > delta && a > b);
    }

    public static boolean isALesserThanB(Double a, Double b, double delta) {
        return !isAGreaterThanBOrEqual(a, b, delta);
    }

    public static boolean isAEqualB(Double a, Double b, double delta) {
        return Math.abs(a - b) < delta;
    }
}
