package lt.smworks.gasup.models.json;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LocationJson implements Parcelable {

    private static final String TYPE_POINT = "Point";
    private String type;
    private List<Double> coordinates;

    public LocationJson() {
        this(0.0, 0.0);
    }

    public LocationJson(double lat, double lon) {
        type = TYPE_POINT;
        coordinates = Arrays.asList(lat, lon);
    }

    public LocationJson(Location location) {
        this(location.getLatitude(), location.getLongitude());
    }

    protected LocationJson(Parcel in) {
        type = in.readString();
    }

    public static final Creator<LocationJson> CREATOR = new Creator<LocationJson>() {
        @Override
        public LocationJson createFromParcel(Parcel in) {
            return new LocationJson(in);
        }

        @Override
        public LocationJson[] newArray(int size) {
            return new LocationJson[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Double> getCoordinates() {
        if (coordinates == null) {
            coordinates = new ArrayList<>();
        }
        return coordinates;
    }

    public void setCoordinates(List<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public double getLat() {
        return coordinates.isEmpty() ? 0.0 : coordinates.get(0);
    }

    public double getLon() {
        return coordinates.size() < 2 ? 0.0 : coordinates.get(1);
    }

    public Location toLocation() {
        Location location = new Location("");
        location.setLatitude(getLat());
        location.setLongitude(getLon());
        return location;
    }

    public double distanceTo(LocationJson location) {
        return toLocation().distanceTo(location.toLocation());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
    }

    @Override
    public String toString() {
        return getLat() + "," + getLon();
    }
}
