package lt.smworks.gasup;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Transformation;

import androidx.annotation.NonNull;
import lt.smworks.tools.utils.ImageUtils;
import lt.smworks.tools.utils.MetricsHelper;

public class Picasso {

    private static final String TRANSFORMATION_CIRCLE = "circle";
    private static final int PHOTO_CORNER_WIDE = 4;
    private static final int SMALL_WIDTH = 128;
    private static final int SMALL_HEIGHT = 128;

    public static void loadProfilePicture(@NonNull ImageView imageView, String photoUrl) {
        if (TextUtils.isEmpty(photoUrl)) {
            imageView.setImageResource(R.mipmap.ic_launcher_round);
            return;
        }
        com.squareup.picasso.Picasso.get()
                .load(photoUrl)
                .placeholder(R.drawable.preloader)
                .error(R.mipmap.ic_launcher_round)
                .transform(new Transformation() {
                    @Override
                    public Bitmap transform(Bitmap source) {
                        float width = MetricsHelper.convertDpToPixel(
                                imageView.getContext(), PHOTO_CORNER_WIDE);
                        return ImageUtils.applyCircleMask(source, (int) width, Color.WHITE);
                    }

                    @Override
                    public String key() {
                        return TRANSFORMATION_CIRCLE;
                    }
                })
                .into(imageView);
    }

    public static void loadGasStationPicture(@NonNull ImageView imageView,
                                             @NonNull Uri photoUrl,
                                             boolean smallIcon) {
        if (photoUrl == null) {
            imageView.setImageResource(R.mipmap.ic_launcher_round);
            return;
        }
        RequestCreator requestCreator = com.squareup.picasso.Picasso.get().load(photoUrl);

        if (smallIcon) {
            requestCreator.resize(SMALL_WIDTH, SMALL_HEIGHT).centerCrop();
        }
        requestCreator.placeholder(R.drawable.preloader)
                .error(R.mipmap.ic_launcher_round)
                .into(imageView);
    }
}
