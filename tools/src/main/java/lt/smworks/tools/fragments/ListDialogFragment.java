package lt.smworks.tools.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import lt.smworks.tools.R;
import lt.smworks.tools.utils.L;

public class ListDialogFragment extends DialogFragment {

    public static final String TAG = "ListDialogFragment";
    public static final int REQUEST_CODE = 9001;
    private static final String KEY_TITLE = "key_title";
    private static final String KEY_LIST = "key_list";
    public static final String KEY_SELECTED_ITEM_VALUE = "key_selected_item_value";
    private FragmentManager fragmentManager;


    public void setFragmentManager(@NonNull FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public Dialog onCreateDialog(@NonNull Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (getArguments().containsKey(KEY_LIST)) {
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.list_dialog_fragment_item);
            ArrayList<String> stringArrayList = getArguments().getStringArrayList(KEY_LIST);
            if (stringArrayList == null) {
                stringArrayList = new ArrayList<>();
            }
            for (String item : stringArrayList) {
                arrayAdapter.add(item);
            }
            ArrayList<String> finalStringArrayList = stringArrayList;
            builder.setAdapter(arrayAdapter, (dialog, which)
                    -> onItemSelected(which, finalStringArrayList.get(which)));
        }

        if (getArguments().containsKey(KEY_TITLE)) {
            builder.setTitle(getArguments().getInt(KEY_TITLE));
        }

        return builder.create();
    }


    private void onItemSelected(int which, @NonNull String value) {
        L.d(TAG, "Selected item: %d", which);
        Intent intent = new Intent();
        intent.putExtra(KEY_SELECTED_ITEM_VALUE, value);
        getTargetFragment().onActivityResult(REQUEST_CODE, which, intent);
    }

    public void show() {
        if (fragmentManager != null) {
            show(fragmentManager, TAG);
        }
    }

    public static class Builder {

        private final FragmentManager fragmentManager;
        private final Bundle bundle = new Bundle();
        private final BaseFragment targetFragment;

        public Builder(@NonNull BaseFragment baseFragment) {
            this.targetFragment = baseFragment;
            this.fragmentManager = baseFragment.getFragmentManager();
        }

        public Builder setTitle(@StringRes int title) {
            bundle.putInt(KEY_TITLE, title);
            return this;
        }

        public Builder setItems(@NonNull ArrayList<String> items) {
            bundle.putStringArrayList(KEY_LIST, items);
            return this;
        }

        public ListDialogFragment build() {
            ListDialogFragment listDialogFragment = new ListDialogFragment();
            listDialogFragment.setFragmentManager(fragmentManager);
            listDialogFragment.setArguments(bundle);
            listDialogFragment.setTargetFragment(targetFragment, REQUEST_CODE);
            return listDialogFragment;
        }
    }
}
