//package lt.smworks.gasup.fragments;
//
//import android.content.Context;
//
//import androidx.annotation.NonNull;
//import lt.smworks.gasup.R;
//import lt.smworks.gasup.utils.OcrCameraPreview;
//import lt.smworks.tools.interfaces.Layout;
//
//@Layout(R.layout.preview_fragment)
//public class PreviewFragment extends GasupBaseFragment {
//
//    public static final String TAG = "PreviewFragment";
//    private OcrCameraPreview preview;
//
//
//    @Override
//    protected void onCreateView(@NonNull Context context) {
//        preview = findView(R.id.preview);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        preview.start();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        preview.stop();
//    }
//}
