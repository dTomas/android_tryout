package lt.smworks.gasup.adapters;

import android.content.Context;
import android.view.View;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.R;
import lt.smworks.gasup.databinding.DiscountCardItemBinding;
import lt.smworks.gasup.models.json.DiscountCardJson;
import lt.smworks.tools.adapters.RecyclerViewAdapter;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.discount_card_item)
public class DiscountCardAdapter extends RecyclerViewAdapter<DiscountCardJson, DiscountCardAdapter.ItemViewHolder> {

    public DiscountCardAdapter(@NonNull Context context) {
        super(context);
    }

    @Override
    protected ItemViewHolder onCreateViewHolder(@NonNull View itemView) {
        DiscountCardItemBinding binding = DataBindingUtil.bind(itemView);
        return new ItemViewHolder(Objects.requireNonNull(binding));
    }

    @Override
    protected void onBindViewHolder(@NonNull ItemViewHolder holder,
                                    @NonNull DiscountCardJson item,
                                    int position) {
        holder.bind(item);
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        private final DiscountCardItemBinding binding;

        private ItemViewHolder(DiscountCardItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(DiscountCardJson item) {
            binding.setModel(item);
            binding.executePendingBindings();
        }
    }
}
