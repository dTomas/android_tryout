package lt.smworks.vehicleguard.adapters;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import lt.smworks.tools.adapters.SettingsAdapter;
import lt.smworks.tools.models.SettingType;
import lt.smworks.vehicleguard.models.SettingTypes;

public class VehicleGuardSettingsAdapter extends SettingsAdapter {
    
    public VehicleGuardSettingsAdapter(Context context) {
        super(context);
    }

    @Override
    protected void setupSettingItems() {
        List<SettingType> settingList = new ArrayList<>();
        settingList.add(SettingTypes.PHONE_NUMBER);
        settingList.add(lt.smworks.models.SettingTypes.AVERAGE_LOCATION_UPDATE_INTERVAL);
        settingList.add(lt.smworks.models.SettingTypes.FASTEST_LOCATION_UPDATE_INTERVAL);
        settingList.add(SettingTypes.SENSOR_SENSITIVITY);
        setItems(settingList);
    }
}
