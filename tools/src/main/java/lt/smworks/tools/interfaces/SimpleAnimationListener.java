package lt.smworks.tools.interfaces;

import android.view.animation.Animation;

public interface SimpleAnimationListener extends Animation.AnimationListener {
    @Override
    default void onAnimationStart(Animation animation) {
        onAnimation(Type.START);
    }

    @Override
    default void onAnimationEnd(Animation animation) {
        onAnimation(Type.END);
    }

    @Override
    default void onAnimationRepeat(Animation animation) {
        onAnimation(Type.REPEAT);
    }

    void onAnimation(Type type);

    enum Type {
        START,
        END,
        REPEAT
    }
}
