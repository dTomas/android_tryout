package lt.smworks.gasup.utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResponse;
import com.google.android.gms.location.places.PlacePhotoResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.Task;

public final class PlacePickerUtils {

    public static void getPlacePhoto(Context context,
                                       Place place,
                                       OnPlacePictureLoadedListener listener) {
        GeoDataClient geoDataClient = Places.getGeoDataClient(context);
        Task<PlacePhotoMetadataResponse> photoMetadataResponse = geoDataClient.getPlacePhotos(place.getId());
        photoMetadataResponse.addOnCompleteListener(task -> {
            PlacePhotoMetadataResponse photos = task.getResult();
            if (photos == null) {
                listener.onPlacePictureLoaded(null);
                return;
            }
            PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
            if (photoMetadataBuffer == null || photoMetadataBuffer.getCount() == 0) {
                listener.onPlacePictureLoaded(null);
                return;
            }
            PlacePhotoMetadata photoMetadata = photoMetadataBuffer.get(0);
            Task<PlacePhotoResponse> photoResponse = geoDataClient.getPhoto(photoMetadata);
            photoResponse.addOnCompleteListener(task1 -> {
                PlacePhotoResponse photo = task1.getResult();
                if (photo != null && photo.getBitmap() != null) {
                    listener.onPlacePictureLoaded(photo.getBitmap());
                } else {
                    listener.onPlacePictureLoaded(null);
                }
            });
        });
    }

    public interface OnPlacePictureLoadedListener {
        void onPlacePictureLoaded(Bitmap bitmap);

    }
}
