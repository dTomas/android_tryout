package lt.smworks.gasup.dialogs;

import android.app.Dialog;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.R;
import lt.smworks.tools.dialogs.BaseDialogFragment;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.add_discount_card)
public class AddDiscountCardDialogFragment extends BaseDialogFragment<AddDiscountCardDialogFragment> {

    public static final String KEY_FUEL_PRICE_ARRAY = "fuelPriceArray";

    @Override
    public Dialog onCreateDialog(@NonNull View view) {
        RecyclerView list = view.findViewById(R.id.addDiscountCard_discountCards);

        AlertDialog d = new AlertDialog.Builder(view.getContext())
                .setTitle(R.string.available_fuel_types)
                .setView(view)
                .setPositiveButton(R.string.update, (dialog, which) -> {
                    dismiss();
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
                .show();

        if (d.getWindow() != null) {
            d.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        }
        return d;
    }
}
