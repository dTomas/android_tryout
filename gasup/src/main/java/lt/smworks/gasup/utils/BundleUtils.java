package lt.smworks.gasup.utils;

import android.os.Bundle;

public final class BundleUtils {

    private BundleUtils() {
    }

    public static boolean contains(Bundle bundle, String key, String value) {
        if (bundle == null || !bundle.containsKey(key)) {
            return false;
        }
        String v = bundle.getString(key);

        return v != null && v.equals(value);
    }
}
