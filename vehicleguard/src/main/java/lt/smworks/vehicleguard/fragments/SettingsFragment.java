package lt.smworks.vehicleguard.fragments;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.vehicleguard.R;
import lt.smworks.vehicleguard.adapters.VehicleGuardSettingsAdapter;

@Layout(R.layout.settings_fragment)
public class SettingsFragment extends VehicleGuardFragment {

    @Override
    protected void onCreateView(@NonNull Context context) {
        RecyclerView list = (RecyclerView) getRootView();
        list.setLayoutManager(new LinearLayoutManager(context));
        list.setAdapter(new VehicleGuardSettingsAdapter(context));
        list.setHasFixedSize(true);
    }
}
