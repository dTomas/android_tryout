package lt.smworks.gasup.viewmodels;

import android.app.Application;
import android.os.Bundle;

import java.lang.reflect.InvocationTargetException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class BundleAwareViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final Bundle bundle;
    private final Application applicaiton;

    public BundleAwareViewModelFactory(@NonNull Application application, @Nullable Bundle bundle) {
        this.applicaiton = application;
        this.bundle = bundle;
    }

    @NonNull
    @SuppressWarnings("unchecked")
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (GasupViewModel.class.isAssignableFrom(modelClass)) {
            try {
                return modelClass.getConstructor(Application.class, Bundle.class)
                        .newInstance(applicaiton, bundle);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException("Cannot create an instance of " + modelClass, e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Cannot create an instance of " + modelClass, e);
            } catch (InstantiationException e) {
                throw new RuntimeException("Cannot create an instance of " + modelClass, e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Cannot create an instance of " + modelClass, e);
            }
        }
        return super.create(modelClass);
    }
}