package lt.smworks.tools.models;

public enum SortOrder {
    ASCENDING,
    DESCENDING
}
