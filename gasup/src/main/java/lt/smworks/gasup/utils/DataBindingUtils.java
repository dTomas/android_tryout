package lt.smworks.gasup.utils;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.RequestCreator;

import java.io.File;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.R;

public class DataBindingUtils {

    private static final int SMALL_WIDTH = 128;
    private static final int SMALL_HEIGHT = 128;

    @BindingAdapter(value = {"android:src", "placeholder", "useThumbnail"}, requireAll = false)
    public static void loadImage(ImageView view, Uri uri, Drawable placeholder, boolean useThumbnail) {
        if (uri == null || TextUtils.isEmpty(uri.toString())) {
            if (placeholder != null) {
                view.setImageDrawable(placeholder);
            } else {
                view.setImageResource(R.drawable.ic_image_placeholder);
            }
            return;
        }
        loadImage(view, useThumbnail, uri.toString());
    }

    @BindingAdapter(value = {"android:src", "placeholder", "useThumbnail"}, requireAll = false)
    public static void loadImage(ImageView view, String url, Drawable placeholder, boolean useThumbnail) {
        if (TextUtils.isEmpty(url)) {
            if (placeholder != null) {
                view.setImageDrawable(placeholder);
            } else {
                view.setImageResource(R.drawable.ic_image_placeholder);
            }
            return;
        }
        loadImage(view, useThumbnail, url);
    }

    private static void loadImage(ImageView view, boolean useThumbnail, String path) {
        File file = new File(path);
        if (file.exists()) {
            path = "file:" + path;
        }
        RequestCreator requestCreator = com.squareup.picasso.Picasso.get().load(path);

        if (useThumbnail) {
            requestCreator.resize(SMALL_WIDTH, SMALL_HEIGHT).centerCrop();
        }
        requestCreator.placeholder(R.drawable.preloader)
                .error(R.mipmap.ic_launcher_round)
                .into(view);
    }

    @BindingAdapter({"android:text"})
    public static void setText(TextView textView, CharSequence text) {
        final CharSequence oldText = textView.getText();
        if (text == oldText || (text == null && oldText.length() == 0)) {
            return;
        }
        if (text instanceof Spanned) {
            if (text.equals(oldText)) {
                return; // No change in the spans, so don't set anything.
            }
        } else if (!haveContentsChanged(text, oldText)) {
            return; // No content changes, so don't set anything.
        }
        if (oldText != null && text == null) {
            text = oldText;
        }
        textView.setText(text);
    }

    private static boolean haveContentsChanged(CharSequence str1, CharSequence str2) {
        if ((str1 == null) != (str2 == null)) {
            return true;
        } else if (str1 == null) {
            return false;
        }
        final int length = str1.length();
        if (length != str2.length()) {
            return true;
        }
        for (int i = 0; i < length; i++) {
            if (str1.charAt(i) != str2.charAt(i)) {
                return true;
            }
        }
        return false;
    }

    @BindingAdapter(value = {"android:entries", "columnCount"}, requireAll = false)
    public static <T extends RecyclerView.Adapter> void setEntries(RecyclerView view,
                                                                   T adapter,
                                                                   int columnCount) {
        if (adapter != null) {
            if (columnCount <= 1) {
                view.setLayoutManager(new LinearLayoutManager(view.getContext()));
            } else {
                view.setLayoutManager(new GridLayoutManager(view.getContext(), columnCount));
            }

            view.setAdapter(adapter);
        } else {
            view.setAdapter(null);
        }
    }
}
