package lt.smworks.gasup.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.Picasso;
import lt.smworks.gasup.R;
import lt.smworks.gasup.databinding.MainFragmentListItemBinding;
import lt.smworks.gasup.models.enums.FuelType;
import lt.smworks.gasup.models.json.FuelPriceJson;
import lt.smworks.gasup.models.json.GasStationJson;
import lt.smworks.gasup.models.json.LocationJson;
import lt.smworks.tools.adapters.RecyclerViewAdapter;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.main_fragment_list_item)
public class GasStationAdapter extends RecyclerViewAdapter<GasStationJson, GasStationAdapter.ItemViewHolder> {

    private final LayoutInflater layoutInflater;
    private LocationJson userLocation;

    public GasStationAdapter(@NonNull Context context) {
        super(context);
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    protected ItemViewHolder onCreateViewHolder(@NonNull View itemView) {
        MainFragmentListItemBinding binding = DataBindingUtil.bind(itemView);
        return new ItemViewHolder(Objects.requireNonNull(binding));
    }

    @Override
    protected void onBindViewHolder(@NonNull ItemViewHolder holder,
                                    @NonNull GasStationJson item,
                                    int position) {
        holder.bind(item);
    }

    public void updateDistances(@NonNull LocationJson location) {
        this.userLocation = location;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private final MainFragmentListItemBinding binding;

        private ItemViewHolder(MainFragmentListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(GasStationJson gasStation) {
            binding.setModel(gasStation);
            binding.executePendingBindings();

            if (gasStation.getImageUrl() != null) {
                Picasso.loadGasStationPicture(
                        binding.mainFragmentListItemImage,
                        Uri.parse(gasStation.getImageUrl()),
                        true);
            } else {
                binding.mainFragmentListItemImage.setImageResource(R.drawable.ic_image_placeholder);
            }
            binding.mainFragmentListItemContainer.setOnClickListener(v -> onItemClickListener.onItemClick(gasStation));
            binding.mainFragmentListItemName.setText(gasStation.getName());
            binding.mainFragmentListItemAddress.setText(gasStation.getAddress());
            setupPriceList(binding.mainFragmentListItemFuelTypes, gasStation.getFuelPrices());
            LocationJson location = gasStation.getLocation();
            if (!location.getCoordinates().isEmpty()) {
                LocationJson loc = new LocationJson(location.getLat(), location.getLon());
                binding.mainFragmentListItemDistance.setText(String.format(
                        Locale.getDefault(), "Distance: %.2f km", loc.distanceTo(userLocation) / 1000.0));
            } else {
                binding.mainFragmentListItemDistance.setText("");
            }
        }

        private void setupPriceList(@NonNull LinearLayout fuelTypeList, @NonNull List<FuelPriceJson> items) {
            fuelTypeList.removeAllViews();
            for (FuelPriceJson item : items) {
                layoutInflater.inflate(R.layout.round_textview, fuelTypeList);
                TextView tv = (TextView) fuelTypeList.getChildAt(fuelTypeList.getChildCount() - 1);
                FuelType fuelType = FuelType.fromId(item.getFuelTypeId());
                tv.setText(String.format(Locale.getDefault(),
                        "%s: %.3f €", context.getString(fuelType.getStringResId()), item.getPrice()));
            }
        }
    }


}
