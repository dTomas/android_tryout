package lt.smworks.vehicleguard.models;

import androidx.annotation.NonNull;

public enum SmsAction {
    SEND_LOCATION("LOCATION"),
    START("START"),
    UNKNOWN("unknown");

    private final String type;

    SmsAction(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }

    public static SmsAction fromMessage(@NonNull String message) {
        for (SmsAction action : values()) {
            if (message.toLowerCase().trim().equals(action.toString().toLowerCase())) {
                return action;
            }
        }
        return UNKNOWN;
    }
}