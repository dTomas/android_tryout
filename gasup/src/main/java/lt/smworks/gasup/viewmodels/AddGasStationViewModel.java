package lt.smworks.gasup.viewmodels;

import android.app.Application;
import android.location.Location;
import android.os.Bundle;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.databinding.PropertyChangeRegistry;
import lt.smworks.gasup.BR;
import lt.smworks.gasup.models.json.FuelPriceJson;
import lt.smworks.gasup.models.json.GasStationJson;
import lt.smworks.gasup.models.json.LocationJson;
import lt.smworks.gasup.utils.MathUtils;

public class AddGasStationViewModel extends GasupViewModel implements Observable {

    private PropertyChangeRegistry registry = new PropertyChangeRegistry();
    private final GasStationJson station;

    public AddGasStationViewModel(@NonNull Application application, @NonNull Bundle bundle) {
        super(application, bundle);
        if (!bundle.containsKey(GasStationJson.TAG)) {
            station = new GasStationJson();
        } else {
            station = bundle.getParcelable(GasStationJson.TAG);
        }
    }

    public GasStationJson getGasStation() {
        return station;
    }

    @Bindable
    public String getName() {
        return station.getName();
    }

    public void setName(String name) {
        station.setName(name);
        registry.notifyChange(this, BR.name);
    }

    @Bindable
    public String getImageUrl() {
        return station.getImageUrl();
    }

    public void setImageUrl(String uri) {
        station.setImageUrl(uri);
        registry.notifyChange(this, BR.imageUrl);
    }

    @Bindable
    public List<FuelPriceJson> getFuelPrices() {
        return station.getFuelPrices();
    }

    @Bindable
    public String getAddress() {
        return station.getAddress();
    }

    public void setAddress(String address) {
        station.setAddress(address);
        registry.notifyChange(this, BR.address);
    }

    public void setLocation(LocationJson location) {
        LocationJson locationJson = new LocationJson();
        locationJson.setCoordinates(Arrays.asList(location.getLat(), location.getLon()));
        station.setLocation(locationJson);
    }

    public boolean hasLocation() {
        boolean a = MathUtils.isAEqualB(station.getLocation().getLat(), 0.0, 0.000001);
        boolean b = MathUtils.isAEqualB(station.getLocation().getLon(), 0.0, 0.000001);
        return !(a && b);
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }
}
