package lt.smworks.gasup.adapters;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.R;
import lt.smworks.gasup.databinding.AddGasStationFuelTypeItemBinding;
import lt.smworks.gasup.models.enums.FuelType;
import lt.smworks.gasup.models.json.FuelPriceJson;
import lt.smworks.tools.adapters.RecyclerViewAdapter;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.add_gas_station_fuel_type_item)
public class FuelTypeEditAdapter extends RecyclerViewAdapter<FuelPriceJson, FuelTypeEditAdapter.ItemViewHolder> {

    public FuelTypeEditAdapter(@NonNull Context context) {
        super(context);
        addItem(new FuelPriceJson());
    }

    @Override
    protected ItemViewHolder onCreateViewHolder(@NonNull View itemView) {
        AddGasStationFuelTypeItemBinding binding = DataBindingUtil.bind(itemView);
        return new ItemViewHolder(Objects.requireNonNull(binding));
    }

    @Override
    protected void onBindViewHolder(@NonNull ItemViewHolder holder, @NonNull FuelPriceJson item, int position) {
        holder.bind(item);
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private final AddGasStationFuelTypeItemBinding binding;

        private ItemViewHolder(AddGasStationFuelTypeItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            Spinner spinner = binding.getRoot().findViewById(R.id.addGasStationFuelTypeItem_fuelType);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                    R.layout.toolbar_spinner, FuelType.getSelectableValues(getContext().getResources()));
            adapter.setDropDownViewResource(R.layout.toolbar_spinner_dropdown);
            spinner.setAdapter(adapter);

            binding.getRoot().findViewById(R.id.addGasStationFuelTypeItem_remove).setOnClickListener(v -> {
                int adapterPosition = getAdapterPosition();
                new Handler().post(() -> FuelTypeEditAdapter.this.removeItem(adapterPosition));
            });
        }

        private void bind(FuelPriceJson item) {
            binding.setFuelPrice(item);
            binding.executePendingBindings();
        }
    }
}
