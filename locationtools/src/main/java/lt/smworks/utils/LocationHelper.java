package lt.smworks.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import lt.smworks.locationtools.R;
import lt.smworks.models.SettingTypes;
import lt.smworks.tools.interfaces.OnLocationListener;
import lt.smworks.tools.interfaces.OnResultListener;
import lt.smworks.tools.utils.L;
import lt.smworks.tools.utils.PermissionUtils;
import lt.smworks.tools.utils.Settings;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class LocationHelper {

    private static final String TAG = "LocationHelper";
    private static final int REQUEST_CHECK_SETTINGS = 24;
    private static final int SECOND = 1000;
    private static final float DEFAULT_LOCATION_DISPLACEMENT = 0.0f;
    private final Context context;
    private FusedLocationProviderClient client;
    private final LocationRequest locationRequest;
    private PendingIntent pendingIntent;
    private LocationCallback locationCallback;
    private boolean started;

    public LocationHelper(@NonNull Context context, @NonNull PendingIntent pendingIntent) {
        this.context = context;
        this.pendingIntent = pendingIntent;
        Settings settings = new Settings(context);
        locationRequest = getLocationRequest(
                settings.getInt(SettingTypes.AVERAGE_LOCATION_UPDATE_INTERVAL) * SECOND,
                settings.getInt(SettingTypes.FASTEST_LOCATION_UPDATE_INTERVAL) * SECOND, -1);
        client = new FusedLocationProviderClient(context);
    }

    public LocationHelper(@NonNull Context context, @NonNull OnLocationListener onLocationListener) {
        this.context = context;
        this.locationCallback = new LocationCallback() {
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null) {
                    onLocationListener.onLocationResult(locationResult.getLastLocation());
                }
            }

            public void onLocationAvailability(LocationAvailability availability) {
                L.d(TAG, "Location availability: %b", availability.isLocationAvailable());
            }
        };
        Settings settings = new Settings(context);
        locationRequest = getLocationRequest(
                settings.getInt(SettingTypes.AVERAGE_LOCATION_UPDATE_INTERVAL) * SECOND,
                settings.getInt(SettingTypes.FASTEST_LOCATION_UPDATE_INTERVAL) * SECOND, -1);
        client = new FusedLocationProviderClient(context);
    }

    public static Boolean getLocationAvailability(Intent intent) {
        LocationAvailability availability = LocationAvailability.extractLocationAvailability(intent);
        if (availability != null) {
            return availability.isLocationAvailable();
        }
        return null;
    }

    public static boolean hasResult(Intent intent) {
        return LocationResult.hasResult(intent);
    }

    public static Location extractLocation(Intent intent) {
        LocationResult locationResult = LocationResult.extractResult(intent);
        return locationResult.getLastLocation();
    }

    @SuppressLint("MissingPermission")
    public void start() {
        if (started) {
            return;
        }

        L.d(TAG, "Starting location updates");
        if (!PermissionUtils.hasPermissions(context, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)) {
            L.w(TAG, "Missing permissions for location");
            return;
        }
        if (pendingIntent != null) {
            client.requestLocationUpdates(locationRequest, pendingIntent);
        } else if (locationCallback != null) {
            client.requestLocationUpdates(locationRequest, locationCallback, null);
        }
        started = true;
    }

    public boolean stop() {
        if (!started) {
            return false;
        }

        L.d(TAG, "Stopping location updates");
        if (pendingIntent != null) {
            client.removeLocationUpdates(pendingIntent);
        } else {
            client.removeLocationUpdates(locationCallback);
        }
        started = false;
        return true;
    }

    @SuppressLint("MissingPermission")
    public void addOnLastLocationListener(OnLastLocationListener lastLocationListener) {
        if (!PermissionUtils.hasPermissions(context, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)) {
            L.w(TAG, "Missing permissions for last location");
            return;
        }

        client.getLastLocation().addOnSuccessListener(location -> {
                    if (location != null) {
                        lastLocationListener.onLastLocation(location);
                    } else {
                        L.w(TAG, "Unable to get last known location");
                    }
                }

        );
    }

    @NonNull
    public static LocationRequest getLocationRequest(int interval, int fastestInterval, int updateCount) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(interval);
        locationRequest.setFastestInterval(fastestInterval);
        locationRequest.setSmallestDisplacement(DEFAULT_LOCATION_DISPLACEMENT);
        if (updateCount > 0) {
            locationRequest.setNumUpdates(updateCount);
        }
        return locationRequest;
    }

    public boolean isStarted() {
        return started;
    }

    public static void checkSettings(@NonNull Activity activity,
                                     @Nullable OnResultListener onResultListener) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(activity).checkLocationSettings(builder.build());
        result.addOnFailureListener(e -> {
            if (!(e instanceof ApiException)) {
                L.ex(e);
                return;
            }
            ApiException exception = (ApiException) e;
            switch (exception.getStatusCode()) {
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) exception;
                        resolvable.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException | ClassCastException ex) {
                        L.ex(ex);
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Toast.makeText(activity, R.string.location_settings_not_satisfied,
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }).addOnSuccessListener(locationSettingsResponse -> {
            L.d(TAG, "Settings are successfully enabled");
            if (onResultListener != null) {
                onResultListener.onResult(true);
            }
        }).addOnFailureListener(e -> {
            L.w(TAG, "Required settings are not enabled");
            if (onResultListener != null) {
                onResultListener.onResult(false);
            }
        });
    }

    private static void logStates(@NonNull LocationSettingsStates states) {
        L.d(TAG, "Listing available location providers");
        L.d(TAG, "BLE present: %b", states.isBlePresent());
        L.d(TAG, "BLE usable: %b", states.isBleUsable());
        L.d(TAG, "GPS present: %b", states.isGpsPresent());
        L.d(TAG, "GPS usable: %b", states.isGpsUsable());
        L.d(TAG, "Location present: %b", states.isLocationPresent());
        L.d(TAG, "Location usable: %b", states.isLocationUsable());
        L.d(TAG, "Network present: %b", states.isNetworkLocationPresent());
        L.d(TAG, "Network usable: %b", states.isNetworkLocationUsable());
    }

    public static boolean onActivityResult(
            @NonNull Activity activity, int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                L.d(TAG, "User accepted states");
            } else if (resultCode == Activity.RESULT_CANCELED) {
                L.d(TAG, "User declined states. Finishing activity");
                activity.finish();
            }
            LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
            logStates(states);
            return true;
        }
        return false;
    }

    public interface OnLastLocationListener {
        void onLastLocation(Location location);
    }
}
