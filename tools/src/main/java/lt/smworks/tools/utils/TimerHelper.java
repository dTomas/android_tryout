/**
 * @author Martynas Šustavičius
 * @since 2012 02 01
 */
package lt.smworks.tools.utils;

/**
 * TimerHelper class helps to count frames per second and measure specified time intervals.
 */
public class TimerHelper {
	/** Time stamp. */
	private long timeStamp = 0;
	/** New time stamp. */
	private long currentTimeStamp = 0;
	/** Time between update calls. */
	private int delta = 0;
	/** Time interval between which update should return elapsed time in milliseconds. */
	private int timeInterval = 0;
	/** Time elapsed since last return of positive number. */
	private int elapsedTime = 0;
	/** Call count in time interval. */
	private int callCount = 0;
	
	/**
	 * Creates timer with default update value of 25 milliseconds.
	 */
	public TimerHelper() {
		this(25);
	}
	
	/**
	 * Creates timer with specified update interval in milliseconds.
	 * To measure update calls per second specify 1000 milliseconds for time interval and
	 * call getCPS method each time update returns anything but -1.
	 * @param timeInterval - time in milliseconds.
	 */
	public TimerHelper(int timeInterval) {
		this.timeInterval = timeInterval;
		this.timeStamp = System.nanoTime();
	}
	
	/**
	 * Updates time fields. If elapsed time is less than time interval,
	 * this method returns -1.
	 * @return Time difference between update calls in milliseconds.
	 */
	public int update() {
		currentTimeStamp = System.nanoTime();
		callCount++;
		delta = (int) ((currentTimeStamp - timeStamp) / 1000000);
		elapsedTime += delta;
		timeStamp = currentTimeStamp;
		if (elapsedTime >= timeInterval) {
			delta = elapsedTime; // Use delta just to store temporary value.
			elapsedTime = 0;
			return delta;
		} else {
			return -1;
		}
	}
	
	/**
	 * @return Update call count during previous time interval.
	 */
	public int getCPS() {
		int temp = callCount;
		callCount = 0;
		return temp;
	}
}
