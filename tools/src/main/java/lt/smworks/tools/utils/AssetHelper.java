/**
 * @author Martynas Šustavičius
 * @since 2014 05 28
 */
package lt.smworks.tools.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.content.res.AssetManager;

public class AssetHelper {

	/**
	 * Loads text file from assets folder.
	 * @param context - activity or service context.
	 * @param path - file path in assets folder.
	 * @return String containing content from text file, or null on failure.
	 */
	public static String loadTextFile(Context context, String path) {
		try {
			AssetManager am = context.getAssets();
			InputStream is = am.open(path);
			StringBuilder inputStringBuilder = new StringBuilder();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
			String line = bufferedReader.readLine();
	        while(line != null){
	            inputStringBuilder.append(line);
	            inputStringBuilder.append('\n');
	            line = bufferedReader.readLine();
	        }
	        return inputStringBuilder.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
