package lt.smworks.gasup.fragments;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import bolts.Continuation;
import bolts.Task;
import lt.smworks.gasup.R;
import lt.smworks.gasup.adapters.AmenityAdapter;
import lt.smworks.gasup.adapters.FuelTypeEditAdapter;
import lt.smworks.gasup.interfaces.RestService;
import lt.smworks.gasup.models.enums.Amenity;
import lt.smworks.gasup.models.json.GasStationJson;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.utils.L;
import retrofit2.Response;

@Layout(R.layout.add_gas_station_fragment)
public class EditGasStationFragment extends AddGasStationFragment {

    public static final String TAG = "EditGasStationFragment";
    private static final String PREFIX_HTTP = "http";

    @Override
    protected void onCreateView(@NonNull Context context) {
        super.onCreateView(context);
    }

    @Override
    protected void setupAdapters(@NonNull Context context) {
        FuelTypeEditAdapter fuelTypeAdapter = new FuelTypeEditAdapter(context);
        fuelTypeAdapter.setItems(viewModel.getFuelPrices());
        binding.setFuelTypeAdapter(fuelTypeAdapter);
        List<Integer> amenities = viewModel.getGasStation().getAmenities();
        List<Amenity> list = new ArrayList<>();
        for (Integer amenity : amenities) {
            list.add(Amenity.fromId(amenity));
        }
        binding.setAmenityAdapter(new AmenityAdapter(context, list, true));
    }

    @Override
    protected void uploadData() {
        showProgressDialog(R.string.updating_gas_station);
        viewModel.getGasStation().setUser(getMainActivity().getUser());
        GasStationJson station = viewModel.getGasStation();
        RestService restService = getMainActivity().getRestService();
        Task.callInBackground(() -> {
            if (!saveImage(station, restService)) {
                return getString(R.string.unable_to_save_image);
            }
            try {
                Response<Void> response = restService.updateGasStation(
                        station.getId(), station).execute();
                if (!response.isSuccessful()) {
                    return getString(R.string.unable_to_save_gas_station);
                }
            } catch (Exception e) {
                L.ex(e);
                return e.getMessage();
            }

            return null;
        }).continueWith((Continuation<String, Void>) task -> {
            hideProgressDialog();
            String error = task.getResult();
            if (error != null) {
                L.e(TAG, error);
                showSnackbarFailure(error);
            } else {
                showSnackbarSuccess(getString(R.string.gas_station_updated));
                getMainActivity().getSettings().setGasStationUpdateRequired(true);
                getMainActivity().popFragment();
            }
            return null;
        }, Task.UI_THREAD_EXECUTOR);
    }
}
