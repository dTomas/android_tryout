package lt.smworks.tools.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.interfaces.OnDialogResultListener;

public abstract class BaseDialogFragment<T> extends DialogFragment {

    public static final String KEY_ACTION = "action";
    public static final String VAL_POSITIVE = "positive";

    private FragmentManager fragmentManager;

    public BaseDialogFragment() {
        setArguments(new Bundle());
    }

    @SuppressWarnings("unchecked")
    public T withTargetFragment(@NonNull Fragment fragment) {
        this.fragmentManager = fragment.getFragmentManager();
        setTargetFragment(fragment, 0);
        return (T) this;
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (!getClass().isAnnotationPresent(Layout.class)) {
            throw new IllegalStateException("Child dialog fragment does not contain @Layout " +
                    "annotation");
        }

        if (getTargetFragment() == null) {
            throw new IllegalStateException("DialogFragment has no target fragment");
        }

        if (!(getTargetFragment() instanceof OnDialogResultListener)) {
            throw new IllegalStateException("Calling fragment must implement " +
                    "OnDialogResultListener interface");
        }

        Layout layout = getClass().getAnnotation(Layout.class);
        if (layout == null) {
            throw new IllegalStateException("Layout annotation present but it's value is null");
        }

        View view = View.inflate(getContext(), layout.value(), null);

        if (view == null) {
            throw new IllegalStateException("Unable to inflate layout with id: " + layout.value());
        }

        return onCreateDialog(view);
    }

    public abstract Dialog onCreateDialog(@NonNull View view);

    protected void sendResult(@NonNull Bundle bundle) {
        OnDialogResultListener listener = (OnDialogResultListener) getTargetFragment();
        if (listener != null) {
            listener.onDialogResult(bundle);
        }
    }

    public void show() {
        if (fragmentManager == null) {
            throw new IllegalStateException("No fragment manager specified. " +
                    "Make sure withTargetFragment() method is called");
        }
        show(fragmentManager, getClass().getName());
    }
}
