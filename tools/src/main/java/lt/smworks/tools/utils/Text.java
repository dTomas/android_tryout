/**
 * @author Martynas Šustavičius
 * @since 2012 01 30
 */
package lt.smworks.tools.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;


/**
 * This class is used to store text with all of its visible attributes.
 */
public class Text {
	/** Bold style. */
	public static final short STYLE_BOLD = 0;
	/** Italic style. */
	public static final short STYLE_ITALIC = 1;
	/** Underline style. */
	public static final short STYLE_UNDERLINE = 2;
	/** Text. */
	public String text = null;
	/** Text color. */
	public float[] color = null;
	/** Text size. */
	public short size = -1;
	/** Text style. */
	public short style = -1;
	/** Paint used to draw text and calculate its dimensions. */
	public Paint paint = new Paint();
	/** Width of the text. */
	public short width = -1;
	/** Height of the text. */
	public short height = -1;
	
	/**
	 * Creates text object with default parameters.
	 */
	public Text() {
		this("");
	}
	
	/**
	 * Creates text object with custom parameters.
	 * @param text - string containing symbols.
	 */
	public Text(String text) {
		this(text, new float[] {1.0f, 0.0f, 0.0f, 1.0f}, (short) 10, STYLE_BOLD);
	}
	
	/**
	 * Creates text object with custom parameters.
	 * @param text
	 * @param color
	 * @param size
	 * @param style
	 */
	public Text(String text, float[] color, short size, short style) {
		this.text = text;
		this.color = color;
		this.size = size;
		this.style = style;
		paint.setTextSize(size);
		paint.setColor(Color.WHITE);
		paint.setAntiAlias(true);
		
	}
	
	/**
	 * Generates texture which contains letters, numbers and symbols.
	 * Generated texture dimensions are powers of two.
	 * @return Bitmap containing text.
	 */
	public Bitmap getTexture() {
		// Finds rows in string.
		String[] rows = this.text.split("\n");
		// Calculates text height.
		this.height = (short) (this.size * rows.length);
		// Calculates text width.
		short texWidth;
		for (String row : rows) {
			texWidth = (short) this.paint.measureText(row);
			if (texWidth > this.width) {
				this.width = texWidth;
			}
		}
		// Generate symbol dimensions using current font.
		int i = 0;
		// Max width and height variables need to be in power of two.
		// This is because some phones doesn't support textures
		// whose dimensions aren't powers of two.
		for (i = 2; ; i *= 2) {
			if (this.width <= i) {
				this.width = (short) i;
				break;
			}
		}
		for (i = 2; ; i *= 2) {
			if (this.height <= i) {
				this.height = (short) i;
				break;
			}
		}
		// Create empty mutable bitmap.
		Bitmap bitmap = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_4444);
		// Use this bitmap as canvas surface.
		Canvas canvas = new Canvas(bitmap);
		bitmap.eraseColor(Color.TRANSPARENT);
		// Draw text to bitmap.
		short offset = 0;
		for (String row : rows) {
			canvas.drawText(row, 0, this.size + offset, paint);
			offset += this.size;
		}
		return bitmap;
	}
}
