package lt.smworks.gasup.fragments;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import androidx.annotation.NonNull;
import lt.smworks.gasup.R;
import lt.smworks.gasup.interfaces.RetrofitResult;
import lt.smworks.gasup.models.enums.HttpResponseCode;
import lt.smworks.gasup.models.json.UserJson;
import lt.smworks.gasup.utils.AuthUtils;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.utils.ValidationUtils;
import retrofit2.Response;

@Layout(R.layout.login_fragment)
public class LoginFragment extends GasupBaseFragment {

    private static final String TAG = "LoginFragment";
    private TextInputEditText emailInput;
    private TextInputEditText passwordInput;
    private CallbackManager callbackManager;

    @Override
    protected void onCreateView(@NonNull Context context) {
        emailInput = findView(R.id.emailAndPasswordForm_email);
        passwordInput = findView(R.id.emailAndPasswordForm_password);
        findView(R.id.loginFragment_register).setOnClickListener(this::register);

        new ValidationUtils.FormValidator()
                .setEmailInput(emailInput)
                .setPasswordInput(passwordInput)
                .setSubmitButton(findView(R.id.loginFragment_login))
                .setOnSubmitListener(this::login);

        LoginButton loginButton = findView(R.id.loginFragment_facebookLogin);
        loginButton.setOnClickListener(this::authWithFacebook);
        LoginManager.getInstance().logOut();
    }

    private void login() {
        getMainActivity().hideKeyboard();
        showProgressDialog(R.string.login_in_progress);
        String email = Objects.requireNonNull(emailInput.getText()).toString();
        String password = Objects.requireNonNull(passwordInput.getText()).toString();
        getMainActivity().getRestService().getUser(email, password)
                .enqueue((RetrofitResult<UserJson>) (data, error) -> {
                    hideProgressDialog();
                    onLogin(data, error);
                });
    }

    private void onLogin(Response<UserJson> data, Throwable error) {
        if (error != null) {
            showError(error);
            LoginManager.getInstance().logOut();
            return;
        }

        if (data.code() == HttpResponseCode.UNAUTHORIZED.getCode()) {
            showSnackbar(getString(R.string.login_failed));
            LoginManager.getInstance().logOut();
            return;
        }

        if (!data.isSuccessful()) {
            showSnackbarFailure(
                    data.message() != null ? data.message() : getString(R.string.unknown_error));
            LoginManager.getInstance().logOut();
            return;
        }

        getMainActivity().getSettings().setUser(data.body());
        getMainActivity().clearFragments();
        getMainActivity().showNavigationDrawer();
        getMainActivity().addFragment(new MainFragment());
    }

    private void register(View view) {
        getMainActivity().addFragment(new RegisterFragment());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void authWithFacebook(View view) {
        callbackManager = AuthUtils.getFacebookAccessToken(this, (fbToken, fbE) -> {
            if (fbE != null) {
                showError(fbE);
                LoginManager.getInstance().logOut();
                return;
            }
            showProgressDialog(R.string.login_in_progress);
            getMainActivity().getRestService().getUser(fbToken.getToken())
                    .enqueue((RetrofitResult<UserJson>) (data, error) -> {
                        hideProgressDialog();
                        onLogin(data, error);
                    });
        });
    }
}
