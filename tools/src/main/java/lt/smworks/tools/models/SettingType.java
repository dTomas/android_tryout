package lt.smworks.tools.models;

import android.content.Context;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import lt.smworks.tools.R;

public class SettingType {

    public static final SettingType LAST_LOCATION = new SettingType("last_location");

    @StringRes
    private int name = 0;
    private String defaultValue = "";
    @LayoutRes
    private int layout = 0;
    private boolean editable = false;
    @NonNull
    private final String key;

    public SettingType(@NonNull String key) {
        this.key = key;
    }

    public SettingType withName(@StringRes int nameStringRes) {
        this.name = nameStringRes;
        return this;
    }

    public SettingType withDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    public SettingType withLayout(@LayoutRes int layout) {
        this.layout = layout;
        return this;
    }

    public SettingType useInSettings() {
        this.editable = true;
        return this;
    }

    @StringRes
    public int getName() {
        return name;
    }

    public boolean isEditable() {
        return editable;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    @LayoutRes
    public int getLayout() {
        return layout;
    }

    @StringRes
    public int validate(@NonNull Context resources, @NonNull String content) {
        return R.string.ok;
    }

    @StringRes
    public int getHint() {
        return R.string.no_hint;
    }

    public String getKey() {
        return key;
    }

    public static class Constants {
        @LayoutRes
        public static final int LAYOUT_SLIDER = R.layout.settings_slider_item;
        @LayoutRes
        public static final int LAYOUT_DEFAULT = R.layout.settings_item;
    }
}