package lt.smworks.tools.interfaces;

public interface OnResultListener {

    void onResult(boolean success);
}
