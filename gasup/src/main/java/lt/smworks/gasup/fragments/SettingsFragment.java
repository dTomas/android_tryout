package lt.smworks.gasup.fragments;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.R;
import lt.smworks.gasup.adapters.GasupSettingsAdapter;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.settings_fragment)
public class SettingsFragment extends GasupBaseFragment {

    @Override
    protected void onCreateView(@NonNull Context context) {
        RecyclerView list = (RecyclerView) getRootView();
        list.setLayoutManager(new LinearLayoutManager(context));
        list.setAdapter(new GasupSettingsAdapter(context));
        list.setHasFixedSize(true);
    }
}
