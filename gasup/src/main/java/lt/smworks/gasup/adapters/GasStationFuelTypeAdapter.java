package lt.smworks.gasup.adapters;

import android.content.Context;
import android.view.View;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.R;
import lt.smworks.gasup.databinding.GasStationListItemBinding;
import lt.smworks.gasup.models.json.FuelPriceJson;
import lt.smworks.tools.adapters.RecyclerViewAdapter;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.gas_station_list_item)
public class GasStationFuelTypeAdapter extends RecyclerViewAdapter<FuelPriceJson, GasStationFuelTypeAdapter.ItemViewHolder> {

    public GasStationFuelTypeAdapter(@NonNull Context context) {
        super(context);
    }

    @Override
    protected ItemViewHolder onCreateViewHolder(@NonNull View itemView) {
        GasStationListItemBinding binding = DataBindingUtil.bind(itemView);
        return new ItemViewHolder(Objects.requireNonNull(binding));
    }

    @Override
    protected void onBindViewHolder(@NonNull ItemViewHolder holder, @NonNull FuelPriceJson item, int position) {
        holder.bind(item);
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        private final GasStationListItemBinding binding;

        private ItemViewHolder(GasStationListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(FuelPriceJson item) {
            binding.setModel(item);
            binding.executePendingBindings();
        }
    }
}