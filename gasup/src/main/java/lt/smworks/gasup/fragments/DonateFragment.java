package lt.smworks.gasup.fragments;

import android.content.Context;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import lt.smworks.gasup.R;
import lt.smworks.gasup.adapters.DonationButtonAdapter;
import lt.smworks.gasup.databinding.DonateFragmentBinding;
import lt.smworks.tools.adapters.RecyclerViewAdapter;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.utils.L;

@Layout(R.layout.donate_fragment)
public class DonateFragment extends GasupBaseFragment implements PurchasesUpdatedListener, RecyclerViewAdapter.OnItemClickListener<SkuDetails> {

    private static final String TAG = "DonateFragment";
    private BillingClient billingClient;
    private DonationButtonAdapter adapter;

    @Override
    protected void onCreateView(@NonNull Context context) {
        if (adapter == null) {
            adapter = new DonationButtonAdapter(context);
            adapter.setOnItemClickListener(this);
        }
        DonateFragmentBinding binding = Objects.requireNonNull(DataBindingUtil.bind(getRootView()));
        binding.setView(this);
        binding.setAdapter(adapter);
        setupBillingClient();
    }

    private void setupBillingClient() {
        if (billingClient != null) {
            return;
        }
        showProgressDialog(R.string.loading_donation_types);
        billingClient = BillingClient.newBuilder(getNonNullContext())
                .setListener(this)
                .build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    getAvailableDonationItems(billingClient);
                } else {
                    onBillingServiceDisconnected();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                hideProgressDialog();
                showSnackbarFailure(getString(R.string.billing_service_unavailable));
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (billingClient != null) {
            billingClient.endConnection();
            billingClient = null;
        }
    }

    private void getAvailableDonationItems(BillingClient billingClient) {
        List<String> skuList = new ArrayList<>();
        skuList.add("lt.smworks.gasup.donation1euro");
        skuList.add("lt.smworks.gasup.donation3euros");
        skuList.add("lt.smworks.gasup.donation5euros");
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        billingClient.querySkuDetailsAsync(params.build(), (responseCode, skuDetailsList) -> {
            hideProgressDialog();
            if (!isValid()) {
                return;
            }
            if (responseCode == BillingClient.BillingResponse.OK && skuDetailsList != null) {
                adapter.setItems(skuDetailsList);
            }
        });
    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        L.d(TAG, "Purchase updated with response code: " + responseCode);
        if (purchases == null) {
            return;
        }
        for (Purchase purchase : purchases) {
            L.d(TAG, "Purchase orderid(%s), token(%s), signature(%s)",
                    purchase.getOrderId(), purchase.getPurchaseToken(), purchase.getSignature());
            billingClient.consumeAsync(purchase.getPurchaseToken(), (rc, pt) -> purchaseToken(pt));
        }
    }

    private void purchaseToken(String purchaseToken) {
        L.i(TAG, "Successfully consumed purchase with token: " + purchaseToken);
        if (!isValid()) {
            return;
        }
        showSnackbarSuccess(getString(R.string.donation_successful));
    }

    @Override
    public void onItemClick(SkuDetails skuDetails) {
        BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(skuDetails)
                .build();
        int responseCode = billingClient.launchBillingFlow(getMainActivity(), flowParams);
        if (responseCode == BillingClient.BillingResponse.OK) {
            L.i(TAG, "Item successfully purchased");
        } else if (responseCode == BillingClient.BillingResponse.ITEM_ALREADY_OWNED) {
            L.w(TAG, "Item was already owned");
            Purchase.PurchasesResult purchasesResult = billingClient.queryPurchases(skuDetails.getType());
            List<Purchase> purchasesList = purchasesResult.getPurchasesList();
            for (Purchase purchase : purchasesList) {
                billingClient.consumeAsync(purchase.getPurchaseToken(), (rc, pt) -> purchaseToken(pt));
            }
        } else {
            L.d(TAG, "Unknown response code from launchBillingFlow: " + responseCode);
        }
    }
}
