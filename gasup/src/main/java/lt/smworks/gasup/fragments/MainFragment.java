package lt.smworks.gasup.fragments;

import android.Manifest;
import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import lt.smworks.gasup.R;
import lt.smworks.gasup.adapters.GasStationAdapter;
import lt.smworks.gasup.databinding.MainFragmentBinding;
import lt.smworks.gasup.interfaces.RetrofitResult;
import lt.smworks.gasup.interfaces.SpinnerItemSelectedListener;
import lt.smworks.gasup.models.enums.FuelType;
import lt.smworks.gasup.models.enums.SortType;
import lt.smworks.gasup.models.json.GasStationJson;
import lt.smworks.gasup.models.json.LocationJson;
import lt.smworks.gasup.utils.NetworkUtils;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.interfaces.OnLocationListener;
import lt.smworks.tools.models.SortOrder;
import lt.smworks.tools.utils.L;
import lt.smworks.utils.LocationHelper;
import retrofit2.Response;

@Layout(R.layout.main_fragment)
public class MainFragment extends GasupBaseFragment implements SwipeRefreshLayout.OnRefreshListener, OnLocationListener {

    private GasStationAdapter adapter;
    private FuelType fuelType;
    private LocationHelper locationHelper;
    private SortType sortType = SortType.PRICE;
    private MenuItem sortByDistanceMenuItem;
    private MenuItem sortByPriceMenuItem;
    private MenuItem sortByTimeMenuItem;
    private SortOrder sortOrder = SortOrder.ASCENDING;
    private boolean skipeAdapterUpdateOnSelectedItem = false;
    private MainFragmentBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getContext() == null) {
            return;
        }
        adapter = new GasStationAdapter(getContext());
        adapter.setOnItemClickListener(gasStation -> {
            GasStationFragment fragment = new GasStationFragment()
                    .withParcelable(GasStationJson.TAG, gasStation);
            getMainActivity().addFragment(fragment);
        });
    }

    @Override
    protected void onCreateView(@NonNull Context context) {
        setHasOptionsMenu(true);
        fuelType = getMainActivity().getSettings().getDefaultFuelType();
        binding = Objects.requireNonNull(DataBindingUtil.bind(getRootView()));
        binding.mainFragmentPullToRefresh.setOnRefreshListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        binding.mainFragmentList.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(binding.mainFragmentList.getContext(),
                layoutManager.getOrientation());
        binding.mainFragmentList.addItemDecoration(dividerItemDecoration);
        binding.mainFragmentList.setAdapter(adapter);
        askForLocationPermissions(context);
        if (getMainActivity().getSettings().isGasStationUpdateRequired()) {
            loadData(true);
        }
    }

    private void askForLocationPermissions(@NonNull Context context) {
        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};
        String description = getString(R.string.app_requires_access_to_gps_sensor);
        getMainActivity().askForPermission(description, granted -> {
            if (!granted) {
                showSnackbarFailure(getString(R.string.location_related_functions_will_not_work));
            } else {
                locationHelper = new LocationHelper(context, this);
                locationHelper.addOnLastLocationListener(location -> {
                    L.d(TAG, "Received last location");
                    onLocationResult(location);
                });
                locationHelper.start();
            }
        }, permissions);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (locationHelper != null) {
            locationHelper.stop();
            locationHelper = null;
        }
        skipeAdapterUpdateOnSelectedItem = true;
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    private void loadData(boolean usePullToRefreshPreloader) {
        L.d(TAG, "Calling load data. Location is unavailable: " + (binding.getLocation() == null));
        if (binding.getLocation() == null) {
            showProgressDialog(R.string.waiting_for_user_location);
            return;
        }

        getMainActivity().getSettings().setGasStationUpdateRequired(false);

        if (usePullToRefreshPreloader) {
            binding.mainFragmentPullToRefresh.setRefreshing(false);
        }

        showProgressDialog(R.string.loading_gas_stations);
        String coords = binding.getLocation().toString();
        if (fuelType == FuelType.ALL) {
            getMainActivity().getRestService().getGasStations(coords).enqueue(
                    (RetrofitResult<List<GasStationJson>>) this::setupData);
        } else {
            int id = fuelType.getId();
            getMainActivity().getRestService().getSortedGasStations(id, coords).enqueue(
                    (RetrofitResult<List<GasStationJson>>) this::setupData);
        }
    }

    private void setupData(Response<List<GasStationJson>> data, Throwable error) {
        hideProgressDialog();
        if (data == null) {
            adapter.clear();
            showErrorMessage(error);
            return;
        }
        List<GasStationJson> list = data.body();
        if (list == null || list.isEmpty()) {
            showErrorMessage(error);
            return;
        }

        Comparator<GasStationJson> comparator;
        if (sortType == SortType.DISTANCE) {
            comparator = new GasStationJson.DistanceComparator(binding.getLocation());
        } else if (sortType == SortType.PRICE) {
            comparator = new GasStationJson.PriceComparator(fuelType.getId());
        } else {
            comparator = new GasStationJson.UpdateTimeComparator();
        }
        if (sortOrder == SortOrder.ASCENDING) {
            Collections.sort(list, comparator);
        } else {
            Collections.sort(list, Collections.reverseOrder(comparator));
        }

        adapter.setItems(list);
    }

    private void showErrorMessage(Throwable error) {
        if (error != null) {
            showSnackbarFailure(error.getMessage());
            L.ex(error);
        } else if (!NetworkUtils.isNetworkAvailable(getMainActivity())) {
            showSnackbarFailure(getString(R.string.no_internet_connection));
        } else {
            showSnackbarFailure(getString(R.string.no_data_found));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.action_fuel_type_dropdown);
        Spinner spinner = (Spinner) item.getActionView();
        spinner.setBackgroundColor(Color.WHITE);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getMainActivity(),
                R.layout.toolbar_spinner_black, FuelType.getAllValues(getResources()));
        adapter.setDropDownViewResource(R.layout.toolbar_spinner_dropdown);
        spinner.setAdapter(adapter);
        spinner.setSelection(getMainActivity().getSettings().getDefaultFuelType().getId());
        spinner.setOnItemSelectedListener((SpinnerItemSelectedListener) this::onFuelTypeSelected);
        sortByDistanceMenuItem = menu.findItem(R.id.action_sort_by_distance);
        sortByPriceMenuItem = menu.findItem(R.id.action_sort_by_price);
        sortByTimeMenuItem = menu.findItem(R.id.action_sort_by_time);
        SortType type = getMainActivity().getSettings().getDefaultSortType();
        MenuItem selectedItem = getSelectedSortMenuItem(type);
        selectedItem.setIcon(R.drawable.ic_arrow_down_black);

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void onFuelTypeSelected(int position) {
        fuelType = FuelType.fromId(position);
        getMainActivity().getSettings().setDefaultFuelType(fuelType);
        if (skipeAdapterUpdateOnSelectedItem) {
            skipeAdapterUpdateOnSelectedItem = false;
        } else {
            loadData(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort_by_distance:
                if (binding.getLocation() == null) {
                    showSnackbarFailure(getString(R.string.user_location_unavailable));
                } else {
                    sortList(SortType.DISTANCE);
                }
                return true;
            case R.id.action_sort_by_price:
                sortList(SortType.PRICE);
                return false;
            case R.id.action_sort_by_time:
                sortList(SortType.TIME);
                return false;
            default:
                break;
        }
        return false;
    }

    @NonNull
    private MenuItem getSelectedSortMenuItem(SortType sortType) {
        switch (sortType) {
            case PRICE:
                return sortByPriceMenuItem;
            case DISTANCE:
                return sortByDistanceMenuItem;
            case TIME:
                return sortByTimeMenuItem;
        }
        return sortByTimeMenuItem;
    }

    private void sortList(SortType sortType) {
        sortByDistanceMenuItem.setIcon(null);
        sortByPriceMenuItem.setIcon(null);
        sortByTimeMenuItem.setIcon(null);
        if (this.sortType == sortType) {
            updateSortOrder(getSelectedSortMenuItem(sortType));
        } else {
            getSelectedSortMenuItem(sortType).setIcon(R.drawable.ic_arrow_down_black);
            sortOrder = SortOrder.ASCENDING;
            this.sortType = sortType;
            getMainActivity().getSettings().setDefaultSortType(sortType);
        }
        loadData(false);
    }

    private void updateSortOrder(MenuItem menuItem) {
        if (sortOrder == SortOrder.ASCENDING) {
            sortOrder = SortOrder.DESCENDING;
            menuItem.setIcon(R.drawable.ic_arrow_up_black);
        } else {
            sortOrder = SortOrder.ASCENDING;
            menuItem.setIcon(R.drawable.ic_arrow_down_black);
        }
    }

    @Override
    public void onLocationResult(@NonNull Location location) {
        L.d(TAG, "Received location: %f, %f", location.getLatitude(), location.getLongitude());
        boolean updateGasStations = binding.getLocation() == null;
        binding.setLocation(new LocationJson(location.getLatitude(), location.getLongitude()));
        adapter.updateDistances(binding.getLocation());
        if (updateGasStations) {
            loadData(false);
        }
    }
}