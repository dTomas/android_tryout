/**
 * @author Martynas Šustavičius
 * @since 2013 03 27
 */
package lt.smworks.tools.utils;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import android.annotation.SuppressLint;
import android.util.Base64;

public class CryptographyUtils {

    private static final String TAG = "CryptographyUtils";

    private static char[] getHexArray() {
        return new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    }

    private static String getRandomSymbols() {
        return "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }

    /**
     * Method used to convert byte data into hex.
     *
     * @param arr - byte array.
     * @return String containing hex data.
     */
    private static String convertBytesToHexString(byte[] arr) {
        StringBuilder buf = new StringBuilder();
        for (byte b : arr) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    /**
     * Method converts byte array into string.
     *
     * @param byteArray - byte array.
     * @return String representation of byte array.
     */
    public static String bytesToHex(byte[] byteArray) {
        char[] hexChars = new char[byteArray.length * 2];
        int v;
        char[] hexArray = getHexArray();
        for (int j = 0; j < byteArray.length; j++) {
            v = byteArray[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Generates string with random symbols.
     *
     * @param len - length of the random string.
     * @return Generated random string.
     */
    public static String randomString(int len) {
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        String randomSymbols = getRandomSymbols();
        for (int i = 0; i < len; i++) {
            sb.append(randomSymbols.charAt(rnd.nextInt(randomSymbols.length())));
        }
        return sb.toString();
    }

    /**
     * Encodes string using MD5 algorithm.
     *
     * @param str - string that should be encoded.
     * @return Encoded string.
     */
    public static String md5(String str) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes(), 0, str.length());
        } catch (NoSuchAlgorithmException e) {
            L.e(TAG, "Sha1 algorithm not supported.\nDetails: %s", e.toString());
            return null;
        }
        byte[] hash = md.digest();
        return convertBytesToHexString(hash);
    }

    /**
     * Encodes string using SHA1 algorithm.
     *
     * @param str - string that should be encoded.
     * @return Encoded string.
     */
    public static String sha1(String str) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
            md.update(str.getBytes(), 0, str.length());
        } catch (NoSuchAlgorithmException e) {
            L.e(TAG, "Sha1 algorithm not supported.\nDetails: %s", e.toString());
            return null;
        }
        byte[] hash = md.digest();
        return convertBytesToHexString(hash);
    }

    /**
     * Encodes string using SHA256 algorithm.
     *
     * @param str - string that should be encoded.
     * @return Encoded string.
     */
    public static String sha256(String str) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(str.getBytes(), 0, str.length());
        } catch (NoSuchAlgorithmException e) {
            L.e(TAG, "Sha1 algorithm not supported.\nDetails: %s", e.toString());
            return null;
        }
        byte[] hash = md.digest();
        return convertBytesToHexString(hash);
    }

    /**
     * Encodes string using base64 algorithm.
     *
     * @param str - string that should be encoded.
     * @return Encoded string.
     */
    public static String base64Encode(String str) {
        byte[] bytes = Base64.encode(str.getBytes(), Base64.NO_WRAP);
        return new String(bytes);
    }

    /**
     * Encodes byte array using base64 algorithm.
     *
     * @return Encoded string.
     */
    public static String base64Encode(byte[] arr) {
        return Base64.encodeToString(arr, Base64.NO_WRAP);
    }

    /**
     * Decodes string into byte array.
     *
     * @param str - base64 encoded byte array.
     * @return Byte array.
     */
    public static byte[] base64Decode(String str) {
        byte[] arr = null;
        if (str == null) {
            return arr;
        }
        try {
            arr = Base64.decode(str, Base64.NO_WRAP);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return arr;
    }

    /**
     * Encodes string using base64 encoding and then
     * removes all symbols that cannot be used for file name.
     *
     * @param string - string containing filename.
     * @return String with removed not supported symbols for file name.
     */
    public static String base64EncodeForFile(String string) {
        return base64Encode(string).replaceAll("[^a-zA-Z0-9]+", "");
    }

    /**
     * AES initialization vector.
     */
    private static byte[] iv = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0".getBytes();

    /**
     * Encodes byte array using AES encryption algorithm.
     *
     * @param message - bytes containing message.
     * @param key     - bytes containing special key.
     * @return Encoded byte array.
     */
    @SuppressLint("TrulyRandom")
    public static byte[] aesEncode(byte[] message, byte[] key) {
        IvParameterSpec spec = new IvParameterSpec(iv);
        SecretKeySpec sks = new SecretKeySpec(key, "AES");
        Cipher cipher = null;
        byte[] result = null;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.ENCRYPT_MODE, sks, spec);
            result = cipher.doFinal(message);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Decodes byte array using AES decryption algorithm.
     *
     * @param message - bytes containing message.
     * @param key     - bytes containing special key.
     * @return Decoded byte array.
     */
    public static byte[] aesDecode(byte[] message, byte[] key) {
        IvParameterSpec spec = new IvParameterSpec(iv);
        SecretKeySpec sks = new SecretKeySpec(key, "AES");
        Cipher cipher;
        byte[] result = null;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.DECRYPT_MODE, sks, spec);
            result = cipher.doFinal(message);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Password-Based Key Derivation Function 2.
     *
     * @param password         - master password from which a derived key is generated.
     * @param salt             - cryptographic salt. At least 64-bit salt is recommended.
     * @param iterationCount   - how many times to repeat encryption operation.
     *                         When the standard was written in 2000,
     *                         the recommended minimum number of iterations was 1000,
     *                         but the parameter is intended to be increased over time as CPU speeds increase.
     * @param derivedKeyLength - length of the generated key.
     * @return Derived key.
     */
    public static String pbkdf2(String password, String salt, int iterationCount, int derivedKeyLength) {
        byte[] hash = null;
        try {
            char[] chars = password.toCharArray();
            byte[] csalt = salt.getBytes();
            PBEKeySpec spec = new PBEKeySpec(chars, csalt, iterationCount, derivedKeyLength * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hash = skf.generateSecret(spec).getEncoded();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        return bytesToHex(hash);
        //return bytesToHex(Native.pbkdf2(password, salt, iterationCount, derivedKeyLength));
    }

}
