package lt.smworks.gasup.models.enums;

public enum HttpResponseCode {
    UNAUTHORIZED(401),
    CONFLICT(409);

    private final int code;

    HttpResponseCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
