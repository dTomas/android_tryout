package lt.smworks.vehicleguard.models;

import android.content.Context;
import android.text.TextUtils;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import lt.smworks.tools.models.SettingType;
import lt.smworks.tools.utils.L;
import lt.smworks.vehicleguard.R;

public final class SettingTypes {

    public static final SettingType SERVICE_STARTED = new SettingType("service_started")
            .withDefaultValue("false");
    public static final SettingType PHONE_NUMBER = new SettingType("phone_number") {
        @Override
        @StringRes
        public int validate(@NonNull Context context, @NonNull String content) {
            if (TextUtils.isEmpty(content)) {
                return R.string.phone_number_is_empty;
            }

            PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
            try {
                Phonenumber.PhoneNumber number = instance.parse(content, "");
                if (instance.isValidNumber(number)) {
                    return R.string.ok;
                }
            } catch (NumberParseException e) {
                L.ex(e);
            }

            return R.string.phone_number_doesnt_comply_with_international_standard;
        }

        @Override
        public int getHint() {
            return R.string.please_enter_internationl_phone_number;
        }
    }.withName(R.string.phone_number)
            .withDefaultValue("")
            .withLayout(SettingType.Constants.LAYOUT_DEFAULT)
            .useInSettings();

    public static final SettingType SENSOR_SENSITIVITY = new SettingType("sensor_sensitivity")
            .withName(R.string.sensitivity_threshold)
            .withDefaultValue("5")
            .withLayout(SettingType.Constants.LAYOUT_SLIDER)
            .useInSettings();
}
