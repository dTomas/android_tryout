package lt.smworks.gasup;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.Nullable;
import lt.smworks.gasup.models.enums.FuelType;
import lt.smworks.gasup.models.enums.SortType;
import lt.smworks.gasup.models.json.UserJson;

public final class Settings {

    public static final String ID = "smghost_settings";
    private static final String DEFAULT_FUEL_TYPE = "smghost_default_fuel_type";
    private static final String DEFAULT_SORT_TYPE = "smghost_default_sort_type";
    private static final String IS_GAS_STATION_UPDATE_REQUIRED = "smghost_is_gas_station_update_required";
    public static final String USER = "smghost_user";
    private SharedPreferences sharedPreferences;

    public Settings(Context context) {
        sharedPreferences = context.getSharedPreferences(ID, Activity.MODE_PRIVATE);
    }

    public void setDefaultFuelType(FuelType fuelType) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(DEFAULT_FUEL_TYPE, fuelType.getId());
        editor.apply();
    }

    public FuelType getDefaultFuelType() {
        int id = sharedPreferences.getInt(DEFAULT_FUEL_TYPE, FuelType.ALL.getId());
        return FuelType.fromId(id);
    }

    public void setDefaultSortType(SortType sortType) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(DEFAULT_SORT_TYPE, sortType.getId());
        editor.apply();
    }

    public SortType getDefaultSortType() {
        int id = sharedPreferences.getInt(DEFAULT_SORT_TYPE, SortType.PRICE.getId());
        return SortType.fromId(id);
    }

    public boolean isGasStationUpdateRequired() {
        return sharedPreferences.getBoolean(IS_GAS_STATION_UPDATE_REQUIRED, true);
    }

    public void setGasStationUpdateRequired(boolean state) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_GAS_STATION_UPDATE_REQUIRED, state);
        editor.apply();
    }

    public void setUser(UserJson user) {
        if (user == null) {
            return;
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER, user.toString());
        editor.apply();
    }

    @Nullable
    public UserJson getUser() {
        String userJson = sharedPreferences.getString(USER, "");
        if (userJson == null || userJson.isEmpty()) {
            return null;
        }
        return UserJson.fromString(userJson);
    }

    public void clearUser() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER, "");
        editor.apply();
    }
}
