package lt.smworks.models;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import lt.smworks.locationtools.R;
import lt.smworks.tools.models.SettingType;

public class SettingTypes {

    public static final SettingType FASTEST_LOCATION_UPDATE_INTERVAL = new SettingType("fastest_location_update_interval") {
        @Override
        @StringRes
        public int validate(@NonNull Context context, @NonNull String content) {
            if (TextUtils.isEmpty(content)) {
                return R.string.interval_should_be_at_least_30_seconds;
            }

            long value = Integer.valueOf(content);
            if (value < 30) {
                return R.string.interval_should_be_at_least_30_seconds;
            }

            return R.string.ok;
        }

        @Override
        public int getHint() {
            return R.string.please_enter_location_update_interval;
        }
    }.withName(R.string.fastest_location_update_interval)
            .withDefaultValue("60")
            .withLayout(SettingType.Constants.LAYOUT_DEFAULT).useInSettings();

    public static final SettingType AVERAGE_LOCATION_UPDATE_INTERVAL = new SettingType("average_location_update_interval") {
        @Override
        @StringRes
        public int validate(@NonNull Context context, @NonNull String content) {
            if (TextUtils.isEmpty(content)) {
                return R.string.interval_should_be_at_least_60_seconds;
            }

            long value = Integer.valueOf(content);
            if (value < 30) {
                return R.string.interval_should_be_at_least_60_seconds;
            }

            return R.string.ok;
        }

        @Override
        public int getHint() {
            return R.string.please_enter_location_update_interval;
        }
    }.withName(R.string.average_location_update_interval)
            .withDefaultValue("120")
            .withLayout(SettingType.Constants.LAYOUT_DEFAULT).useInSettings();

}
