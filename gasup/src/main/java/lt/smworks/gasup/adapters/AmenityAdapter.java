package lt.smworks.gasup.adapters;

import android.content.Context;
import android.view.View;
import android.widget.CheckedTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.R;
import lt.smworks.gasup.models.enums.Amenity;
import lt.smworks.tools.adapters.RecyclerViewAdapter;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.amenity_adapter_item)
public class AmenityAdapter extends RecyclerViewAdapter<Amenity, AmenityAdapter.ItemViewHolder> {

    private Map<Amenity, Boolean> selectionStates = new HashMap<>();
    private final boolean enableCheck;
    private OnCheckListener onCheckListener;

    public AmenityAdapter(@NonNull Context context) {
        super(context);
        List<Amenity> items = Amenity.toList();
        addItems(items);
        for (Amenity item : items) {
            selectionStates.put(item, false);
        }
        enableCheck = true;
    }

    public AmenityAdapter(@NonNull Context context,
                          List<Amenity> checkedAmenities,
                          boolean enableCheck) {
        super(context);
        addItems(Amenity.toList());
        for (Amenity item : checkedAmenities) {
            selectionStates.put(item, true);
        }
        this.enableCheck = enableCheck;
    }

    public void setOnCheckListener(OnCheckListener listener) {
        this.onCheckListener = listener;
    }

    @Override
    protected ItemViewHolder onCreateViewHolder(@NonNull View itemView) {
        return new ItemViewHolder(itemView);
    }

    @Override
    protected void onBindViewHolder(@NonNull ItemViewHolder holder,
                                    @NonNull Amenity item,
                                    int position) {
        holder.textView.setText(item.getStringResource());
        Boolean state = selectionStates.get(item);
        holder.textView.setChecked(state == null ? false : state);
        if (!enableCheck) {
            return;
        }
        holder.textView.setOnClickListener(v -> {
            holder.textView.toggle();
            selectionStates.put(item, holder.textView.isChecked());
            if (onCheckListener != null) {
                onCheckListener.onCheck(getCheckedAmenities());
            }
        });
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        private final CheckedTextView textView;

        private ItemViewHolder(View itemView) {
            super(itemView);
            this.textView = (CheckedTextView) itemView;
        }
    }

    public List<Amenity> getCheckedAmenities() {
        List<Amenity> amenities = new ArrayList<>();
        for (Map.Entry<Amenity, Boolean> entry : selectionStates.entrySet()) {
            if (entry.getValue()) {
                amenities.add(entry.getKey());
            }
        }
        return amenities;
    }

    public interface OnCheckListener {
        void onCheck(List<Amenity> checkedItems);
    }
}
