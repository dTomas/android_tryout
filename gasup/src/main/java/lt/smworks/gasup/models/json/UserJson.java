package lt.smworks.gasup.models.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import lt.smworks.gasup.interfaces.User;
import lt.smworks.tools.utils.L;

public class UserJson implements Parcelable, User {

    public static final String KEY_USER_ID = "userId";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_AVATAR_URL = "avatarUrl";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_DISCOUNT_CARDS = "discount_cards";

    @Json(name = "_id")
    private String userId;
    private String username;
    private String email;
    private String avatarUrl;
    private String token;
    private List<DiscountCardJson> discountCards;

    public UserJson() {

    }

    protected UserJson(Parcel in) {
        userId = in.readString();
        username = in.readString();
        email = in.readString();
        avatarUrl = in.readString();
        token = in.readString();
        discountCards = in.createTypedArrayList(DiscountCardJson.CREATOR);
    }

    public static final Creator<UserJson> CREATOR = new Creator<UserJson>() {
        @Override
        public UserJson createFromParcel(Parcel in) {
            return new UserJson(in);
        }

        @Override
        public UserJson[] newArray(int size) {
            return new UserJson[size];
        }
    };

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Override
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @NonNull
    public List<DiscountCardJson> getDiscountCards() {
        if (discountCards == null) {
            discountCards = new ArrayList<>();
        }
        return discountCards;
    }

    public void setDiscountCards(List<DiscountCardJson> discountCards) {
        this.discountCards = discountCards;
    }

    public static UserJson fromString(String json) {
        UserJson user = new UserJson();
        try {
            JSONObject jsonObject = new JSONObject(json);
            user.setUserId(jsonObject.optString(KEY_USER_ID));
            user.setUsername(jsonObject.optString(KEY_USERNAME));
            user.setEmail(jsonObject.optString(KEY_EMAIL));
            user.setAvatarUrl(jsonObject.optString(KEY_AVATAR_URL));
            user.setToken(jsonObject.optString(KEY_TOKEN));
            JSONArray discountCards = jsonObject.optJSONArray(KEY_DISCOUNT_CARDS);
            List<DiscountCardJson> discountCardJsons = new ArrayList<>();
            if (discountCards != null) {
                for (int i = 0; i < discountCards.length(); i++) {
                    JSONObject discountCard = (JSONObject) discountCards.get(i);
                    discountCardJsons.add(new DiscountCardJson(discountCard));
                }
            }
            user.setDiscountCards(discountCardJsons);
        } catch (JSONException e) {
            L.ex(e);
        }
        return user;
    }

    @NonNull
    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_USER_ID, userId);
            jsonObject.put(KEY_USERNAME, username);
            jsonObject.put(KEY_EMAIL, email);
            jsonObject.put(KEY_AVATAR_URL, avatarUrl);
            jsonObject.put(KEY_TOKEN, token);
        } catch (JSONException e) {
            L.ex(e);
        }
        return jsonObject.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(username);
        dest.writeString(email);
        dest.writeString(avatarUrl);
        dest.writeString(token);
        dest.writeTypedList(discountCards);
    }
}
