package lt.smworks.tools.utils;


import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.IllegalFormatConversionException;
import java.util.List;
import java.util.Locale;
import java.util.UnknownFormatConversionException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import lt.smworks.tools.BuildConfig;

public final class L {

    private static final BlockingQueue<String> logQueue = new ArrayBlockingQueue<>(100);

    private static final int MAX_SINGLE_MSG_LENGTH = 4000;
    private static final DateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss", Locale.getDefault());
    private static final String LOG_FILE_LOCATION = "log.txt";
    private static final int MAX_LOG_LINE_COUNT = 10000;
    private static final int OLD_LOG_LINES_TO_TRANSFER = 1000;
    public static final String TAG = "L";
    private File logFileDir;
    private OnLogExceptionListener onLogExceptionListener;
    private final Object lock = new Object();


    private static final L instance = new L();
    private Thread loggerThread;

    public static L getInstance() {
        return instance;
    }

    private L() {
    }

    /**
     * Log file location
     *
     * @param logFileDir - usually result from getFilesDir()
     * @return this
     */
    public L setup(@NonNull File logFileDir) {
        if (loggerThread != null && loggerThread.isAlive()) {
            return this;
        }
        this.logFileDir = logFileDir;
        loggerThread = new Thread(this::startLogger);
        loggerThread.start();
        return this;
    }

    public void setOnLogExceptionListener(OnLogExceptionListener onLogExceptionListener) {
        this.onLogExceptionListener = onLogExceptionListener;
    }

    public synchronized static void ex(@NonNull String tag, @NonNull Throwable e) {
        getInstance().logException(tag, e);
    }

    @Deprecated
    public synchronized static void e(Exception e) {
        getInstance().logException("", e);
    }

    public synchronized static void ex(Throwable e) {
        getInstance().logException("", e);
    }

    private void logException(@NonNull String tag, @NonNull Throwable e) {
        if (e == null) {
            L.e(tag, "Exception log attempt failed with no exception");
            return;
        }
        L.e(tag, e.getLocalizedMessage());
        appendLogToFile(e.getMessage());
        if (onLogExceptionListener != null) {
            onLogExceptionListener.onLogException(e);
        }
        e.printStackTrace();
    }

    public synchronized static void e(@NonNull String tag, @NonNull String msg, Object... args) {
        try {
            String message = formatMessage(msg, args);
            android.util.Log.e(tag, message);
            appendLogToFile(message);
        } catch (IllegalFormatConversionException e) {
            L.ex(TAG, e);
        }
    }

    public synchronized static void w(@NonNull String tag, @NonNull String msg, Object... args) {
        try {
            String message = formatMessage(msg, args);
            android.util.Log.w(tag, message);
            appendLogToFile(message);
        } catch (IllegalFormatConversionException e) {
            L.e(e);
        }
    }

    public synchronized static void i(@NonNull String tag, @NonNull String msg, Object... args) {
        try {
            String message = formatMessage(msg, args);
            android.util.Log.i(tag, message);
            appendLogToFile(message);
        } catch (IllegalFormatConversionException e) {
            L.e(e);
        }
    }

    public synchronized static void d(@NonNull String tag, @NonNull String msg, Object... args) {
        String message;
        int length;
        try {
            message = formatMessage(msg, args);
            length = message.length();
        } catch (IllegalFormatConversionException | UnknownFormatConversionException e) {
            L.ex(e);
            length = 0;
            message = "";
        }
        appendLogToFile(message);

        if (!BuildConfig.DEBUG) {
            return;
        }
        if (length > MAX_SINGLE_MSG_LENGTH) {
            for (int i = 0; i < length / MAX_SINGLE_MSG_LENGTH + 1; i++) {
                int to = i * MAX_SINGLE_MSG_LENGTH + MAX_SINGLE_MSG_LENGTH;
                android.util.Log.d(tag,
                        message.substring(i * MAX_SINGLE_MSG_LENGTH, to > length ? length : to));
            }
        } else {
            android.util.Log.d(tag, message);
        }
    }

    /**
     * Converts stack trace to string.
     *
     * @param elems - stack trace elements.
     * @return String containing stack trace.
     */
    private synchronized static String stackTraceToStr(StackTraceElement[] elems) {
        StringBuilder sb = new StringBuilder();
        boolean firstFound = false;
        for (StackTraceElement ste : elems) {
            if (!firstFound) {
                firstFound = true;
                continue;
            }
            sb.append(ste.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    private synchronized static String formatMessage(String msg, Object... args) {
        String date = dateFormat.format(Calendar.getInstance().getTime());
        return String.format(Locale.US, "[%s] %s", date, String.format(Locale.US, msg, args));
    }

    private static void appendLogToFile(String text) {
        if (getInstance().logFileDir == null) {
            return;
        }
        logQueue.add(text);
    }

    private void startLogger() {
        File logFile = IOUtils.getOrCreateFile(logFileDir, LOG_FILE_LOCATION);
        if (logFile == null) {
            return;
        }

        int lineCount = IOUtils.getLineCount(logFile);


        while (!Thread.currentThread().isInterrupted()) {
            String text;
            try {
                text = logQueue.take();
            } catch (InterruptedException e) {
                continue;
            }
            synchronized (lock) {
                if (lineCount > MAX_LOG_LINE_COUNT) {
                    List<String> lastLines = IOUtils.readLastNLines(
                            logFile, OLD_LOG_LINES_TO_TRANSFER);
                    lastLines.add(0,
                            "Clearing old log file. Starting from last 100 rows");
                    if (!IOUtils.writeToFile(logFile, lastLines)) {
                        return;
                    }
                    lineCount = 0;
                }

                if (!IOUtils.appendToFile(logFile, text)) {
                    return;
                }

                lineCount++;
            }
        }
    }

    @WorkerThread
    public boolean copyLogToFile(@NonNull File destination) {
        synchronized (lock) {
            File source = IOUtils.getOrCreateFile(logFileDir, LOG_FILE_LOCATION);
            return IOUtils.copyFile(source, destination);
        }
    }

    public interface OnLogExceptionListener {
        void onLogException(Throwable e);
    }
}
