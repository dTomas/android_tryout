package lt.smworks.tools.utils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.tools.adapters.RecyclerViewAdapter;

public final class ListUtils {

    public static void setupList(@NonNull RecyclerView recyclerView,
                                 @NonNull RecyclerViewAdapter adapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(adapter.getContext()));
        recyclerView.setAdapter(adapter);
    }

    public static void setupFixedList(@NonNull RecyclerView recyclerView,
                                      @NonNull RecyclerViewAdapter adapter) {
        setupList(recyclerView, adapter);
        recyclerView.setHasFixedSize(true);
    }

    public static void setupGridList(@NonNull RecyclerView recyclerView,
                                     @NonNull RecyclerViewAdapter adapter,
                                     int colCount) {
        recyclerView.setLayoutManager(new GridLayoutManager(adapter.getContext(), colCount));
        recyclerView.setAdapter(adapter);
    }

    public static void setupFixedGridList(@NonNull RecyclerView recyclerView,
                                     @NonNull RecyclerViewAdapter adapter,
                                     int colCount) {
        setupGridList(recyclerView, adapter, colCount);
        recyclerView.setHasFixedSize(true);
    }
}
