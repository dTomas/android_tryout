package lt.smworks.tools.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.tools.interfaces.Layout;

public abstract class RecyclerViewAdapter<T, VH extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<VH> {

    protected final Context context;
    private final List<T> items = new ArrayList<>();
    protected OnItemClickListener<T> onItemClickListener;

    public RecyclerViewAdapter(@NonNull Context context) {
        this.context = context;
    }

    protected abstract VH onCreateViewHolder(@NonNull View itemView);

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (!getClass().isAnnotationPresent(Layout.class)) {
            throw new IllegalStateException("Child of RecyclerViewAdapter doesn't provide " +
                    "Layout annotation");
        }
        Layout layout = getClass().getAnnotation(Layout.class);

        return onCreateViewHolder(inflater.inflate(layout.value(), parent, false));
    }

    protected abstract void onBindViewHolder(@NonNull VH holder, @NonNull T item, int position);

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(v -> onItemClickListener.onItemClick(items.get(position)));
        }
        onBindViewHolder(holder, items.get(position), position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public void setOnItemClickListener(OnItemClickListener<T> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void clear() {
        int itemCount = items.size();
        items.clear();
        notifyItemRangeRemoved(0, itemCount);
    }

    public void addItem(@NonNull T item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void addItems(List<? extends T> items) {
        int prevListSize = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(prevListSize, prevListSize + items.size());
    }

    public void setItems(List<? extends T> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void updateItem(@NonNull T item, int position) {
        items.set(position, item);
        notifyItemChanged(position);
    }

    public T getItem(int position) {
        return items.get(position);
    }

    public List<T> getItems() {
        return items;
    }

    public Context getContext() {
        return context;
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnItemClickListener<T> {
        void onItemClick(T t);
    }
}
