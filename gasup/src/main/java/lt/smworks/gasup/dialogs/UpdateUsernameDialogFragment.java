package lt.smworks.gasup.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.WindowManager;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import lt.smworks.gasup.R;
import lt.smworks.gasup.databinding.UpdateUsernameDialogFragmentBinding;
import lt.smworks.tools.dialogs.BaseDialogFragment;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.update_username_dialog_fragment)
public class UpdateUsernameDialogFragment extends BaseDialogFragment<UpdateUsernameDialogFragment> {

    public static final String KEY_USERNAME = "username";

    @Override
    public Dialog onCreateDialog(@NonNull View view) {
        UpdateUsernameDialogFragmentBinding binding = Objects.requireNonNull(DataBindingUtil.bind(view));

        AlertDialog d = new AlertDialog.Builder(view.getContext())
                .setTitle(R.string.available_fuel_types)
                .setView(view)
                .setPositiveButton(R.string.update, (dialog, which) -> {
                    Bundle bundle = new Bundle();
                    bundle.putString(KEY_ACTION, VAL_POSITIVE);
                    Editable text = binding.updateUsernameDialogFragmentUsername.getText();
                    if (text != null) {
                        bundle.putString(KEY_USERNAME, text.toString());
                    }
                    sendResult(bundle);
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
                .show();

        if (d.getWindow() != null) {
            d.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        }
        return d;
    }
}
