package lt.smworks.tools.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.ScriptIntrinsicBlur;

public class RenderScript {

	public static Bitmap applyGaussianBlur(Activity activity, Bitmap bitmap, float radius) {
		if (DeviceInfo.getAndroidVersion() < Build.VERSION_CODES.JELLY_BEAN_MR1) {
			return null;
		}
	    // Create another bitmap that will hold the results of the filter.
	    Bitmap blurredBitmap = Bitmap.createBitmap(bitmap);
	    // Create the Renderscript instance that will do the work.
	    android.renderscript.RenderScript rs = android.renderscript.RenderScript.create(activity);
	    // Allocate memory for Renderscript to work with
	    Allocation input = Allocation.createFromBitmap(
	    	rs, bitmap, Allocation.MipmapControl.MIPMAP_FULL, Allocation.USAGE_SCRIPT);
	    Allocation output = Allocation.createTyped(rs, input.getType());
	    // Load up an instance of the specific script that we want to use.
	    ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create (rs, Element.U8_4 (rs));
	    script.setInput(input);
	    // Set the blur radius
	    script.setRadius(radius);
	    // Start the ScriptIntrinisicBlur
	    script.forEach(output);
	    // Copy the output to the blurred bitmap
	    output.copyTo(blurredBitmap);
	    return blurredBitmap;
	}
}
