package lt.smworks.gasup.models.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import lt.smworks.gasup.models.enums.FuelType;
import lt.smworks.gasup.utils.MathUtils;


public class GasStationJson implements Parcelable {

    public static final transient String TAG = "GasStationJson";
    private static final double DELTA = 0.000001;

    @Json(name = "_id")
    private String id;
    private UserJson user;
    private String name;
    private String address;
    private String imageUrl;
    private List<FuelPriceJson> fuelPrices;
    private List<Integer> amenities;
    private LocationJson location;
    private long createdAt;
    private long updatedAt;

    public GasStationJson() {
        fuelPrices = new ArrayList<>();
        amenities = new ArrayList<>();
    }

    protected GasStationJson(Parcel in) {
        id = in.readString();
        user = in.readParcelable(UserJson.class.getClassLoader());
        name = in.readString();
        address = in.readString();
        imageUrl = in.readString();
        fuelPrices = in.createTypedArrayList(FuelPriceJson.CREATOR);
        location = in.readParcelable(LocationJson.class.getClassLoader());
        createdAt = in.readLong();
        updatedAt = in.readLong();
    }

    public static final Creator<GasStationJson> CREATOR = new Creator<GasStationJson>() {
        @Override
        public GasStationJson createFromParcel(Parcel in) {
            return new GasStationJson(in);
        }

        @Override
        public GasStationJson[] newArray(int size) {
            return new GasStationJson[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<FuelPriceJson> getFuelPrices() {
        if (fuelPrices == null) {
            fuelPrices = new ArrayList<>();
        }
        return fuelPrices;
    }

    public void setFuelPrices(List<FuelPriceJson> fuelPrices) {
        this.fuelPrices = fuelPrices;
    }

    public List<Integer> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Integer> amenities) {
        this.amenities = amenities;
    }

    public LocationJson getLocation() {
        if (location == null) {
            location = new LocationJson();
        }
        return location;
    }

    public void setLocation(LocationJson location) {
        this.location = location;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeParcelable(user, flags);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(imageUrl);
        dest.writeTypedList(fuelPrices);
        dest.writeParcelable(location, flags);
        dest.writeLong(createdAt);
        dest.writeLong(updatedAt);
    }

    private double getPrice(int fuelTypeId, double defaultValue) {
        for (FuelPriceJson fuelPriceDb : fuelPrices) {
            if (fuelPriceDb.getFuelTypeId() == fuelTypeId) {
                return fuelPriceDb.getPrice();
            }
        }
        return defaultValue;
    }

    public UserJson getUser() {
        return user;
    }

    public void setUser(UserJson user) {
        this.user = user;
    }

    public static class PriceComparator implements Comparator<GasStationJson> {

        private final int fuelTypeId;

        public PriceComparator(int fuelTypeId) {
            this.fuelTypeId = fuelTypeId == FuelType.ALL.getId()
                    ? FuelType.PETROL_95.getId() : fuelTypeId;
        }

        @Override
        public int compare(GasStationJson o1, GasStationJson o2) {
            double defVal = 999.0;
            double left = o1.getPrice(fuelTypeId, defVal);
            double right = o2.getPrice(fuelTypeId, defVal);
            if (MathUtils.isAEqualB(left, right, DELTA)) {
                return 0;
            } else if (left < right) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    public static class DistanceComparator implements Comparator<GasStationJson> {

        private final LocationJson location;

        public DistanceComparator(LocationJson location) {
            this.location = location;
        }

        @Override
        public int compare(GasStationJson o1, GasStationJson o2) {
            double left = o1.getLocation().distanceTo(location);
            double right = o2.getLocation().distanceTo(location);
            if (MathUtils.isAEqualB(left, right, DELTA)) {
                return 0;
            } else if (left < right) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    public static class UpdateTimeComparator implements Comparator<GasStationJson> {

        @Override
        public int compare(GasStationJson o1, GasStationJson o2) {
            if (o1.updatedAt == o2.updatedAt) {
                return 0;
            } else if (o1.updatedAt < o2.updatedAt) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}
