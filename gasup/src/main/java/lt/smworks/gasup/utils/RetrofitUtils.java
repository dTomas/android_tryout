package lt.smworks.gasup.utils;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public final class RetrofitUtils {

    private RetrofitUtils() {
    }

    public static MultipartBody.Part getImageForUpload(String pathToImage) {
        File file = new File(pathToImage);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        return MultipartBody.Part.createFormData("imageParameterName", file.getName(), reqFile);
    }
}
