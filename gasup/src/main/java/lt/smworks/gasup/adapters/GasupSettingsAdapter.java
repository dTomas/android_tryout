package lt.smworks.gasup.adapters;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import lt.smworks.tools.adapters.SettingsAdapter;
import lt.smworks.tools.models.SettingType;

public class GasupSettingsAdapter extends SettingsAdapter {

    public GasupSettingsAdapter(Context context) {
        super(context);
    }

    @Override
    protected void setupSettingItems() {
        List<SettingType> settingList = new ArrayList<>();
        settingList.add(lt.smworks.models.SettingTypes.AVERAGE_LOCATION_UPDATE_INTERVAL);
        settingList.add(lt.smworks.models.SettingTypes.FASTEST_LOCATION_UPDATE_INTERVAL);
        setItems(settingList);
    }
}
