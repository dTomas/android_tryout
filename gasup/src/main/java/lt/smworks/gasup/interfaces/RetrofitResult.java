package lt.smworks.gasup.interfaces;

import androidx.annotation.NonNull;
import lt.smworks.tools.utils.L;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public interface RetrofitResult<T> extends Callback<T> {

    String TAG = "RetrofitResult";

    @Override
    default void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        onResult(response, null);
    }

    @Override
    default void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        onResult(null, t);
        L.ex(TAG, t);
    }

    void onResult(Response<T> data, Throwable error);
}
