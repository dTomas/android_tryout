package lt.smworks.tools.interfaces;

import android.widget.SeekBar;

public interface SimpleSeekBarListener extends SeekBar.OnSeekBarChangeListener {

    @Override
    default void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    default void onStopTrackingTouch(SeekBar seekBar) {

    }
}
