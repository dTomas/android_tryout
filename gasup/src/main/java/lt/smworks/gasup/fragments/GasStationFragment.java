package lt.smworks.gasup.fragments;

import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import lt.smworks.gasup.MainActivity;
import lt.smworks.gasup.R;
import lt.smworks.gasup.adapters.AmenityAdapter;
import lt.smworks.gasup.adapters.GasStationFuelTypeAdapter;
import lt.smworks.gasup.databinding.GasStationFragmentBinding;
import lt.smworks.gasup.interfaces.RetrofitResult;
import lt.smworks.gasup.models.enums.Amenity;
import lt.smworks.gasup.models.json.GasStationJson;
import lt.smworks.gasup.models.json.LocationJson;
import lt.smworks.gasup.models.json.UserJson;
import lt.smworks.tools.fragments.BaseFragment;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.utils.IntentUtils;
import lt.smworks.tools.utils.L;

@Layout(R.layout.gas_station_fragment)
public class GasStationFragment extends GasupBaseFragment {

    public static final String TAG = "GasStationFragment";
    public static final String GAS_STATION_ID = "gas_station_id";
    private GasStationFragmentBinding binding;

    @Override
    protected void onCreateView(@NonNull Context context) {
        setHasOptionsMenu(true);
        setMenuVisibility(true);

        GasStationJson station = getBundle().getParcelable(GasStationJson.TAG);
        String stationId = getBundle().getString(GAS_STATION_ID);

        if (station == null && stationId == null) {
            showSnackbarFailure(getString(R.string.unable_to_open_gas_station));
            getMainActivity().popFragment();
            return;
        }

        binding = Objects.requireNonNull(DataBindingUtil.bind(getRootView()));

        if (stationId != null) {
            showProgressDialog(R.string.loading_gas_station);
            getMainActivity().getRestService().getGasStation(stationId).enqueue((RetrofitResult<GasStationJson>) (data, error) -> {
                hideProgressDialog();
                if (error != null) {
                    L.ex(error);
                    showSnackbarFailure(error.getMessage());
                    getMainActivity().popFragment();
                    return;
                }

                if (data.body() == null) {
                    showSnackbarFailure(getString(R.string.no_data_found));
                    getMainActivity().popFragment();
                }

                initBinding(context, data.body());
            });
        } else {
            initBinding(context, station);
        }
    }

    private void initBinding(@NonNull Context context, GasStationJson station) {
        binding.setView(this);
        binding.setModel(station);
        AmenityAdapter amenityAdapter = new AmenityAdapter(
                context, Amenity.fromIds(station.getAmenities()), false);
        binding.setAmenityAdapter(amenityAdapter);
        GasStationFuelTypeAdapter fuelTypeAdapter = new GasStationFuelTypeAdapter(context);
        fuelTypeAdapter.setItems(station.getFuelPrices());
        binding.setFuelTypeAdapter(fuelTypeAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.add(R.string.edit);
        item.setIcon(R.drawable.ic_edit_white);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        item.setOnMenuItemClickListener(this::edit);
    }

    private boolean edit(MenuItem menuItem) {
        BaseFragment<MainActivity> fragment = new EditGasStationFragment()
                .withParcelable(GasStationJson.TAG, binding.getModel());
        getMainActivity().addFragment(fragment);
        return false;
    }

    public void openInMaps() {
        LocationJson location = binding.getModel().getLocation();
        IntentUtils.openMaps(getContext(), location.getLat(), location.getLon());
    }

    public void openUserDescription(UserJson user) {
        getMainActivity().addFragment(
                new UserInfoFragment().withParcelable(UserInfoFragment.KEY_USER, user));
    }

    public void share() {
        String url = "https://smworks.herokuapp.com/web/gas_station/" + binding.getModel().getId();
        Intent intent = IntentUtils.getShareLinkIntent(url);
        startActivity(Intent.createChooser(intent, getString(R.string.share_with)));
    }
}
