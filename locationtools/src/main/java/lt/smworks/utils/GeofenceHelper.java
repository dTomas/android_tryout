package lt.smworks.utils;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Looper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import androidx.annotation.NonNull;
import lt.smworks.tools.utils.L;
import lt.smworks.tools.utils.PermissionUtils;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_DWELL;
import static com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_EXIT;
import static com.google.android.gms.location.Geofence.NEVER_EXPIRE;

public class GeofenceHelper {

    public static final String TAG = "GeofenceHelper";
    private final GeofencingClient geofencingClient;
    private final FusedLocationProviderClient locationClient;
    private final Context context;
    private final PendingIntent pendingIntent;
    private boolean isStarted;
    private boolean isStarting;
    private OnGeofenceLocationReceivedListener listener;

    public GeofenceHelper(@NonNull Context context, @NonNull PendingIntent pendingIntent) {
        this.context = context;
        this.pendingIntent = pendingIntent;
        geofencingClient = LocationServices.getGeofencingClient(context);
        locationClient = new FusedLocationProviderClient(context);
    }

    public static int getGeofenceTransition(Intent intent) {
       return GeofencingEvent.fromIntent(intent).getGeofenceTransition();
    }

    @SuppressLint("MissingPermission")
    public void add(@NonNull String geofenceId, float radiusInMeters) {
        if (!PermissionUtils.hasPermissions(context, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)) {
            L.w(TAG, "FINE and COARSE location permissions are not available");
            return;
        }

        L.d(TAG, "Starting geofence helper");
        isStarting = true;
        LocationRequest locationRequest = LocationHelper.getLocationRequest(0, 0, 1);
        locationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (!isStarting) {
                    return;
                }
                Location loc = locationResult.getLastLocation();
                L.d(TAG, "Received geofence location (%.6f, %.6f). Accuracy: %.2f. Provider: %s",
                        loc.getLatitude(), loc.getLongitude(), loc.getAccuracy(), loc.getProvider());
                if (listener != null) {
                    listener.onGeofenceLocationReceived(loc);
                }
                Geofence geofence = new Geofence.Builder()
                        .setRequestId(geofenceId)
                        .setCircularRegion(loc.getLatitude(), loc.getLongitude(), radiusInMeters)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_EXIT)
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .build();
                GeofencingRequest request = new GeofencingRequest.Builder()
                        .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                        .addGeofence(geofence)
                        .build();
                geofencingClient.addGeofences(request, pendingIntent)
                        .addOnSuccessListener(aVoid -> {
                            L.d(TAG, "Geofence %s successfully added with radius: %f",
                                    geofenceId, radiusInMeters);
                            if (!isStarting) {
                                L.d(TAG, "It appears geofence helper was stopped before " +
                                        "finishing initialization. Removing registered intent");
                                geofencingClient.removeGeofences(pendingIntent);
                                return;
                            }

                            isStarted = true;
                            isStarting = false;
                        }).addOnFailureListener(L::e);
            }
        }, Looper.myLooper());
    }

    public void stop() {
        if (!isStarted && !isStarting) {
            return;
        }

        L.d(TAG, "Stopping geofence helper");
        isStarting = false;
        if (isStarted) {
            geofencingClient.removeGeofences(pendingIntent);
        }
        isStarted = false;
    }

    @NonNull
    public static String transitionToString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return "Entered geofence zone";
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                return "Dwelling in geofence zone";
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return "Exited geofence zone";
            default:
                return "Unknown";
        }
    }

    public void setOnGeofenceLocationReceivedListener(
            @NonNull OnGeofenceLocationReceivedListener listener) {
        this.listener = listener;
    }

    public interface OnGeofenceLocationReceivedListener {
        void onGeofenceLocationReceived(@NonNull Location location);
    }
}
