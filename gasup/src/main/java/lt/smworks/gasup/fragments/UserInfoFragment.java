package lt.smworks.gasup.fragments;

import android.content.Context;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import lt.smworks.gasup.R;
import lt.smworks.gasup.databinding.UserInfoFragmentBinding;
import lt.smworks.gasup.models.json.UserJson;
import lt.smworks.tools.interfaces.Layout;

@Layout(R.layout.user_info_fragment)
public class UserInfoFragment extends GasupBaseFragment {

    private static final String TAG = "UserInfoFragment";
    public static final String KEY_USER = "key_user";

    @Override
    protected void onCreateView(@NonNull Context context) {
        UserInfoFragmentBinding binding = Objects.requireNonNull(DataBindingUtil.bind(getRootView()));
        UserJson user = getBundle().getParcelable(KEY_USER);
        binding.setModel(user);
        binding.setView(this);
    }

}
