package lt.smworks.tools.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import androidx.annotation.CallSuper;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import lt.smworks.tools.BaseActivity;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.utils.L;

public abstract class BaseFragment<T extends BaseActivity> extends Fragment {

    public static final String TAG = "BaseFragment";
    private ProgressDialog progressdialog;
    private View rootView;
    private Bundle bundle = new Bundle();
    private boolean valid;

    public BaseFragment<T> withParameter(@NonNull String key, @NonNull String value) {
        Bundle bundle = getArguments();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString(key, value);
        setArguments(bundle);
        return this;
    }

    public BaseFragment<T> withParameter(@NonNull String key, boolean value) {
        Bundle bundle = getArguments();
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean(key, value);
        setArguments(bundle);
        return this;
    }

    public abstract T getMainActivity();

    abstract protected void onCreateView(@NonNull Context context);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null || getContext() == null) {
            return null;
        }

        if (getArguments() != null) {
            bundle = getArguments();
        } else if (savedInstanceState != null) {
            bundle = savedInstanceState;
        }

        return inflateLayout(inflater, container);
    }

    @Nullable
    private View inflateLayout(LayoutInflater inflater, ViewGroup container) {
        if (getClass().isAnnotationPresent(Layout.class)) {
            Layout annotation = getClass().getAnnotation(Layout.class);
            rootView = inflater.inflate(annotation.value(), container, false);
            onCreateView(getContext());
            return rootView;
        } else {
            throw new RuntimeException("Fragment doesn't specify layout");
        }
    }

    @CallSuper
    @Override
    public void onResume() {
        super.onResume();
        valid = true;
    }

    @CallSuper
    @Override
    public void onPause() {
        super.onPause();
        valid = false;
        hideProgressDialog();
    }

    protected void showProgressDialog(@StringRes int message) {
        if (progressdialog != null && progressdialog.isShowing()) {
            progressdialog.dismiss();
        }
        progressdialog = new ProgressDialog(getContext());
        progressdialog.setMessage(getString(message));
        progressdialog.setCancelable(false);
        progressdialog.show();
    }

    protected void hideProgressDialog() {
        if (progressdialog != null && progressdialog.isShowing()) {
            progressdialog.dismiss();
        }
    }

    protected <T extends View> T findView(@IdRes int viewId) {
        if (rootView == null) {
            throw new NullPointerException("rootView is not available yet. " +
                    "You can only use findView after onCreateView method is called");
        }
        return rootView.findViewById(viewId);
    }

    protected View getRootView() {
        return rootView;
    }

    protected void showSnackbar(@NonNull String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    protected void showSnackbarSuccess(@NonNull String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).setActionTextColor(Color.GREEN).show();
    }

    protected void showSnackbarFailure(@NonNull String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).setActionTextColor(Color.RED).show();
    }

    protected void showError(Throwable e) {
        showError(e, "exception is null");
    }

    protected void showError(Throwable e, String messageIfErrorIsNull) {
        if (e != null) {
            L.ex(e);
            showSnackbar(e.getLocalizedMessage());
        } else {
            L.e(TAG, messageIfErrorIsNull);
            showSnackbar(messageIfErrorIsNull);
        }
    }

    public boolean isValid() {
        return valid;
    }

    @NonNull
    public Bundle getBundle() {
        return bundle;
    }

    @NonNull
    public Context getNonNullContext() {
        return Objects.requireNonNull(getContext());
    }

    public <F extends BaseFragment<T>> F withString(@NonNull String key, @NonNull String value) {
        Bundle arguments = getArguments();
        if (arguments == null) {
            arguments = new Bundle();
        }
        arguments.putString(key, value);
        setArguments(arguments);
        return (F) this;
    }

    public <F extends BaseFragment<T>> F withParcelable(@NonNull String key, @NonNull Parcelable value) {
        Bundle arguments = getArguments();
        if (arguments == null) {
            arguments = new Bundle();
        }
        arguments.putParcelable(key, value);
        setArguments(arguments);
        return (F) this;
    }
}
