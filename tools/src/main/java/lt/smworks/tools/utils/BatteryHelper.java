package lt.smworks.tools.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

public class BatteryHelper {

    public static final String TAG = "BatteryHelper";
    private final Context context;
    private BatteryBroadcastReceiver receiver;
    private OnBatteryStatusChangeListener onBatteryStatusChangeListener;
    private boolean isStarted;
    private int lastBatteryLevel = -1;

    public BatteryHelper(Context context) {
        this.context = context;
        receiver = new BatteryBroadcastReceiver();
    }

    public void setBatteryStatusChangeListener(OnBatteryStatusChangeListener listener) {
        this.onBatteryStatusChangeListener = listener;
    }

    public void start() {
        if (isStarted) {
            return;
        }

        L.d(TAG, "Starting battery helper");
        context.registerReceiver(receiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        isStarted = true;
    }

    public void stop() {
        if (!isStarted) {
            return;
        }

        L.d(TAG, "Stopping battery helper");
        context.unregisterReceiver(receiver);
        isStarted = false;
    }

    public interface OnBatteryStatusChangeListener {
        void onChange(int level);
    }

    private class BatteryBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }

            if (onBatteryStatusChangeListener != null) {
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                if (level != lastBatteryLevel) {
                    onBatteryStatusChangeListener.onChange(level);
                    lastBatteryLevel = level;
                }
            }
        }
    }
}
