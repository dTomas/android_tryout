package lt.smworks.gasup.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import lt.smworks.tools.utils.L;

public final class DateUtils {

    private DateUtils() {
    }

    public static String toDate(long timestamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            Date netDate = (new Date(timestamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            L.ex(ex);
        }
        return "";
    }
}
