package lt.smworks.tools.utils;

import android.content.Context;
import android.os.PowerManager;

import androidx.annotation.NonNull;

import static android.content.Context.POWER_SERVICE;

public class WakeLockHelper {

    private static final String TAG = "WakeLockHelper";
    private static final long TIMEOUT = 24 * 60 * 60 * 1000;
    private PowerManager.WakeLock wakeLock;

    public WakeLockHelper(@NonNull Context context, @NonNull String wakeLockTag) {
        PowerManager powerManager = (PowerManager) context.getSystemService(POWER_SERVICE);
        if (powerManager != null) {
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, wakeLockTag);
        } else {
            L.e(TAG, "Unable to get power manager required for wake lock. Ignoring wake lock");
        }
    }

    public void acquire() {
        if (wakeLock == null) {
            L.e(TAG, "Wake lock is unavailable");
        }

        if (!wakeLock.isHeld()) {
            wakeLock.acquire(TIMEOUT);
            L.d(TAG,"Wake lock acquired");
        }
    }

    public void release() {
        if (wakeLock.isHeld()) {
            wakeLock.release();
            L.d(TAG,"Wake lock released. Device might go to sleep");
        }
    }
}
