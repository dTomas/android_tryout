package lt.smworks.gasup.models.json;

import android.os.Parcel;
import android.os.Parcelable;

public class FuelPriceJson implements Parcelable {

    private int fuelTypeId;
    private double price;
    private String modification;

    public FuelPriceJson() {
        fuelTypeId = 0;
        price = 0.0;
        modification = "";
    }

    protected FuelPriceJson(Parcel in) {
        fuelTypeId = in.readInt();
        price = in.readDouble();
        modification = in.readString();
    }

    public static final Creator<FuelPriceJson> CREATOR = new Creator<FuelPriceJson>() {
        @Override
        public FuelPriceJson createFromParcel(Parcel in) {
            return new FuelPriceJson(in);
        }

        @Override
        public FuelPriceJson[] newArray(int size) {
            return new FuelPriceJson[size];
        }
    };

    public int getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(int fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getModification() {
        return modification;
    }

    public void setModification(String modification) {
        this.modification = modification;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(fuelTypeId);
        dest.writeDouble(price);
        dest.writeString(modification);
    }
}
