//package lt.smworks.gasup.fragments;
//
//import android.Manifest;
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.pm.PackageManager;
//import android.util.Log;
//
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GoogleApiAvailability;
//import com.google.android.gms.vision.text.TextBlock;
//
//import java.io.IOException;
//
//import androidx.annotation.NonNull;
//import lt.smworks.gasup.R;
//import lt.smworks.gasup.ui.camera.CameraSource;
//import lt.smworks.gasup.ui.camera.CameraSourcePreview;
//import lt.smworks.gasup.utils.CameraUtils;
//import lt.smworks.tools.interfaces.Layout;
//
//@Layout(R.layout.ocr_fragment)
//public class OcrFragment extends GasupBaseFragment {
//
//    // Intent request code to handle updating play services if needed.
//    private static final int RC_HANDLE_GMS = 9001;
//
//    // Permission request codes need to be < 256
//    private static final int RC_HANDLE_CAMERA_PERM = 2;
//
//    // Constants used to pass extra data in the intent
//    private static final String KEY_AUTOFOCUS = "KEY_AUTOFOCUS";
//    private static final String KEY_USE_FLASH = "KEY_USE_FLASH";
//    public static final String TextBlockObject = "String";
//    public static final String TAG = "OcrFragment";
//
//    private CameraSource cameraSource;
//    private CameraSourcePreview preview;
//
//    // Helper objects for detecting taps and pinches.
//
//    @Override
//    protected void onCreateView(@NonNull Context context) {
//
//        rootView.setOnTouchListener((v, event) -> onTap(event.getRawX(), event.getRawY()));
//
//        preview = findView(R.id.preview);
//
//        boolean autoFocus = getBundle().getBoolean(KEY_AUTOFOCUS);
//        boolean useFlash = getBundle().getBoolean(KEY_USE_FLASH);
//
//        getMainActivity().askForPermission(
//                getString(R.string.permission_camera_rationale),
//                (granted) -> cameraSource = CameraUtils.getCameraSource(getMainActivity(), useFlash),
//                Manifest.permission.CAMERA);
//    }
//
//    /**
//     * Restarts the camera.
//     */
//    @Override
//    public void onResume() {
//        super.onResume();
//        showSnackbar("Tap to capture. Pinch/Stretch to zoom");
//        startCameraSource();
//    }
//
//    /**
//     * Stops the camera.
//     */
//    @Override
//    public void onPause() {
//        super.onPause();
//        if (preview != null) {
//            preview.stop();
//        }
//    }
//
//    /**
//     * Releases the resources associated with the camera source, the associated detectors, and the
//     * rest of the processing pipeline.
//     */
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (preview != null) {
//            preview.release();
//        }
//    }
//
//    /**
//     * Callback for the result from requesting permissions. This method
//     * is invoked for every call on {@link #requestPermissions(String[], int)}.
//     * <p>
//     * <strong>Note:</strong> It is possible that the permissions request interaction
//     * with the user is interrupted. In this case you will receive empty permissions
//     * and results arrays which should be treated as a cancellation.
//     * </p>
//     *
//     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
//     * @param permissions  The requested permissions. Never null.
//     * @param grantResults The grant results for the corresponding permissions
//     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
//     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
//     * @see #requestPermissions(String[], int)
//     */
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        if (requestCode != RC_HANDLE_CAMERA_PERM) {
//            Log.d(TAG, "Got unexpected permission result: " + requestCode);
//            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//            return;
//        }
//
//        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Log.d(TAG, "Camera permission granted - initialize the camera source");
//            // We have permission, so create the camerasource
//            boolean autoFocus = getBundle().getBoolean(KEY_AUTOFOCUS);
//            boolean useFlash = getBundle().getBoolean(KEY_USE_FLASH);
//            CameraUtils.getCameraSource(getMainActivity(), useFlash);
//            return;
//        }
//
//        Log.ex(TAG, "Permission not granted: results len = " + grantResults.length +
//                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));
//
//        DialogInterface.OnClickListener listener = (dialog, id) -> getMainActivity().finish();
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setTitle("Multitracker sample")
//                .setMessage(R.string.no_camera_permission)
//                .setPositiveButton(R.string.ok, listener)
//                .show();
//    }
//
//    /**
//     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
//     * (ex.g., because onResume was called before the camera source was created), this will be called
//     * again when the camera source is created.
//     */
//    private void startCameraSource() throws SecurityException {
//        // Check that the device has play services available.
//        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
//                getContext());
//        if (code != ConnectionResult.SUCCESS) {
//            Dialog dlg =
//                    GoogleApiAvailability.getInstance().getErrorDialog(getMainActivity(), code, RC_HANDLE_GMS);
//            dlg.show();
//        }
//
//        if (cameraSource != null) {
//            try {
//                preview.start(cameraSource);
//            } catch (IOException ex) {
//                Log.ex(TAG, "Unable to start camera source.", ex);
//                cameraSource.release();
//                cameraSource = null;
//            }
//        }
//    }
//
//    /**
//     * onTap is called to capture the first TextBlock under the tap location and return it to
//     * the Initializing Activity.
//     *
//     * @param rawX - the raw position of the tap
//     * @param rawY - the raw position of the tap.
//     * @return true if the activity is ending.
//     */
//    private boolean onTap(float rawX, float rawY) {
////        OcrGraphic graphic = graphicOverlay.getGraphicAtLocation(rawX, rawY);
//        TextBlock text = null;
////        if (graphic != null) {
////            text = graphic.getTextBlock();
////            if (text != null && text.getValue() != null) {
////                Intent data = new Intent();
////                data.putExtra(TextBlockObject, text.getValue());
////            } else {
////                Log.d(TAG, "text data is null");
////            }
////        } else {
////            Log.d(TAG, "no text detected");
////        }
////        for (OcrGraphic ocrGraphic : graphicOverlay.getTextObjects()) {
////            L.d(TAG, ocrGraphic.getTextBlock().getValue());
////        }
//        return text != null;
//    }
//}
