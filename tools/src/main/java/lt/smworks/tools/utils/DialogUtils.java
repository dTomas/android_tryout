/**
 * @author Martynas Šustavičius
 * @since 2012 03 06
 */
package lt.smworks.tools.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


public class DialogUtils {
	
	/**
	 * Custom runnable used in dialogs with yes or no buttons.
	 */
	public interface DialogRunnable {
		/**
		 * @param yes - if true, then yes button was pressed, if false - no button.
		 */
		void run(boolean yes);
	}
	
	/**
	 * Shows a dialog with a message and an option to choose yes or no.
	 * @param context - activity or service context.
	 * @param message - dialog message.
	 * @param yes - text on yes button.
	 * @param no - text on no button.
	 * @param runnable - runnable that will be executed on button press.
	 * @return Dialog object.
	 */
	public static Dialog yesNo(Context context, String message, String yes, String no, final DialogRunnable runnable) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton(yes, (dialog, which) -> {
            dialog.dismiss();
            runnable.run(true);
        });
		builder.setNegativeButton(no, (dialog, which) -> {
            dialog.dismiss();
            runnable.run(false);
        });
		return builder.create();
	}
	
	/**
	 * Shows a dialog with a custom view and an option to choose yes or no.
	 * @param context - activity or service context.
	 * @param view - custom dialog view.
	 * @param yes - text on yes button.
	 * @param no - text on no button.
	 * @param runnable - runnable that will be executed on button press.
	 * @return Dialog object.
	 */
	public static Dialog yesNo(Context context, View view, String yes, String no, final DialogRunnable runnable) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setView(view);
		builder.setCancelable(false);
		builder.setPositiveButton(yes, (dialog, which) -> {
            dialog.dismiss();
            runnable.run(true);
        });
		builder.setNegativeButton(no, (dialog, which) -> {
            dialog.dismiss();
            runnable.run(false);
        });
		return builder.create();
	}
	
	/**
	 * Shows a dialog with a custom view and ok button.
	 * @param context - activity or service context.
	 * @param view - custom dialog view.
	 * @param ok - text on ok button.
	 * @param runnable - runnable that will be executed on button press.
	 * @return Dialog object.
	 */
	public static Dialog ok(Context context, View view, String ok, final Runnable runnable) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setView(view);
		builder.setCancelable(false);
		builder.setPositiveButton(ok, (dialog, which) -> {
            dialog.dismiss();
            runnable.run();
        });
		return builder.create();
	}
	
	/**
	 * Shows a dialog with a custom view and custom OK button.
	 * @param context - activity or service context.
	 * @param view - custom dialog view.
	 * @param okResId - OK button id in specified view.
	 * @param cancelable - set to true if dialog can be cancelled.
	 * @param runnable - runnable that will be executed when OK button is pressed.
	 * @return Dialog object.
	 */
	public static Dialog ok(Context context, View view, int okResId, boolean cancelable, final Runnable runnable) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(true);
		final AlertDialog dialog = builder.create();
		dialog.setView(view, 0, 0, 0, 0);
		View button = view.findViewById(okResId);
		if (button != null) {
			button.setOnClickListener(v -> {
                dialog.dismiss();
                runnable.run();
            });
		}
		return dialog;
	}
	
	/**
	 * @param context - activity or service context.
	 * @param view - custom dialog view.
	 * @param force - force hides action bar on tablets.
	 * @return Dialog object.
	 */
	public static Dialog fullscreen(Context context, final View view, final boolean force) {
		if (force) {
			view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		} else {
			//view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
		}
		view.setOnSystemUiVisibilityChangeListener(visibility -> {
            if (visibility == 0) { // Navigation bar on tablets appear.
                view.postDelayed(() -> {
                    if (force) {
                        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                    } else {
                        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
                    }
                }, 2000);
            }
        });
		Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		dialog.setCancelable(true);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		dialog.setContentView(view);
		return dialog;
	}
	
	/**
	 * Shows progress dialog with specified message resource id.
	 * @param context - activity or service context.
	 * @param resId - message resource id.
	 * @return Handle to dialog.
	 */
	public static Dialog showProgress(Context context, int resId) {
		return showProgress(context, context.getString(resId));
	}
	
	/**
	 * Shows progress dialog with specified message.
	 * @param context - activity or service context.
	 * @param message - message to be shown.
	 * @return Handle to dialog.
	 */
	public static Dialog showProgress(Context context, String message) {
		ProgressDialog dialog = ProgressDialog.show(context, "", message, true);
		dialog.setCancelable(false);
		dialog.show();
		return dialog;
	}
	
	/**
	 * Shows simple long toast with specified message resource id.
	 * @param context - activity or service context.
	 * @param resId - message resource id.
	 */
	public static void showToast(Context context, int resId) {
		showToast(context, context.getString(resId));
	}
	
	/**
	 * Shows simple long toast with specified message.
	 * @param context - activity or service context.
	 * @param message - message to be shown.
	 */
	public static void showToast(Context context, String message) {
		if (context == null) {
			return;
		}
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	
	/**
	 * Shows simple short toast with specified message resource id.
	 * @param context - activity or service context.
	 * @param resId - message resource id.
	 */
	public static void showToastShort(Context context, int resId) {
		showToastShort(context, context.getString(resId));
	}
	
	/**
	 * Shows simple short toast with specified message.
	 * @param context - activity or service context.
	 * @param message - message to be shown.
	 */
	public static void showToastShort(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
}
