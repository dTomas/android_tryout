package lt.smworks.tools;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import lt.smworks.tools.fragments.BaseFragment;
import lt.smworks.tools.interfaces.ContentFragmentId;
import lt.smworks.tools.utils.CacheHelper;
import lt.smworks.tools.utils.L;

public class BaseActivity extends AppCompatActivity {

    public static final int BACK_PRESS_DELAY = 2000;
    public static final String TAG = "BaseActivity";
    boolean doubleBackToExitPressedOnce;
    private CacheHelper cacheHelper = new CacheHelper();

    public boolean doubleClickBackToExit(@NonNull View layout) {
        if (getFragmentCount() > 1) {
            return false;
        }

        if (doubleBackToExitPressedOnce) {
            finish();
            return false;
        }

        doubleBackToExitPressedOnce = true;
        Snackbar.make(layout, R.string.press_back_to_exit, Snackbar.LENGTH_LONG).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, BACK_PRESS_DELAY);
        return true;
    }

    @Nullable
    public Fragment getLastFragmentInStack() {
        FragmentManager manager = getSupportFragmentManager();
        int index = manager.getBackStackEntryCount() - 1;
        if (index < 0) {
            return null;
        }
        FragmentManager.BackStackEntry backEntry = manager.getBackStackEntryAt(index);
        String tag = backEntry.getName();
        return manager.findFragmentByTag(tag);
    }

    public void addFragment(@NonNull Fragment fragment) {
        L.d(TAG, "Adding fragment: %s", fragment.getClass().getSimpleName());
        Fragment lastFragmentInStack = getLastFragmentInStack();
        if (lastFragmentInStack != null && lastFragmentInStack.getClass() == fragment.getClass()) {
            L.d(TAG, "Fragment already added. Ignoring operation");
            return;
        }
        int contentFragmentId = getContentFragmentId();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(contentFragmentId, fragment, fragment.getClass().getName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    public void refreshFragment(@NonNull BaseFragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.detach(fragment);
        ft.attach(fragment);
        ft.commit();
    }

    public void clearFragments() {
        getSupportFragmentManager()
                .popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void popFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
    }

    public int getFragmentCount() {
        return getSupportFragmentManager().getBackStackEntryCount();
    }

    @IdRes
    private int getContentFragmentId() {
        if (getClass().isAnnotationPresent(ContentFragmentId.class)) {
            ContentFragmentId id = getClass().getAnnotation(ContentFragmentId.class);
            return id.value();
        } else {
            throw new RuntimeException("Activity doesn't specify content fragment id");
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm == null) {
            L.e(TAG, "InputMethodManager unavailable!");
            return;
        }
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public CacheHelper getCacheHelper() {
        return cacheHelper;
    }
}
