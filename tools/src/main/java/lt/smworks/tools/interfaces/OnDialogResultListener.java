package lt.smworks.tools.interfaces;

import android.os.Bundle;

import androidx.annotation.NonNull;

public interface OnDialogResultListener {

    void onDialogResult(@NonNull Bundle bundle);
}
