package lt.smworks.gasup.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import java.util.Arrays;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import lt.smworks.gasup.R;
import lt.smworks.gasup.adapters.FuelTypeEditAdapter;
import lt.smworks.gasup.models.json.FuelPriceJson;
import lt.smworks.tools.dialogs.BaseDialogFragment;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.interfaces.OnDialogResultListener;
import lt.smworks.tools.utils.ListUtils;

@Layout(R.layout.update_fuel_price_dialog_fragment)
public class UpdateFuelPriceDialogFragment extends BaseDialogFragment<UpdateFuelPriceDialogFragment> {

    public static final String KEY_FUEL_PRICE_ARRAY = "fuelPriceArray";

    public UpdateFuelPriceDialogFragment withArray(FuelPriceJson[] fuelPrices) {
        Bundle arguments = Objects.requireNonNull(getArguments());
        arguments.putParcelableArray(KEY_FUEL_PRICE_ARRAY, fuelPrices);
        return this;
    }

    @Override
    public Dialog onCreateDialog(@NonNull View view) {
        RecyclerView list = view.findViewById(R.id.updateFuelPriceDialogFragment_list);
        FuelTypeEditAdapter fuelTypeEditAdapter = new FuelTypeEditAdapter(view.getContext());
        ListUtils.setupFixedList(list, fuelTypeEditAdapter);
        Bundle arguments = Objects.requireNonNull(getArguments());
        FuelPriceJson[] fuelPrices = (FuelPriceJson[]) arguments.getParcelableArray(KEY_FUEL_PRICE_ARRAY);
        fuelTypeEditAdapter.setItems(Arrays.asList(fuelPrices));

        AlertDialog d = new AlertDialog.Builder(view.getContext())
                .setTitle(R.string.available_fuel_types)
                .setView(view)
                .setPositiveButton(R.string.update, (dialog, which) -> {
                    Fragment targetFragment = getTargetFragment();
                    if (targetFragment instanceof OnDialogResultListener) {
                        Bundle bundle = new Bundle();
                        bundle.putString(KEY_ACTION, VAL_POSITIVE);
                        FuelPriceJson[] items = fuelTypeEditAdapter.getItems().toArray(new FuelPriceJson[0]);
                        bundle.putParcelableArray(KEY_FUEL_PRICE_ARRAY, items);
                        ((OnDialogResultListener) targetFragment).onDialogResult(bundle);
                    }
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
                .show();

        if (d.getWindow() != null) {
            d.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        }
        return d;
    }
}
