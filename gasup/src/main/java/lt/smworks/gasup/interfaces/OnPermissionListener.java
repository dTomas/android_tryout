package lt.smworks.gasup.interfaces;

public interface OnPermissionListener {

    void onPermission(boolean granted);
}
