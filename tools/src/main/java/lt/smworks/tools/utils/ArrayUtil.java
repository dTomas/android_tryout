package lt.smworks.tools.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.List;



/**
 * Utility class for array manipulation.
 * @author MS
 * @since 2011 09 26
 */
public class ArrayUtil {

	public static final String TAG = "ArrayUtils";

	/**
	 * Creates float buffer from float array.
	 * @param array - array of floats.
	 * @return Array as float buffer.
	 */
	public static final FloatBuffer arrayToBuffer(float[] array) {
		ByteBuffer bb = ByteBuffer.allocateDirect(array.length * 4);
		bb.order(ByteOrder.nativeOrder());
		FloatBuffer buffer = bb.asFloatBuffer();
		buffer.put(array);
		buffer.position(0);
		return buffer;
	}
	
	/**
	 * Creates float buffer from float list.
	 * @param list - list of floats.
	 * @return List converted to float buffer.
	 */
	public static final FloatBuffer listToBuffer(List<Float> list, float f) {
		float[] array = new float[list.size()];
		int c = 0;
		for (float i : list) {
			array[c++] = i;
		}
		return arrayToBuffer(array);
	}
	
	/**
	 * Creates short buffer from short array.
	 * @param array - array of indices.
	 * @return Array as short buffer.
	 */
	public static final ShortBuffer arrayToBuffer(short[] array)
	{
		ByteBuffer bb = ByteBuffer.allocateDirect(array.length * 2);
		bb.order(ByteOrder.nativeOrder());
		ShortBuffer buffer = bb.asShortBuffer();
		buffer.put(array);
		buffer.position(0);
		return buffer;
	}
	
	/**
	 * Creates short buffer from short list.
	 * @param list - list of shorts.
	 * @return List converted to short buffer.
	 */
	public static final ShortBuffer listToBuffer(List<Short> list, short s) {
		short[] array = new short[list.size()];
		int c = 0;
		for (short i : list) {
			array[c++] = i;
		}
		return arrayToBuffer(array);
	}
	
	/**
	 * Prints integer array in one row.
	 * @param array - array of integers.
	 */
	public static final void printArray(int[] array) {
		StringBuilder builder = new StringBuilder();
		for (int i : array) {
			builder.append(i);
			builder.append(" ");
		}
		L.d(TAG, builder.toString());
	}
	
	/**
	 * Prints float array in one row.
	 * @param array - array of floats.
	 */
	public static final void printArray(float[] array) {
		StringBuilder builder = new StringBuilder();
		for (float i : array) {
			builder.append(i);
			builder.append(" ");
		}
		L.d(TAG, builder.toString());
	}
}
