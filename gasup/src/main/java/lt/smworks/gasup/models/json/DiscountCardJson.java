package lt.smworks.gasup.models.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

import org.json.JSONException;
import org.json.JSONObject;

import lt.smworks.tools.utils.L;


public class DiscountCardJson implements Parcelable {

    public static final transient String TAG = "DiscountCardJson";
    public static final String KEY_ID = "id";
    public static final String KEY_GAS_STATION_ID = "gasStationId";
    public static final String KEY_NAME = "name";
    public static final String KEY_IMAGE_URL = "imageUrl";
    public static final String KEY_DISCOUNT = "discount";

    @Json(name = "_id")
    private String id;
    private String gasStationId;
    private String name;
    private String imageUrl;
    private double discount;
    private long createdAt;
    private long updatedAt;

    public DiscountCardJson(JSONObject json) {
        try {
            id = json.getString(KEY_ID);
            gasStationId = json.getString(KEY_GAS_STATION_ID);
            name = json.getString(KEY_NAME);
            imageUrl = json.getString(KEY_IMAGE_URL);
            discount = json.getDouble(KEY_DISCOUNT);
        } catch (JSONException e) {
            L.ex(e);
        }
    }

    protected DiscountCardJson(Parcel in) {
        id = in.readString();
        gasStationId = in.readString();
        name = in.readString();
        imageUrl = in.readString();
        createdAt = in.readLong();
        updatedAt = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(gasStationId);
        dest.writeString(name);
        dest.writeString(imageUrl);
        dest.writeLong(createdAt);
        dest.writeLong(updatedAt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiscountCardJson> CREATOR = new Creator<DiscountCardJson>() {
        @Override
        public DiscountCardJson createFromParcel(Parcel in) {
            return new DiscountCardJson(in);
        }

        @Override
        public DiscountCardJson[] newArray(int size) {
            return new DiscountCardJson[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGasStationId() {
        return gasStationId;
    }

    public void setGasStationId(String gasStationId) {
        this.gasStationId = gasStationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

}
