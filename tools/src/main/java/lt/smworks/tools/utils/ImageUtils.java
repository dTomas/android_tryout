/**
 * @author Martynas Šustavičius
 * @since 2013 12 18
 */
package lt.smworks.tools.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

public final class ImageUtils {

    public static final String TAG = "ImageUtils";

    /**
     * Converts byte array to Bitmap.
     *
     * @param bytes - image bytes.
     * @return Bitmap or null if unable to convert.
     */
    public static Bitmap convert(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    /**
     * Converts byte array to Bitmap.
     *
     * @param bytes  - image bytes.
     * @param config - bitmap configuration.
     * @return Bitmap or null if unable to convert.
     */
    public static Bitmap convert(byte[] bytes, Config config) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inDither = true;
        opt.inPreferredConfig = config;
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, opt);
    }

    /**
     * Changes specified image configuration.
     *
     * @param bitmap - source image.
     * @param config - required configuration.
     * @return Same image with different configuration.
     */
    public static Bitmap convert(Bitmap bitmap, Config config) {
        Bitmap convertedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), config);
        Canvas canvas = new Canvas(convertedBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return convertedBitmap;
    }

    /**
     * Converts encoded JPEG byte array with YUV data to integer array containing raw ARGB8888.
     *
     * @param fg
     * @param width
     * @param height
     * @return
     * @throws NullPointerException
     * @throws IllegalArgumentException
     */
    public static int[] convertJPEGToARGB888(byte[] fg, int width, int height)
            throws NullPointerException, IllegalArgumentException {
        int sz = width * height;
        int out[] = new int[sz];
        if (fg == null)
            throw new NullPointerException("buffer 'fg' is null");
        if (fg.length < sz)
            throw new IllegalArgumentException("buffer fg size " + fg.length
                    + " < minimum " + sz * 3 / 2);
        int i, j;
        int Y, Cr = 0, Cb = 0;
        for (j = 0; j < height; j++) {
            int pixPtr = j * width;
            final int jDiv2 = j >> 1;
            for (i = 0; i < width; i++) {
                Y = fg[pixPtr];
                if (Y < 0)
                    Y += 255;
                if ((i & 0x1) != 1) {
                    final int cOff = sz + jDiv2 * width + (i >> 1) * 2;
                    Cb = fg[cOff];
                    if (Cb < 0)
                        Cb += 127;
                    else
                        Cb -= 128;
                    Cr = fg[cOff + 1];
                    if (Cr < 0)
                        Cr += 127;
                    else
                        Cr -= 128;
                }
                int R = Y + Cr + (Cr >> 2) + (Cr >> 3) + (Cr >> 5);
                if (R < 0)
                    R = 0;
                else if (R > 255)
                    R = 255;
                int G = Y - (Cb >> 2) + (Cb >> 4) + (Cb >> 5) - (Cr >> 1)
                        + (Cr >> 3) + (Cr >> 4) + (Cr >> 5);
                if (G < 0)
                    G = 0;
                else if (G > 255)
                    G = 255;
                int B = Y + Cb + (Cb >> 1) + (Cb >> 2) + (Cb >> 6);
                if (B < 0)
                    B = 0;
                else if (B > 255)
                    B = 255;
                out[pixPtr++] = 0xff000000 + (B << 16) + (G << 8) + R;
            }
        }
        return out;
    }

    /**
     * Applies circle mask on the image.
     * This method uses ARGB_8888 format for generated image.
     *
     * @param bitmap - source image.
     * @return Image containing circle that is filled with specified image.
     */
    public static Bitmap applyCircleMask(Bitmap bitmap) {
        return applyCircleMask(bitmap, 0, 0);
    }

    /**
     * Applies circle mask on the image and adds border of specified width and color.
     * This method uses ARGB_8888 format for generated image.
     *
     * @param bitmap      - source image.
     * @param borderWidth - width of border in pixels.
     * @param borderColor - color of the border.
     * @return Image containing circle that is filled with specified image.
     */
    public static Bitmap applyCircleMask(Bitmap bitmap, int borderWidth, @ColorInt int borderColor) {
        int edge = bitmap.getWidth() < bitmap.getHeight() ? bitmap.getWidth() : bitmap.getHeight();
        float radius = edge * 0.5f;
        Bitmap output = Bitmap.createBitmap(edge, edge, Config.ARGB_8888);
        Paint paint = new Paint();
        int offsetX = edge < bitmap.getWidth() ? (bitmap.getWidth() - edge) / 2 : 0;
        int offsetY = edge < bitmap.getHeight() ? (bitmap.getHeight() - edge) / 2 : 0;
        Rect rectSrc = new Rect(offsetX, offsetY, bitmap.getWidth() + offsetX, bitmap.getHeight() + offsetY);
        Rect rectDst = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        Canvas canvas = new Canvas(output);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(radius, radius, radius - 1.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rectSrc, rectDst, paint);
        if (borderWidth > 0) {
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(borderWidth);
            paint.setXfermode(null);
            paint.setColor(borderColor);
            canvas.drawCircle(radius, radius, radius - borderWidth * 0.5f, paint);
        }
        bitmap.recycle();
        return output;
    }

    /**
     * Resizes specified image.
     * Image configuration is copied over to new one.
     *
     * @param bitmap - source image.
     * @param width  - required width.
     * @param height - required height.
     * @return Resized image.
     */
    public static Bitmap resize(Bitmap bitmap, int width, int height) {
        if (width < 1 || height < 1) {
            return bitmap;
        }
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    /**
     * Resizes specified image.
     * New image will have specified configuration.
     *
     * @param bitmap - source image.
     * @param width  - required width.
     * @param height - required height.
     * @param config - image configuration.
     * @return Resized image.
     */
    public static Bitmap resize(Bitmap bitmap, int width, int height, Config config) {
        Bitmap output = Bitmap.createBitmap(width, height, config);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawBitmap(bitmap, null, new Rect(0, 0, width, height), paint);
        return output;
    }

    /**
     * Resizes specified image to specified width and height.
     * If image and region dimensions are different, a part of image center is cut and resized to
     * match region.
     * ARGB_8888 image format is used.
     *
     * @param bitmap - source image.
     * @param width  - region width.
     * @param height - region height.
     * @return Resized image.
     */
    public static Bitmap resizeWithAspectRatio(Bitmap bitmap, int width, int height) {
        return resizeWithAspectRatio(bitmap, width, height, Config.ARGB_8888);
    }

    /**
     * Resizes specified image to specified width and height.
     * If image and region dimensions are different, a part of image center is cut and resized to
     * match region.
     *
     * @param bitmap - source image.
     * @param width  - region width.
     * @param height - region height.
     * @param config - bitmap configuration. RGB_565 is preferred if transparency is not required.
     * @return Resized image.
     */
    public static Bitmap resizeWithAspectRatio(Bitmap bitmap, int width, int height, Config config) {
        float regionHeightToWidthRatio = (float) height / (float) width;
        float bitmapHeightToWidthRatio = (float) bitmap.getHeight() / (float) bitmap.getWidth();
        Bitmap output = Bitmap.createBitmap(width, height, config);
        Canvas canvas = new Canvas(output);
        canvas.drawARGB(255, 255, 255, 255);
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        Rect rectSrc = null;
        if (Math.abs(regionHeightToWidthRatio - bitmapHeightToWidthRatio) > 0.001f) { // Aspect ratios do not match.
            if (regionHeightToWidthRatio < bitmapHeightToWidthRatio) { // Image is narrower in width.
                int srcHeight = (int) (regionHeightToWidthRatio * bitmap.getWidth());
                int offsetY = (bitmap.getHeight() - srcHeight) / 2;
                rectSrc = new Rect(0, offsetY, bitmap.getWidth(), srcHeight + offsetY);
            } else { // Image is wider.
                int srcWidth = (int) (bitmap.getHeight() / regionHeightToWidthRatio);
                int offsetX = (bitmap.getWidth() - srcWidth) / 2;
                rectSrc = new Rect(offsetX, 0, srcWidth + offsetX, bitmap.getHeight());
            }
        } else {
            rectSrc = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        }
        Rect rectDst = new Rect(0, 0, width, height);
        canvas.drawBitmap(bitmap, rectSrc, rectDst, paint);
        return output;
    }

    /**
     * Rotate image by specified angle in degrees.
     *
     * @param bitmap - source image.
     * @param angle  - rotation angle.
     * @return Rotated bitmap.
     */
    public static Bitmap rotate(Bitmap bitmap, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    /**
     * Loads image from asset folder.
     *
     * @param assetManager - handle to asset manager.
     * @param filename     - image name with extension.
     * @return Image loaded into bitmap, or null.
     */
    public static final Bitmap loadFromAssets(AssetManager assetManager, String filename) {
        Bitmap image = null;
        try {
            image = BitmapFactory.decodeStream(assetManager.open(filename));
        } catch (IOException e) {
            L.w(TAG, "Unable to load image from assets. Error: %s.", e.toString());
        }
        return image;
    }

    /**
     * Loads image from resources drawable folder.
     *
     * @param context - handle to activity context.
     * @param id      - image identifier.
     * @return Image loaded into bitmap, or null.
     */
    public static final Bitmap loadFromResources(Context context, int id) {
        Bitmap image = null;
        BitmapDrawable drawable = (BitmapDrawable) context.getResources().getDrawable(id);
        image = drawable.getBitmap();
        return image;
    }

    /**
     * Saves bitmap as JPG image on memory card.
     * Image will not be saved if there is no memory card.
     * NOTE: you must specify android.permission.WRITE_EXTERNAL_STORAGE permission
     * in your android manifest file.
     *
     * @param bitmap   - image that will be saved.
     * @param location - location where to save the image.
     *                 Format must be: /path/to/file/
     *                 If no location is specified, the image will be saved in root memory card folder.
     * @param name     - name of the image file. If null is specified, random name will be chosen.
     */
    public static boolean saveImageToMemoryCard(Bitmap bitmap, String location, String name) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return false;
        }
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + (location == null ? "" : location));
        myDir.mkdirs();
        if (name == null) {
            Random generator = new Random();
            int n = generator.nextInt(10000);
            name = "Image-" + n + ".jpg";
        } else {
            name = name + ".jpg";
        }
        File file = new File(myDir, name);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void printImageMetadata(@NonNull String filename) {
        try {
            ExifInterface exifInterface = new ExifInterface(filename);
            String m="Exif: " + filename;
            m += "\n IMAGE_LENGTH: " + exifInterface.getAttribute(ExifInterface.TAG_IMAGE_LENGTH);
            m += "\n IMAGE_WIDTH: " + exifInterface.getAttribute(ExifInterface.TAG_IMAGE_WIDTH);
            m += "\n DATETIME: " + exifInterface.getAttribute(ExifInterface.TAG_DATETIME);
            m += "\n TAG_MAKE: " + exifInterface.getAttribute(ExifInterface.TAG_MAKE);
            m += "\n TAG_MODEL: " + exifInterface.getAttribute(ExifInterface.TAG_MODEL);
            m += "\n TAG_ORIENTATION: " + exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION);
            m += "\n TAG_WHITE_BALANCE: " + exifInterface.getAttribute(ExifInterface.TAG_WHITE_BALANCE);
            m += "\n TAG_FOCAL_LENGTH: " + exifInterface.getAttribute(ExifInterface.TAG_FOCAL_LENGTH);
            m += "\n TAG_FLASH: " + exifInterface.getAttribute(ExifInterface.TAG_FLASH);
            m += "\nGPS related:";
            m += "\n TAG_GPS_DATESTAMP: " + exifInterface.getAttribute(ExifInterface.TAG_GPS_DATESTAMP);
            m += "\n TAG_GPS_TIMESTAMP: " + exifInterface.getAttribute(ExifInterface.TAG_GPS_TIMESTAMP);
            m += "\n TAG_GPS_LATITUDE: " + exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
            m += "\n TAG_GPS_LATITUDE_REF: " + exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
            m += "\n TAG_GPS_LONGITUDE: " + exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
            m += "\n TAG_GPS_LONGITUDE_REF: " + exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);
            m += "\n TAG_GPS_PROCESSING_METHOD: " + exifInterface.getAttribute(ExifInterface.TAG_GPS_PROCESSING_METHOD);
            L.d(TAG, m);

        } catch (IOException e) {
            L.ex(TAG, e);
        }
    }

//    public static Location extractLocation(@NonNull Uri imagePath) {
//        try {
//            ExifInterface exifInterface = new ExifInterface(imagePath.toString());
//            exifInterface.g
//        } catch (IOException ex) {
//            L.ex(TAG, ex);
//        }
//    }
}
