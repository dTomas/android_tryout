/**
 * @author Martynas Šustavičius
 * @since 2014 08 09
 */
package lt.smworks.tools.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.BatteryManager;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import androidx.annotation.NonNull;

/**
 * Gives access to available device information.
 */
public class DeviceInfo {

	public static final String TAG = "DeviceInfo";

	/**
	 * This might not work on some older android devices,
	 * and device id might change on factory reset.
	 * @return Unique device id.
	 */
	public static String getUniqueDeviceId(Context context) {
		return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
	}

	/**
	 * @note Depends on screen orientation.
	 * @return Width of the screen in pixels.
	 */
	@SuppressWarnings("deprecation")
	public static int getScreenWidth(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		return display.getWidth();
	}

	/**
	 * @note Depends on screen orientation.
	 * @return Height of the screen in pixels.
	 */
	@SuppressWarnings("deprecation")
	public static int getScreenHeight(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		return display.getHeight();
	}

	/**
	 * @return Integer containing android version id.
	 * Possible values are defined in Build.VERSION_CODES.
	 */
	public static int getAndroidVersion() {
		return android.os.Build.VERSION.SDK_INT;
	}

	/**
	 * @return Operating system version.
	 */
	public static String getOSVersion() {
		return android.os.Build.VERSION.RELEASE;
	}

	/**
	 * @return Device name.
	 */
	public static String getDeviceName() {
		return android.os.Build.MODEL;
	}

	/**
	 * @require android.permission.READ_HONE_STATE
	 * @return Phone number.
	 */
	public static String getPhoneNumber(Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String number = telephonyManager.getLine1Number();
		if (number == null || number.length() == 0) {
			number = telephonyManager.getSubscriberId();
		}
		return number;
	}

	public static String getBuildVersion(Context context) {
		String versionName;
		try {
			versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
		return versionName;
	}

	/**
	 * Returns screen brightness.
	 * @NOTE: requires WRITE_SETTINGS permission.
	 * @return Screen brightness, where 0 - completely dark, 255 - maximum brightness.
	 */
	public static int getBrightness(Context context) {
		return Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS,-1);
	}

	/**
	 * @param lineCount - number of lines to show.
	 * @return Logcat output.
	 */
	public static String getLogcat(long lineCount, String tag) {
		try {
			Process process = Runtime.getRuntime().exec(String.format("logcat -v time -t %d -s %s", lineCount, tag));
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			StringBuilder log = new StringBuilder();
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				log.append(line).append("\n");
			}
			return log.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	/**
	 * @return True if device is charging on outlet charger.
	 */
	public static boolean isChargingOnAC(Context context) {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        return plugged == BatteryManager.BATTERY_PLUGGED_AC; // || plugged == BatteryManager.BATTERY_PLUGGED_USB;
	}

	@Deprecated
	public static void printHashKey(@NonNull Context context) {
		try {
			@SuppressLint("PackageManagerGetSignatures") PackageInfo info =
                    context.getPackageManager().getPackageInfo(
                            context.getPackageName(), PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String hashKey = new String(Base64.encode(md.digest(), 0));
				L.i(TAG, "Hash key: %s", hashKey);
			}
		} catch (Exception e) {
			L.e(e);
		}
	}
}
