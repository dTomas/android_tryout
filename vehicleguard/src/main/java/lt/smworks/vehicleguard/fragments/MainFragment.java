package lt.smworks.vehicleguard.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import lt.smworks.tools.interfaces.Layout;
import lt.smworks.tools.models.SettingType;
import lt.smworks.tools.utils.AnimationUtils;
import lt.smworks.tools.utils.DialogUtils;
import lt.smworks.tools.utils.Settings;
import lt.smworks.tools.utils.SystemUtils;
import lt.smworks.tools.utils.ValidationUtils;
import lt.smworks.vehicleguard.BackgroundService;
import lt.smworks.vehicleguard.R;
import lt.smworks.vehicleguard.models.SettingTypes;

import static lt.smworks.vehicleguard.BackgroundService.ACCELERATION_UPDATE;
import static lt.smworks.vehicleguard.BackgroundService.ACTIVITY_UPDATE;
import static lt.smworks.vehicleguard.BackgroundService.COORDINATE_UPDATE;

@Layout(R.layout.main_fragment)
public class MainFragment extends VehicleGuardFragment {

    private static final int TURN_OFF_DELAY = 2000;
    private static final int FADE_ANIMATION_DURATION = 2000;
    private Settings settings;
    private TextView sensitivity;
    private TextView location;
    private TextView threshold;
    private LinearLayout content;
    private ImageView background;
    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isStarted = Settings.VAL_TRUE.equals(settings.get(SettingTypes.SERVICE_STARTED));
            if (!isStarted || !isVisible()) {
                return;
            }

            if (intent.hasExtra(COORDINATE_UPDATE)) {
                String coords = intent.getStringExtra(COORDINATE_UPDATE);
                location.setText(coords);
            }

            if (intent.hasExtra(ACTIVITY_UPDATE)) {
                activityType.setText(intent.getStringExtra(ACTIVITY_UPDATE));
            }

            if (intent.hasExtra(ACCELERATION_UPDATE)) {
                double acceleration = intent.getDoubleExtra(ACCELERATION_UPDATE, 0.0);
                sensitivity.setText(String.format(Locale.getDefault(), "%.2f", acceleration));
            }
        }
    };
    private TextView activityType;

    @Override
    protected void onCreateView(@NonNull Context context) {
        settings = new Settings(context);
        boolean isStarted = Settings.VAL_TRUE.equals(settings.get(SettingTypes.SERVICE_STARTED));

        getMainActivity().setDrawerMenuEnabled(!isStarted);

        FloatingActionButton fab = findView(R.id.mainFragment_fab);
        ProgressBar fabProgress = findView(R.id.mainFragment_fabProgress);
        sensitivity = findView(R.id.mainFragment_sensitivity);
        threshold = findView(R.id.mainFragment_threshold);
        activityType = findView(R.id.mainFragment_activityValue);
        content = findView(R.id.mainFragment_content);
        location = findView(R.id.mainFragment_location);
        background = findView(R.id.mainFragment_background);

        content.setVisibility(isStarted ? View.VISIBLE : View.INVISIBLE);
        background.setVisibility(isStarted ? View.INVISIBLE : View.VISIBLE);

        setupTurnOnOffButton(fab, fabProgress,
                settings.get(SettingTypes.SERVICE_STARTED).equals(Settings.VAL_TRUE));

        fab.setOnClickListener(view -> {
            if (fabProgress.getVisibility() == View.VISIBLE) {
                return;
            }

            fabProgress.setVisibility(View.VISIBLE);
            if (settings.get(SettingTypes.SERVICE_STARTED).equals(Settings.VAL_TRUE)) {
                actionTurnOff(fab, fabProgress);
            } else {
                actionTurnOn(fab, fabProgress);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean isStarted = Settings.VAL_TRUE.equals(settings.get(SettingTypes.SERVICE_STARTED));
        if (isStarted) {
            showHiddenContent();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        boolean isStarted = Settings.VAL_TRUE.equals(settings.get(SettingTypes.SERVICE_STARTED));
        if (isStarted) {
            hideContent();
        }
    }

    private void showHiddenContent() {
        threshold.setText(getString(
                R.string.disturbance_threshold,
                getSettings().getInt(SettingTypes.SENSOR_SENSITIVITY) * 0.1f));
        AnimationUtils.fadeViewIn(content, FADE_ANIMATION_DURATION);
        AnimationUtils.fadeViewOut(background, FADE_ANIMATION_DURATION);
        IntentFilter intentFilter = new IntentFilter(BackgroundService.SERVICE_UPDATE);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                notificationReceiver, intentFilter);
        String loc = new Settings(getContext()).get(SettingType.LAST_LOCATION);
        location.setText(loc);
    }

    private void hideContent() {
        AnimationUtils.fadeViewIn(background, FADE_ANIMATION_DURATION);
        AnimationUtils.fadeViewOut(content, FADE_ANIMATION_DURATION);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(notificationReceiver);
    }

    private void setupTurnOnOffButton(FloatingActionButton fab, ProgressBar fabProgress, boolean isStarted) {
        fab.setEnabled(true);
        fabProgress.setVisibility(View.INVISIBLE);
        if (isStarted) {
            fab.setBackgroundTintList(
                    ColorStateList.valueOf(getResources().getColor(R.color.red)));
            fab.setImageResource(R.drawable.ic_unlocked);
        } else {
            fab.setBackgroundTintList(
                    ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
            fab.setImageResource(R.drawable.ic_locked);
        }
    }

    private void actionTurnOn(@NonNull FloatingActionButton fab, @NonNull ProgressBar progressBar) {
        if (!settings.isSet(SettingTypes.PHONE_NUMBER)
                || !ValidationUtils.isValidPhoneNumber(settings.get(SettingTypes.PHONE_NUMBER))) {
            DialogUtils.showToast(getMainActivity(), R.string.must_provide_number);
            getMainActivity().replaceFragment(new SettingsFragment());
            return;
        }

        SystemUtils.hideSoftKeyboard(getMainActivity());
        settings.set(SettingTypes.SERVICE_STARTED, Settings.VAL_TRUE);
        BackgroundService.startIfNeeded(getMainActivity().getApplicationContext());
        getMainActivity().setDrawerMenuEnabled(false);
        fab.setEnabled(false);
        closeTurnOnDialog(() -> setupTurnOnOffButton(fab, progressBar, true));
        showHiddenContent();
    }

    private void actionTurnOff(@NonNull FloatingActionButton fab, @NonNull ProgressBar progressBar) {
        settings.set(SettingTypes.SERVICE_STARTED, Settings.VAL_FALSE);
        BackgroundService.stop(getContext());
        getMainActivity().setDrawerMenuEnabled(true);
        fab.setEnabled(false);
        closeTurnOffDialog(() -> setupTurnOnOffButton(fab, progressBar, false));
        hideContent();
    }

    private void closeTurnOffDialog(@NonNull Runnable runnable) {
        new Handler().postDelayed(() -> {
            if (getContext() == null) {
                return;
            }

            if (BackgroundService.isNotRunning(getContext())) {
                runnable.run();
            } else {
                closeTurnOffDialog(runnable);
            }
        }, TURN_OFF_DELAY);
    }

    private void closeTurnOnDialog(@NonNull Runnable runnable) {
        new Handler().postDelayed(() -> {
            if (getContext() == null) {
                return;
            }

            if (!BackgroundService.isNotRunning(getContext())) {
                runnable.run();
            } else {
                closeTurnOnDialog(runnable);
            }
        }, TURN_OFF_DELAY);
    }
}
