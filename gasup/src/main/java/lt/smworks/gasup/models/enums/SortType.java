package lt.smworks.gasup.models.enums;

public enum  SortType {
    PRICE(1),
    DISTANCE(2),
    TIME(3),
    UNKNOWN(0);

    private int id;

    SortType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static SortType fromId(int id) {
        for (SortType sortType : values()) {
            if (sortType.id == id) {
                return sortType;
            }
        }
        return UNKNOWN;
    }
}
